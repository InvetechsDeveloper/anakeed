package com.invetechs.anakeed.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.invetechs.anakeed.AppConfig.CustomDialogProgress;
import com.invetechs.anakeed.AppConfig.ShowDialog;
import com.invetechs.anakeed.R;
import com.invetechs.anakeed.Retrofit.connection.RetrofitClient;
import com.invetechs.anakeed.Retrofit.notifications.NotificationResponse;
import com.invetechs.anakeed.Retrofit.notifications.sendNotificationRequest;
import com.invetechs.anakeed.View.Activity.OrderDetails;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static android.content.Context.MODE_PRIVATE;


public class NotificationstAdapter extends RecyclerView.Adapter<NotificationstAdapter.MyViewHolder> {

    // vars
    SharedPreferences sharedPreferences;
    Context context;
    private List<NotificationResponse.NotificaionsBean> notificaionsList = new ArrayList<>();
    CustomDialogProgress progress;
    Handler handler;
    ShowDialog showDialog = new ShowDialog();


    public NotificationstAdapter(Context context, List<NotificationResponse.NotificaionsBean> notificaionsList) {
        this.context = context;
        this.notificaionsList = notificaionsList;
    } // constructor

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.notification_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        sharedPreferences = context.getSharedPreferences("user", MODE_PRIVATE);

        if (sharedPreferences.getString("language", "ar").equals("ar")) {

            holder.tv_title.setText(notificaionsList.get(position).getAr_title());
            holder.tv_notification_time.setText(notificaionsList.get(position).getAr_time());
            holder.img_view.setRotation(180);

        } else if (sharedPreferences.getString("language", "ar").equals("en")) {
            holder.tv_title.setText(notificaionsList.get(position).getEn_title());
            holder.tv_notification_time.setText(notificaionsList.get(position).getEn_time());

        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.i("Print", "notification order id: " + notificaionsList.get(position).getId());

                Log.i("Print", "notification order id: " + notificaionsList.get(position).getOrder_id());
                Intent intent = new Intent(context, OrderDetails.class);
                intent.putExtra("orderId", String.valueOf(notificaionsList.get(position).getOrder_id()));
                try {
                    sendNotificationID(notificaionsList.get(position).getId());

                }
                catch (Exception e)
                {

                }
                context.startActivity(intent);
                ((Activity) context).finish();


            }
        });
    }

    @SuppressLint("HandlerLeak")
    private void sendNotificationID(final int notId) {

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                //progress.dismiss();
                super.handleMessage(msg);
            }

        };

        new Thread() {
            public void run() {

                Retrofit retrofit = RetrofitClient.getInstance();

                final sendNotificationRequest notificationApi = retrofit.create(sendNotificationRequest.class);

                final Call<NotificationResponse> getInterestConnection = notificationApi.sendNotification(notId);

                Log.i("Print", "notification_id : " + notId);


                getInterestConnection.enqueue(new Callback<NotificationResponse>() {
                    @Override
                    public void onResponse(Call<NotificationResponse> call, Response<NotificationResponse> response) {
                        try {
                            String code = response.body().getCode();

                            Log.i("Print", "code" + code);

                            Log.i("Print", "response" + response.body().getCode());

                            if (code.equals("200")) {

                                Log.i("Print", "sucessssssssss" + response.body().getCode());

                            } // login success

                        } // try
                        catch (Exception e) {
                            Log.i("Print", "exception : " + e.toString());
                        } // catch
                    } // onResponse

                    @Override
                    public void onFailure(Call<NotificationResponse> call, Throwable t) {
                        showDialog.initDialog(context.getString(R.string.retry), context);
                        Log.i("Print", "error : " + t.toString());
                    } // on Failure
                });
            }

        }.start();
    } // show notifications id



    @Override
    public int getItemCount() {
        return notificaionsList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_title)
        TextView tv_title;

        @BindView(R.id.tv_notification_time)
        TextView tv_notification_time;

        @BindView(R.id.img_view)
        ImageView img_view;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
