package com.invetechs.anakeed.Adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.util.Log;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.invetechs.anakeed.Model.HomeCategory;
import com.invetechs.anakeed.View.Fragment.CategoryTabFragment;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class CategoriesViewPagerAdapter extends FragmentPagerAdapter {

    List<HomeCategory> categoryList = new ArrayList<>();
    Context context;

    public CategoriesViewPagerAdapter(FragmentManager fm, List<HomeCategory> categoryList, Context context) {
        super(fm);
        this.categoryList = categoryList;
        this.context = context;
    }

    @Override
    public Fragment getItem(int i) {
        return CategoryTabFragment.getInstance(i);
    }

    @Override
    public int getCount() {

        return (categoryList == null) ? 0 :
                categoryList.size();

        //return categoryList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        SharedPreferences sharedPreferences;
        sharedPreferences = context.getSharedPreferences("user", MODE_PRIVATE);
        String title = "";

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            title = categoryList.get(position).getCategoryArTitle();
            Log.i("Print", "title arabic : " + title);

        } else if (sharedPreferences.getString("language", "ar").equals("en")) {
            title = categoryList.get(position).getCategoryEnTitle();
            Log.i("Print", "title english : " + title);

        }

        return title;
    }


} // class of CategoriesViewPagerAdapter


