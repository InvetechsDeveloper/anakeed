package com.invetechs.anakeed.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.invetechs.anakeed.R;
import com.invetechs.anakeed.Retrofit.orders.ordersResponse;
import com.invetechs.anakeed.View.Activity.OrderDetails;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

import static android.content.Context.MODE_PRIVATE;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.MyViewHolder> {

    // variables
    Context context;
    private List<ordersResponse.OrdersBean> ordersList = new ArrayList<>();
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;


    public OrderAdapter(Context context, List<ordersResponse.OrdersBean> orders) {
        this.context = context;
        ordersList = orders;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.order_item, parent, false);
        return new OrderAdapter.MyViewHolder(view);
    }

    public String convertToArabic(String value) {
        String newValue = (((((((((((value + "")
                .replaceAll("1", "١")).replaceAll("2", "٢"))
                .replaceAll("3", "٣")).replaceAll("4", "٤"))
                .replaceAll("5", "٥")).replaceAll("6", "٦"))
                .replaceAll("7", "٧")).replaceAll("8", "٨"))
                .replaceAll("9", "٩")).replaceAll("0", "٠"));

        return newValue;
    } // convert arabic date to english to send to api

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder , final int position) {

        sharedPreferences = context.getSharedPreferences("user", MODE_PRIVATE);
        editor = sharedPreferences.edit();

        if (sharedPreferences.getString("language", "ar").equals("ar")) {

            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/ElMessiri-Regular.ttf");
            holder.tv_order_number.setTypeface(font);
            holder.tv_order_date.setTypeface(font);
            holder.tv_price.setTypeface(font);
            holder.tv_status.setTypeface(font);

            holder.tv_order_date.setText(convertToArabic(ordersList.get(position).getCreated_at()));
            holder.tv_order_number.setText(convertToArabic(String.valueOf(ordersList.get(position).getId())));
            holder.tv_price.setText(convertToArabic(String.valueOf(ordersList.get(position).getTotal_price())));


            Log.i("Print", "type = " + ordersList.get(position).getType());

            if (ordersList.get(position).getType().equals("current")) {
                holder.tv_status.setText(context.getString(R.string.new_order));

            } else if (ordersList.get(position).getType().equals("finished")) {
                holder.tv_status.setText(context.getString(R.string.finished));

            } else if (ordersList.get(position).getType().equals("accepted")) {
                holder.tv_status.setText(context.getString(R.string.accepted));

            } else if (ordersList.get(position).getType().equals("rejected")) {
                holder.tv_status.setText(context.getString(R.string.rejected));

            }


            Log.i("Print", "font App : ar");
        } else if (sharedPreferences.getString("language", "ar").equals("en")) {

            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Regular.ttf");
            holder.tv_order_number.setTypeface(font);
            holder.tv_order_date.setTypeface(font);
            holder.tv_price.setTypeface(font);
            holder.tv_status.setTypeface(font);
            Log.i("Print", "font App : english");

            holder.tv_status.setText(ordersList.get(position).getType());
            holder.tv_order_date.setText(" " + String.valueOf(ordersList.get(position).getCreated_at()));
            holder.tv_order_number.setText(String.valueOf(ordersList.get(position).getId()));
            holder.tv_price.setText("" + ordersList.get(position).getTotal_price());
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String order_number = String.valueOf(ordersList.get(position).getId());
                Intent intent = new Intent(view.getContext(), OrderDetails.class);
                intent.putExtra("orderId", order_number);
                Log.i("Print", "send order id from adapter : " + order_number);
                view.getContext().startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return ordersList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_order_number)
        TextView tv_order_number;

        @BindView(R.id.tv_order_date)
        TextView tv_order_date;

        @BindView(R.id.tv_price)
        TextView tv_price;

        @BindView(R.id.tv_status)
        TextView tv_status;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
