package com.invetechs.anakeed.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;

import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.google.android.material.button.MaterialButton;

import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.invetechs.anakeed.AppConfig.CustomDialogProgress;
import com.invetechs.anakeed.AppConfig.ShowDialog;
import com.invetechs.anakeed.R;
import com.invetechs.anakeed.Retrofit.connection.RetrofitClient;
import com.invetechs.anakeed.Retrofit.cart.storeBasketRequest;
import com.invetechs.anakeed.Retrofit.cart.storeBasketResponse;
import com.invetechs.anakeed.Retrofit.products.productResponse;
import com.invetechs.anakeed.View.Activity.CategoryItemDetails;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static android.content.Context.MODE_PRIVATE;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {

    List<String> categoryList;
    Context context;
    String color;
    private List<productResponse.ProductBean> productList = new ArrayList<>();
    SharedPreferences sharedPreferences;
    CustomDialogProgress progress;
    Handler handler;
    String category_id = "";
    ShowDialog toastDialog = new ShowDialog();
    int quantity_value = 1;
    String imageUrl = "http://3nakied.invetechs.com/cpanel/upload/Slider/";


//    public CategoryAdapter(Context context, List<String> categoryList, String color) {
//        this.context = context;
//        this.categoryList = categoryList;
//        this.color = color;
//    } // function of CategoryAdapter


    public CategoryAdapter(Context context, String color, List<productResponse.ProductBean> productList) {
        this.context = context;
        this.color = color;
        this.productList = productList;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.categoryItem_title)
        TextView categoryItem_title;

        @BindView(R.id.categoryItem_description)
        TextView categoryItem_description;

        @BindView(R.id.categoryItem_typeOne)
        TextView categoryItem_typeOne;

        @BindView(R.id.category_image)
        ImageView category_image;


//        @BindView(R.id.categoryItem_slash)
//        TextView categoryItem_slash;

//        @BindView(R.id.categoryItem_typeTwo)
//        TextView categoryItem_typeTwo;

        @BindView(R.id.categoryItem_typeOneBackground)
        TextView categoryItem_typeOneBackground;

        @BindView(R.id.number_button)
        ElegantNumberButton number_button;

//        @BindView(R.id.categoryItem_typeTwoBackground)
//        TextView categoryItem_typeTwoBackground;

        @BindView(R.id.categoryItem_addToCartButton)
        MaterialButton categoryItem_addToCartButton;

//        @BindView(R.id.categoryItem_decrease1)
//        ImageView categoryItem_decrease1;

//        @BindView(R.id.categoryItem_increase1)
//        ImageView categoryItem_increase1;

//        @BindView(R.id.categoryItem_decrease2)
//        ImageView categoryItem_decrease2;

//        @BindView(R.id.categoryItem_increase2)
//        ImageView categoryItem_increase2;

//        @BindView(R.id.categoryItem_textCounter1)
//        TextView categoryItem_textCounter1;

        @BindView(R.id.tv_price)
        TextView tv_price;

//        @BindView(R.id.categoryItem_textCounter2)
//        TextView categoryItem_textCounter2;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.category_iteeeem, viewGroup, false);
        return new ViewHolder(view);
    }


    public String convertToArabic(String value) {
        String newValue = (((((((((((value + "")
                .replaceAll("1", "١")).replaceAll("2", "٢"))
                .replaceAll("3", "٣")).replaceAll("4", "٤"))
                .replaceAll("5", "٥")).replaceAll("6", "٦"))
                .replaceAll("7", "٧")).replaceAll("8", "٨"))
                .replaceAll("9", "٩")).replaceAll("0", "٠"));

        return newValue;
    } // convert arabic date to english to send to api


    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder , final int i) {

        /* color setting */
        try {
            viewHolder.categoryItem_title.setTextColor(Color.parseColor(color));
            viewHolder.categoryItem_description.setTextColor(Color.parseColor(color));
            viewHolder.categoryItem_typeOne.setTextColor(Color.parseColor(color));
//            viewHolder.categoryItem_typeTwo.setTextColor(Color.parseColor(color));
//            viewHolder.categoryItem_slash.setTextColor(Color.parseColor(color));
            viewHolder.categoryItem_addToCartButton.setBackgroundColor(Color.parseColor(color));
            ColorStateList csl = new ColorStateList(new int[][]{new int[0]}, new int[]{Color.parseColor(color)});
            viewHolder.categoryItem_addToCartButton.setBackgroundTintList(csl);

            //   viewHolder.categoryItem_decrease1.setColorFilter(Color.parseColor(color));
            //  viewHolder.categoryItem_increase1.setColorFilter(Color.parseColor(color));
            // viewHolder.categoryItem_decrease2.setColorFilter(Color.parseColor(color));
            //  viewHolder.categoryItem_increase2.setColorFilter(Color.parseColor(color));

            //    viewHolder.categoryItem_textCounter1.setTextColor(Color.parseColor(color));
            //    viewHolder.categoryItem_textCounter2.setTextColor(Color.parseColor(color));


            GradientDrawable bgShape = (GradientDrawable) viewHolder.categoryItem_typeOneBackground.getBackground();
            //  GradientDrawable bgShape1 = (GradientDrawable) viewHolder.categoryItem_typeTwoBackground.getBackground();


            bgShape.mutate();
            bgShape.setColor(Color.parseColor(color));
//            bgShape1.mutate();
//            bgShape1.setColor(Color.parseColor(color));
            /* color setting */
        //    String total = convertToArabic(String.valueOf(productList.get(i).getPrice1()));


            Glide.with(context)
                    .load(imageUrl + productList.get(i).getImg())
                    .into(viewHolder.category_image);

            sharedPreferences = context.getSharedPreferences("user", MODE_PRIVATE);

            if (sharedPreferences.getString("language", "ar").equals("ar")) {
                String total = convertToArabic(String.valueOf(productList.get(i).getPrice1()));

                viewHolder.categoryItem_title.setText(productList.get(i).getAr_title());
                viewHolder.categoryItem_description.setText(productList.get(i).getAr_description());

                viewHolder.tv_price.setText(total + " " +  context.getString(R.string.sr));

                Log.i("Print", "categoryItem_title : " + productList.get(i).getAr_title());
                Log.i("Print", "totallll in category adapter: " + total);

            }

            else if (sharedPreferences.getString("language", "ar").equals("en")) {
                viewHolder.categoryItem_title.setText(productList.get(i).getEn_title());
                viewHolder.categoryItem_description.setText(productList.get(i).getEn_description());
                viewHolder.tv_price.setText(productList.get(i).getPrice1() + " " + context.getString(R.string.sr));

            }

        } catch (Exception e) {
            Log.i("Print", "Exception : " + e.getMessage());
        }

        category_id = String.valueOf(productList.get(i).getId());

        Log.i("Print", "category_id in category adapter: " + category_id);


        viewHolder.category_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

              //  category_id = String.valueOf(productList.get(i).getId());
                Log.i("Print", "send category_id in category adapter: " + category_id);

                Intent intent = new Intent(context, CategoryItemDetails.class);
                intent.putExtra("color", color);
                intent.putExtra("category_item_id", String.valueOf(productList.get(i).getId()));
                context.startActivity(intent);
            }
        }); // function of item clicked

        viewHolder.number_button.setOnValueChangeListener(new ElegantNumberButton.OnValueChangeListener() {
            @Override
            public void onValueChange(ElegantNumberButton view, int oldValue, int newValue) {

                quantity_value = Integer.parseInt(viewHolder.number_button.getNumber());

                Log.i("Print", "quantity_value : " + quantity_value);

                int b = productList.get(i).getPrice1();

                Log.i("Print", "price value : " + b);

                int update_price = quantity_value * b;

                Log.i("Print", "live_changes : " + "" + update_price);

                sharedPreferences = context.getSharedPreferences("user", MODE_PRIVATE);

                if (sharedPreferences.getString("language", "ar").equals("ar")) {

                    viewHolder.tv_price.setText(convertToArabic(String.valueOf(update_price)) + " " + context.getString(R.string.sr));

                } else {
                    Log.i("Print", "font App in order details : en");
                    viewHolder.tv_price.setText(productList.get(i).getPrice1() + " " + context.getString(R.string.sr));
                }


                Log.i("Print", "quantity_value in valueChanged: " + quantity_value);
            }
        }); // change quantiti number

        viewHolder.categoryItem_addToCartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendOrderToApi(getPrefUserId() , String.valueOf(productList.get(i).getId()) , quantity_value);
            }
        }); // click add to cart


    }

    public int getPrefUserId() {
        int userId = 0;
        try {
            SharedPreferences preferences = context.getSharedPreferences("MyPrefsFile", MODE_PRIVATE);

            if (preferences != null)
                userId = preferences.getInt("id", 0);

            Log.i("Print", "User id" + userId);
        } catch (Exception e) {
            Log.i("Print", "Exception" + e.getMessage());
        }

        return userId;
    } // function of getPrefUserId

    @SuppressLint("HandlerLeak")
    private void sendOrderToApi(final int user_id ,final   String category_id , final int quantity_value) {

        progress = new CustomDialogProgress();
        progress.init(context);
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }
        };
        progress.show();
        new Thread() {
            public void run() {

                Retrofit retrofit = RetrofitClient.getInstance();
                storeBasketRequest storeBasketRequest = retrofit.create(storeBasketRequest.class);

                Log.i("Print", "send userrr id : " + getPrefUserId());
                Log.i("Print", "send category id :  : " + category_id);
                Log.i("Print", "send quantity_value : " + quantity_value);

                Call<storeBasketResponse> basketResponseCall = storeBasketRequest.storeToBasket(user_id, category_id, quantity_value);
                basketResponseCall.enqueue(new Callback<storeBasketResponse>() {
                    @Override
                    public void onResponse(Call<storeBasketResponse> call, Response<storeBasketResponse> response) {
                        try {

                            Log.i("Print", "code" + response.body().getCode());

                            if (response.body() != null) {

                                String code = response.body().getCode();
                                Log.i("Print", "store basket code : " + code);

                                if (code.equals("200")) {
                                    Log.i("Print", "store basket code success" + code);
                                    toastDialog.initDialog(context.getString(R.string.addedToCart), context);

                                } else {
                                    toastDialog.initDialog(context.getString(R.string.retry), context);
                                    Log.i("Print", "else ");
                                }
                                progress.dismiss();
                            }

                        } catch (Exception e) {
                            Log.i("Print", "exception : " + e.toString());
                            progress.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(Call<storeBasketResponse> call, Throwable t) {
                        toastDialog.initDialog(context.getString(R.string.retry), context);
                        Log.i("Print", "error : " + t.toString());
                        progress.dismiss();
                    }
                });

            }
        }.start();

    } //function of getAddressesFromApi

    @Override
    public int getItemCount() {
        return productList.size();
    }


} // class of CategoryAdapter
