package com.invetechs.anakeed.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.invetechs.anakeed.AppConfig.CustomDialogProgress;
import com.invetechs.anakeed.AppConfig.ShowDialog;
import com.invetechs.anakeed.R;
import com.invetechs.anakeed.Retrofit.connection.RetrofitClient;
import com.invetechs.anakeed.Retrofit.cart.deleteBasketRequest;
import com.invetechs.anakeed.Retrofit.cart.getBasketResponse;
import com.invetechs.anakeed.View.Activity.CartActivity;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static android.content.Context.MODE_PRIVATE;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.MyViewHolder> {

    Context context;
    private List<getBasketResponse.DataBean> cartList = new ArrayList<>();
    SharedPreferences sharedPreferences;
    Handler handler;
    CustomDialogProgress progress;
    ShowDialog toastDialog = new ShowDialog();
    int product_id = 0;


    public CartAdapter() {
    }

    public CartAdapter(Context context, List<getBasketResponse.DataBean> cartList) {
        this.context = context;
        this.cartList = cartList;
    } // constructor

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.cart_item, parent, false);
        return new CartAdapter.MyViewHolder(view);
    }

    public static int getScreenWidth(Activity activity) {
        Point size = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(size);
        return size.x;
    } // function getScreenWidth

    public String convertToArabic(String value) {
        String newValue = (((((((((((value + "")
                .replaceAll("1", "١")).replaceAll("2", "٢"))
                .replaceAll("3", "٣")).replaceAll("4", "٤"))
                .replaceAll("5", "٥")).replaceAll("6", "٦"))
                .replaceAll("7", "٧")).replaceAll("8", "٨"))
                .replaceAll("9", "٩")).replaceAll("0", "٠"));

        return newValue;
    } // convert arabic date to english to send to api

    @SuppressLint("HandlerLeak")
    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        sharedPreferences = context.getSharedPreferences("user", MODE_PRIVATE);

        if (sharedPreferences.getString("language", "ar").equals("ar")) {

            holder.tv_title.setText(cartList.get(position).getAr_title());
            holder.tv_price.setText("" + convertToArabic(String.valueOf(cartList.get(position).getTotal_price())) + " " + context.getString(R.string.sr));
            holder.tv_quantity.setText("" + convertToArabic(String.valueOf(cartList.get(position).getCount())));

        } else if (sharedPreferences.getString("language", "ar").equals("en")) {
            holder.tv_title.setText(cartList.get(position).getEn_title());
            holder.tv_price.setText("" + cartList.get(position).getTotal_price() + " " + context.getString(R.string.sr));
            holder.tv_quantity.setText("" + cartList.get(position).getCount());

        }

        holder.deleteImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                holder.layout.setBackgroundResource(R.color.white);
                holder.layout2.setBackgroundResource(R.color.white);
                holder.dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                holder.dialog.getWindow()
                        .setLayout((int) (getScreenWidth((Activity) v.getContext()) * .9), ViewGroup.LayoutParams.WRAP_CONTENT);
                holder.dialog.show();

            }
        }); // click on delete image

        holder.yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.dialog.dismiss();
                product_id = cartList.get(position).getId();
                Log.i("Print", "product_id to delete " + product_id);
                deleteBasket(cartList.get(position).getId(), position);


            }
        }); // yes delete product item

        holder.no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.dialog.dismiss();
            }
        }); // not delete product item

    }


    @SuppressLint("HandlerLeak")
    private void deleteBasket(final int product_id, final int position) {

        progress = new CustomDialogProgress();
        progress.init(context);

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
        new Thread() {
            public void run() {

                Retrofit retrofit = RetrofitClient.getInstance();

                final deleteBasketRequest basketRequest = retrofit.create(deleteBasketRequest.class);

                Log.i("Print", "product_id before request" + product_id);

                final Call<getBasketResponse> getInterestConnection = basketRequest.deleteBasket(product_id);

                getInterestConnection.enqueue(new Callback<getBasketResponse>() {
                    @Override
                    public void onResponse(Call<getBasketResponse> call, Response<getBasketResponse> response) {
                        try {
                            String code = response.body().getCode();

                            Log.i("Print", "code" + code);

                            response.body();

                            Log.i("Print", "response" + response.body().getCode());

                            if (code.equals("200")) {
                                Log.i("Print", "response success to delete " + response.body().getCode());

//                                if (cartList.size() == 1) {
//
//                                    toastDialog.initDialog(context.getString(R.string.product_deleted), context);
//                                    cartList.remove(position);
//                                    notifyItemRemoved(position);
//                                    notifyItemRangeChanged(position , cartList.size());
//                                    notifyDataSetChanged();
//                                    context.startActivity(new Intent(context , CartActivity.class));
//                                }

                                try {
                                    toastDialog.initDialog(context.getString(R.string.product_deleted), context);
                                    cartList.remove(position);
                                    notifyItemRemoved(position);
                                    notifyItemRangeChanged(position, cartList.size());
                                    notifyDataSetChanged();
                                    if(context instanceof CartActivity){
                                        ((CartActivity)context).getBasketData();
                                    }
//                                        context.startActivity(new Intent(context, CartActivity.class));
//                                        ((Activity) context).finish();


                                } catch (Exception e) {
                                    Log.i("Print", "exception : " + e.toString());

                                }

//                                if(context instanceof CartActivity){
//                                    ((CartActivity)context).getBasketData();
//                                }


                            } // login success

                            progress.dismiss();

                        } // try
                        catch (Exception e) {
                            Log.i("Print", "exception : " + e.toString());
                            progress.dismiss();
                        } // catch
                    } // onResponse

                    @Override
                    public void onFailure(Call<getBasketResponse> call, Throwable t) {
                        try {

                            toastDialog.initDialog(context.getString(R.string.retry), context);
                        } catch (Exception e) {

                        }
                        Log.i("Print", "error : " + t.toString());
                        progress.dismiss();
                    } // on Failure
                });
            }

        }.start();
    } // delete basket

    @Override
    public int getItemCount() {
        return cartList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_title)
        TextView tv_title;

        @BindView(R.id.tv_quantity)
        TextView tv_quantity;

        @BindView(R.id.tv_price)
        TextView tv_price;

        @BindView(R.id.deleteImage)
        ImageView deleteImage;

        // variables
        Dialog dialog;
        Button yes, no;
        LinearLayout layout, layout2;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            dialog = new Dialog(itemView.getContext());
            dialog.setContentView(R.layout.dialog_confirm_delete_product);

            yes = dialog.findViewById(R.id.yes);
            no = dialog.findViewById(R.id.no);
            layout = dialog.findViewById(R.id.dialogLayout);
            layout2 = dialog.findViewById(R.id.dialogLayout2);
        }
    }
}
