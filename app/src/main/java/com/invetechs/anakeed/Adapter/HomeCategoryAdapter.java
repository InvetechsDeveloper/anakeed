package com.invetechs.anakeed.Adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.invetechs.anakeed.Model.HomeCategory;
import com.invetechs.anakeed.Model.HomeCategoryResultModel;
import com.invetechs.anakeed.R;
import com.invetechs.anakeed.View.Activity.CategoriesActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeCategoryAdapter extends RecyclerView.Adapter<HomeCategoryAdapter.MyViewHolder> {

    List<HomeCategory> categoryList;
    Context context;
    HomeCategory homeCategory = HomeCategory.getInstance();
    HomeCategoryResultModel.CategoriesBean model;

    SharedPreferences sharedPreferences;
    String cuurentLanguage = "ar";
    String imageUrl = "http://3nakied.invetechs.com/cpanel/upload/Category/";

    public HomeCategoryAdapter(Context context, List<HomeCategory> categoryList) {
        this.context = context;
        this.categoryList = categoryList;
        homeCategory.setCategoryList(categoryList);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.item_container)
        LinearLayout item_container;


        @BindView(R.id.item_image)
        ImageView item_image;


        @BindView(R.id.item_title)
        TextView item_title;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    } // class of MyViewHolder

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.home_category_item, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder , final int i) {

        try {

            myViewHolder.item_container.setBackgroundColor(Color.parseColor(categoryList.get(i).getBackgroundColor()));
        } catch (Exception e) {
            Log.i("Print" , "exception : " + e.getMessage());
        }
        //myViewHolder.item_image.setImageResource(categoryList.get(i).getCategoryImage());

        Glide.with(context)
                .load(imageUrl + categoryList.get(i).getCategoryImage())
                .into(myViewHolder.item_image);

        if (checkLanguage().equals("en"))
            myViewHolder.item_title.setText(categoryList.get(i).getCategoryEnTitle());
        else if (checkLanguage().equals("ar"))
            myViewHolder.item_title.setText(categoryList.get(i).getCategoryArTitle());

        Log.i("Print", "item_title : " + categoryList.get(i).getCategoryArTitle());


        myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                homeCategory.setSlectedPosition(i);
                Intent intent = new Intent(context, CategoriesActivity.class);
                context.startActivity(intent);
            }
        });

    }

    private String checkLanguage() {
        sharedPreferences = context.getSharedPreferences("user", Context.MODE_PRIVATE);
        cuurentLanguage = sharedPreferences.getString("language", "ar");
        return cuurentLanguage;
    } // function of checkLanguage

    @Override
    public int getItemCount() {
        return categoryList.size();
    }


} // class of HomeCategoryAdapter
