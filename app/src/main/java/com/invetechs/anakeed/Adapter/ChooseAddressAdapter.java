package com.invetechs.anakeed.Adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;

import com.invetechs.anakeed.R;
import com.invetechs.anakeed.Retrofit.addresses.addressResponse;
import com.invetechs.anakeed.View.Activity.MenuActivity;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ChooseAddressAdapter extends RecyclerView.Adapter<ChooseAddressAdapter.MyViewHolder> {

    // variables
    Context context;
    List<addressResponse.DataBean> addressList = new ArrayList<>();
    Button okChooseAddressButton;
    private int lastSelectedPosition = -1;
    double lat, lng;
    SharedPreferences.Editor editorAddress;

    public ChooseAddressAdapter(Context context, List<addressResponse.DataBean> addressList, Button okChooseAddressButton) {
        this.context = context;
        this.addressList = addressList;
        this.okChooseAddressButton = okChooseAddressButton;
    } // constructor of AddressesAdapter


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_choose_address, parent, false);
        return new ChooseAddressAdapter.MyViewHolder(view);
    } // on create

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder,final int position) {

        holder.title.setText(addressList.get(position).getName()); // get note description
        holder.address.setText(addressList.get(position).getAddress()); // get address

        holder.rb_chooseAddress.setChecked(lastSelectedPosition == position);

        if (holder.rb_chooseAddress.isChecked()) {
            Log.i("Print", "itemChecked : " + addressList.get(position).getId());
            lat = addressList.get(position).getLat();
            lng = addressList.get(position).getLng();

            Log.i("Print", "lat in choose address adapter : " + lat);
            Log.i("Print", "lng in choose address adapter : : " + lng);
        } // get lat and lng for selected address

    okChooseAddressButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                editorAddress = view.getContext().getSharedPreferences("AddressPref", Context.MODE_PRIVATE).edit();
                editorAddress.putFloat("lat", (float) lat);
                editorAddress.putFloat("lng", (float) lng);
              //  editorAddress.putString("address", addressList.get(position).getAddress());
                editorAddress.apply();

                Log.i("Print", "lat from adapter choose address: " + lat);
                Log.i("Print", "lng from adapter choose address: " + lng);
                Log.i("Print", "address from adapter choose address: " + addressList.get(position).getAddress());

                Log.i("Print", "adapterAddress : " + lat + " : " + lng);
                Intent intent = new Intent(view.getContext(), MenuActivity.class);
                intent.putExtra("fragmentFlag", "home");
                view.getContext().startActivity(intent);

            }
        }); // click on ok after choose an address


    } // on bind method

    @Override
    public int getItemCount() {
        return addressList.size();
    } // get item count

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.address_title)
        TextView title;

        @BindView(R.id.address_address)
        TextView address;

        @BindView(R.id.rb_chooseAddress)
        RadioButton rb_chooseAddress;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            rb_chooseAddress.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    lastSelectedPosition = getAdapterPosition();
                    notifyDataSetChanged();
                }
            }); // choose address from list
        }
    }
}
