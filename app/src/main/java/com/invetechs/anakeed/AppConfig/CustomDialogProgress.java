package com.invetechs.anakeed.AppConfig;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import androidx.core.widget.ContentLoadingProgressBar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.invetechs.anakeed.R;

public class CustomDialogProgress extends Activity
{
    Dialog progress;
    TextView txt_pleaseWait;
    LinearLayout layout;
    ContentLoadingProgressBar contentLoadingProgressBar;
    Context context;

    private static int getScreenWidth(Activity activity) {
        Point size = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(size);
        return size.x;
    } // function getScreenWidth

    public void init(Context context)
    {
        this.context = context;
        progress = new Dialog(context);
        progress.setContentView(R.layout.progress_loading_dialog);
        progress.setCancelable(false);
        txt_pleaseWait = progress.findViewById(R.id.txt_pleaseWait);
        SharedPreferences sharedPreferences = context.getSharedPreferences("user", Context.MODE_PRIVATE);

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/ElMessiri-Regular.ttf");
            txt_pleaseWait.setTypeface(font);
        }

        else if (sharedPreferences.getString("language", "ar").equals("en")) {
            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Regular.ttf");
            txt_pleaseWait.setTypeface(font);

        }
        layout = progress.findViewById(R.id.layoutDialog);
        contentLoadingProgressBar = progress.findViewById(R.id.progressLoading);
        contentLoadingProgressBar.setVisibility(View.VISIBLE);
//        layout.setBackgroundResource(R.color.);
        progress.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progress.getWindow().setLayout((int) (getScreenWidth((Activity) context) * .9), ViewGroup.LayoutParams.WRAP_CONTENT);
    } // function of init

    public void show()
    {
        if(context != null && !isFinishing()) {
            progress.show();
        }

    } // function of show

    public void dismiss()
    {
        progress.dismiss();
    } // function of dismiss
} // class of CustomDialogProgress
