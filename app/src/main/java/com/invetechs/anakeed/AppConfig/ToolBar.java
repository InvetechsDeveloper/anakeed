package com.invetechs.anakeed.AppConfig;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;

import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.invetechs.anakeed.Language.MyContextWrapper;
import com.invetechs.anakeed.R;
import com.invetechs.anakeed.View.Activity.CartActivity;
import com.invetechs.anakeed.View.Activity.NotificationActivity;
import com.nex3z.notificationbadge.NotificationBadge;
import com.pusher.client.Pusher;
import com.pusher.client.PusherOptions;
import com.pusher.client.channel.Channel;
import com.pusher.client.channel.SubscriptionEventListener;

import org.json.JSONException;
import org.json.JSONObject;

import me.leolin.shortcutbadger.ShortcutBadger;
import static android.content.Context.MODE_PRIVATE;

public class ToolBar {

    // variables

    Toolbar toolbar;
    TextView toolbar_title;
    Activity activity;
    String title;
    int userId = 0;
    DrawerLayout drawer_layout;
    ImageView img_notifications;
    ImageView img_cart;
    public NotificationBadge badge_notification;
    public NotificationBadge badge_cart ;
    public static int count_number = 0;
    public static int cart_number = 0;
    Pusher pusher;
    Channel channel;
    JSONObject jsonObject;
    static int user_id = 0;
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    int user_login = 0;
    protected SharedPreferences sharedPreferences;


    public ToolBar() {
    }

    public ToolBar(Activity activity) {
        this.activity = activity;
        img_notifications = activity.findViewById(R.id.img_notifications);
        img_cart = activity.findViewById(R.id.img_cart);
        badge_notification = activity.findViewById(R.id.badge_notification);
        badge_cart = activity.findViewById(R.id.badge_cart);
        initToolBar();
        openActivities();
        getUserID();
        notification();
    } // constructor

    public String convertToArabic(String value) {
        String newValue = ((((((((((((value + "")
                .replaceAll("1", "١"))
                .replaceAll("2", "٢"))
                .replaceAll("3", "٣")).replaceAll("4", "٤"))
                .replaceAll("5", "٥")).replaceAll("6", "٦"))
                .replaceAll("7", "٧")).replaceAll("8", "٨"))
                .replaceAll("9", "٩")).replaceAll("0", "٠")));

        return newValue;
    } // convert arabic date to english to send to api

    public int getUserID() {

        SharedPreferences prefs = activity.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        //user_login = prefs.getString("id", "0");
        user_login = prefs.getInt("id", 0);
        Log.i("Print", "user_login : " + user_login);
        return user_id;
    } // function get the id of user to send it to api

    public void notification() {

        Log.i("Print", "notification FUNCTION");
        PusherOptions options = new PusherOptions();
        options.setCluster("eu");
        pusher = new Pusher("3fad1753101d0c9ac6dd", options);

        channel = pusher.subscribe("my-channel");

        Log.i("Print", "pusher : " + channel.getName());

        channel.bind("my-event", new SubscriptionEventListener() {

            @Override
            public void onEvent(String channelName, String eventName, final String data) {

                Log.i("Print", "pusherrrrrr on event : " + data.equals("counter"));
                Log.i("Print", "data =  : " + data);

                try {

                    jsonObject = new JSONObject(data);
                    user_id = jsonObject.getInt("user_id");


                    Log.i("Print", "jsonObject   : " + jsonObject.getString("counter"));
                    Log.i("Print", "count_cart   : " + jsonObject.getString("count_cart"));

                    try {

                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                Log.i("Print", user_id + " : " + user_login);

                                if (user_id == user_login) {

                                    try {

                                        count_number = Integer.parseInt(jsonObject.getString("counter"));
                                        cart_number = Integer.parseInt(jsonObject.getString("count_cart"));

                                        Log.i("Print", "cart_number ::" + " : " + cart_number);

                                        Log.i("Print", "count_number ::" + " : " + Integer.parseInt(convertToArabic(String.valueOf(count_number))));

                                        sharedPreferences = activity.getSharedPreferences("user", MODE_PRIVATE);

                                        if (sharedPreferences.getString("language", "ar").equals("ar")) {

                                            badge_notification.setNumber(Integer.parseInt(convertToArabic(String.valueOf(count_number))));
                                            badge_cart.setNumber(Integer.parseInt(convertToArabic(String.valueOf(cart_number))));
                                            Log.i("Print", "arrrabic : ");
                                            Log.i("Print", "number in arabic : " + convertToArabic(String.valueOf(count_number)));

                                        } else if (sharedPreferences.getString("language", "ar").equals("en")) {
                                            badge_notification.setNumber(count_number);
                                            badge_cart.setNumber(cart_number);
                                            Log.i("Print", "ennnnglish : ");

                                        }
                                        //  badge_notification.setNumber(count_number);
                                        ShortcutBadger.applyCount(activity, count_number); //for 1.1.4+

                                        SharedPreferences notifi_count = activity.getSharedPreferences("notification_pusher", Activity.MODE_PRIVATE);
                                        SharedPreferences.Editor editor = notifi_count.edit();
                                        editor.putInt("notifi_count", count_number);
                                        editor.putInt("cart_count", cart_number);
                                        editor.commit();

                                        Log.i("Print", "notification count   : " + count_number);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        Log.i("Print", "notification exception   : " + e.getMessage());

                                    }

                                }
                                // Stuff that updates the UI
                            }
                        });
                    } catch (Exception e) {
                        Log.i("Print", "notification exception   : " + e.getMessage());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.i("Print", "notification exception   : " + e.getMessage());

                }
            }

        });

        pusher.connect();

        img_notifications.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent notification = new Intent(activity, NotificationActivity.class);
                activity.startActivity(notification);
                //context.finish();
            }
        }); // intent to notification activity

    } // function handle notification icon on toolbar

    public void addNotification() {

        SharedPreferences notifi_count = activity.getSharedPreferences("notification_pusher", Activity.MODE_PRIVATE);
        int myIntValue = notifi_count.getInt("notifi_count", 0);
        int cartNumberValue = notifi_count.getInt("cart_count", 0);
        Log.i("Print", "myIntValue : " + myIntValue);

        sharedPreferences = activity.getSharedPreferences("user", MODE_PRIVATE);

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            badge_notification.setNumber(Integer.parseInt(convertToArabic(String.valueOf(myIntValue))));
            badge_cart.setNumber(Integer.parseInt(convertToArabic(String.valueOf(cartNumberValue))));

            Log.i("Print", "arrr : ");
            Log.i("Print", "number in arabic : " + convertToArabic(String.valueOf(myIntValue)));


        } else if (sharedPreferences.getString("language", "ar").equals("en")) {
            badge_notification.setNumber(myIntValue);
            badge_cart.setNumber(cartNumberValue);
            Log.i("Print", "ennn : ");

        }

        ShortcutBadger.applyCount(activity, myIntValue); //for 1.1.4+
        Log.i("Print", "send notifications number : " + myIntValue);

    } // add notification count

    public void setTitle(String title) {
        this.title = title;
        toolbar_title.setText(title);

    } // function of setTitle

    public void openActivities() {

        img_notifications.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent notification = new Intent(activity, NotificationActivity.class);
                activity.startActivity(notification);
                Log.i("Print", "notifications clicked !");
            }
        });

        img_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cart = new Intent(activity, CartActivity.class);
                activity.startActivity(cart);
                Log.i("Print", "cart clicked !");
            }
        });

    }

    public void setColor(String color) {
        toolbar.setBackgroundColor(Color.parseColor(color));
    } // function of seColor

    private void initToolBar() {
        toolbar = activity.findViewById(R.id.tool_bar);
        toolbar_title = activity.findViewById(R.id.toolbar_title);
        drawer_layout = activity.findViewById(R.id.drawer_layout);
        //  relative_notification = activity.findViewById(R.id.relative_notification);
       // img_cart = activity.findViewById(R.id.img_cart);
        ((AppCompatActivity) (activity)).setSupportActionBar(toolbar);
        ActionBar actionBar = ((AppCompatActivity) (activity)).getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);


        if (((AppCompatActivity) (activity)).getSupportActionBar() != null) {
            ((AppCompatActivity) (activity)).getSupportActionBar().setDisplayShowTitleEnabled(false);

            if (drawer_layout != null) {
                ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(activity, drawer_layout, toolbar, R.string.navigation_drawer_open,
                        R.string.navigation_drawer_close);
                drawer_layout.addDrawerListener(toggle);
                toggle.syncState();
            } else {
                toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        activity.onBackPressed();
                    }
                });
            }
        }
    } // function of initToolBar

    public int getPrefUserId() {
        SharedPreferences preferences = activity.getSharedPreferences("MyPrefsFile", Context.MODE_PRIVATE);

        try {
            if (preferences != null)
                userId = preferences.getInt("id", 0);
            Log.i("Print", "User id" + userId);

        } catch (Exception e) {
            Log.i("Print", "Exception" + e.getMessage());
        }

        return userId;
    } // function of getPrefUserId

    public void onDestroy() {
        if (activity != null) {
            activity = null;
        }

        Thread.interrupted();
        if (pusher != null) {
            pusher.disconnect();
            pusher.unsubscribe("my-channel");
        }
    }

} // class of ToolBar
