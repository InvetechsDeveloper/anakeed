package com.invetechs.anakeed.AppConfig;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.invetechs.anakeed.View.Activity.LoginActivity;

public class CheckUserLogin {

    // variables

    private SharedPreferences pref;
    private String isLogin = "";
    private int userId;
    private static final String MY_PREFS_NAME = "MyPrefsFile";

    public boolean check(Context context) {
        pref = context.getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE);
        if (pref != null) {

            isLogin = pref.getString("isLogin", "false");
            userId = pref.getInt("id", 0);
            if (userId == 0 || isLogin.equals("false")) {
                return false;
            }
            return true;
        }
        return true;
    } // check if user already login

    public void openLoginActivity(Context context) {
        Intent login = new Intent(context, LoginActivity.class);
        context.startActivity(login);
    } // open login activity


}
