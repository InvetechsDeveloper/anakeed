package com.invetechs.anakeed.AppConfig;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.TextView;

import com.invetechs.anakeed.R;

import androidx.constraintlayout.widget.ConstraintLayout;

import static android.content.Context.MODE_PRIVATE;

public class ShowDialog {

    // variables
    Dialog dialog;
    TextView messageShow;
    ConstraintLayout layout;

    private static int getScreenWidth(Activity activity) {
        Point size = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(size);
        return size.x;
    } // function getScreenWidth

    public void initDialog(String message, Context context) {
        dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog);
        layout = dialog.findViewById(R.id.constrainLayoutDialog);
        layout.setBackgroundResource(R.color.colorPrimaryDark);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setLayout((int) (getScreenWidth((Activity) context) * .9), ViewGroup.LayoutParams.WRAP_CONTENT);
        try {
            dialog.show();

        } catch (Exception e) {
            Log.i("Print", "exception : " + e.toString());

        }
        messageShow = dialog.findViewById(R.id.dialog_message);
        messageShow.setAllCaps(false);

        SharedPreferences sharedPreferences = context.getSharedPreferences("user", MODE_PRIVATE);

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/ElMessiri-Regular.ttf");
            messageShow.setTypeface(font);
            messageShow.setText(message);

        } else if (sharedPreferences.getString("language", "ar").equals("en")) {
            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Regular.ttf");
            messageShow.setTypeface(font);
            messageShow.setText(message);

        }

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                dialog.dismiss();
                // Toast.makeText(getContext(), getString(R.string.enterVaildDate), Toast.LENGTH_SHORT).show();
            }
        }, 2000);
    }

    public void dismissDialog()
    {
        dialog.dismiss();
    }

}
