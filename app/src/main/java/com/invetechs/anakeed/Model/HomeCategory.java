package com.invetechs.anakeed.Model;

import java.io.Serializable;
import java.util.List;
import java.util.Observable;

public class HomeCategory extends Observable {

    int  slectedPosition , id;
    String categoryImage , categoryEnTitle , categoryArTitle , backgroundColor ;

    List<HomeCategory> categoryList ;

    private  static  HomeCategory instance ;
    public static  HomeCategory getInstance() {
        if(instance == null)
            instance = new HomeCategory();
        return instance;
    }        // singelton

    private HomeCategory(){}

    public HomeCategory(String backgroundColor, String categoryImage, String categoryEnTitle, String categoryArTitle) {
        this.backgroundColor = backgroundColor;
        this.categoryImage = categoryImage;
        this.categoryEnTitle = categoryEnTitle;
        this.categoryArTitle = categoryArTitle;
     }

    public HomeCategory(String backgroundColor, String categoryImage, String categoryEnTitle, String categoryArTitle , int id) {
        this.backgroundColor = backgroundColor;
        this.categoryImage = categoryImage;
        this.categoryEnTitle = categoryEnTitle;
        this.categoryArTitle = categoryArTitle;
        this.id = id;
    }

    public int getId() {
        setChanged();
        return id;
    }

    public void setId(int id) {
        setChanged();
        this.id = id;
    }

    public String getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(String backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public String getCategoryImage() {
        return categoryImage;
    }

    public void setCategoryImage(String categoryImage) {
        this.categoryImage = categoryImage;
    }

    public String getCategoryEnTitle() {
        return categoryEnTitle;
    }

    public void setCategoryEnTitle(String categoryEnTitle) {
        this.categoryEnTitle = categoryEnTitle;
    }

    public String getCategoryArTitle() {
        return categoryArTitle;
    }

    public void setCategoryArTitle(String categoryArTitle) {
        this.categoryArTitle = categoryArTitle;
    }

    public int getSlectedPosition() {
        setChanged();
        return slectedPosition;
    }

    public void setSlectedPosition(int slectedPosition) {
        setChanged();
        this.slectedPosition = slectedPosition;
    }

    public List<HomeCategory> getCategoryList() {
        setChanged();
        return categoryList;
    }

    public void setCategoryList(List<HomeCategory> categoryList) {
        setChanged();
        this.categoryList = categoryList;
    }

} // class of HomeCategory



