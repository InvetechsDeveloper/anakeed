package com.invetechs.anakeed.Model;

import java.util.List;
import java.util.Observable;

public class HomeCategoryResultModel extends Observable
{

    private  static  HomeCategoryResultModel instance ;
    public static  HomeCategoryResultModel getInstance() {
        if(instance == null)
            instance = new HomeCategoryResultModel();
        return instance;
    }        // singelton

    private HomeCategoryResultModel(){}

    private String Status;
    private String message;
    private List<CategoriesBean> Categories;

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<CategoriesBean> getCategories() {
        setChanged();
        return Categories;
    }

    public void setCategories(List<CategoriesBean> Categories) {
        setChanged();
        this.Categories = Categories;
    }

    public static class CategoriesBean extends Observable{

        private int id , slectedPosition;
        private String ar_title;
        private String en_title;
        private String img;
        private String color;

        private  static  CategoriesBean instance ;
        public static  CategoriesBean getInstance() {
            if(instance == null)
                instance = new CategoriesBean();
            return instance;
        }        // singelton

        public int getId() {
            setChanged();
            return id;
        }

        public void setId(int id) {
            setChanged();
            this.id = id;
        }

        public String getAr_title() {
            return ar_title;
        }

        public void setAr_title(String ar_title) {
            this.ar_title = ar_title;
        }

        public String getEn_title() {
            return en_title;
        }

        public void setEn_title(String en_title) {
            this.en_title = en_title;
        }

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public int getSlectedPosition() {
            setChanged();
            return slectedPosition;
        }

        public void setSlectedPosition(int slectedPosition) {
            setChanged();
            this.slectedPosition = slectedPosition;
        }
    }
} // class of HomeCategoryResultModel
