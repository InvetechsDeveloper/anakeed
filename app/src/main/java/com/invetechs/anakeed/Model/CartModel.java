package com.invetechs.anakeed.Model;

import java.util.ArrayList;

public class CartModel {

    private String enTitle , arTitle , notes;
    private  int amount , id , productId;
    private  double price , total ;

    public  CartModel()
    {} // default Constructor

    public CartModel(int id ,String enTitle, String arTitle, String notes, int amount , double price) {
        this.enTitle = enTitle;
        this.arTitle = arTitle;
        this.notes = notes;
        this.amount = amount;
        this.id = id;
        this.price = price;
        this.total = total;
    }

    public CartModel(int id, String enTitle, String arTitle, String notes, int amount, double price, int productId) {
        this.enTitle = enTitle;
        this.arTitle = arTitle;
        this.notes = notes;
        this.amount = amount;
        this.price = price;
        this.id = id;
        this.productId = productId;
    } // Parma Constructor

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEnTitle() {
        return enTitle;
    }

    public void setEnTitle(String enTitle) {
        this.enTitle = enTitle;
    }

    public String getArTitle() {
        return arTitle;
    }

    public void setArTitle(String arTitle) {
        this.arTitle = arTitle;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int kitchenId) {
        this.productId = kitchenId;
    }

    public double getTotal() {
        return price*amount;
    }

    public void setTotal(double total) {
        this.total = total;
    }



}
