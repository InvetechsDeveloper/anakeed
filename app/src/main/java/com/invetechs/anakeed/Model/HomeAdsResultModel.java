package com.invetechs.anakeed.Model;

import java.util.List;

public class HomeAdsResultModel {

    private String Status;
    private String message;
    private List<SlidersBean> Sliders;

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<SlidersBean> getSliders() {
        return Sliders;
    }

    public void setSliders(List<SlidersBean> Sliders) {
        this.Sliders = Sliders;
    }

    public static class SlidersBean {

        private int id;
        private String img;
        private String confirm;
        private String created_by;
        private Object updated_by;
        private String created_at;
        private String updated_at;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }

        public String getConfirm() {
            return confirm;
        }

        public void setConfirm(String confirm) {
            this.confirm = confirm;
        }

        public String getCreated_by() {
            return created_by;
        }

        public void setCreated_by(String created_by) {
            this.created_by = created_by;
        }

        public Object getUpdated_by() {
            return updated_by;
        }

        public void setUpdated_by(Object updated_by) {
            this.updated_by = updated_by;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }
    }
}
