package com.invetechs.anakeed.ViewModel;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.annotation.NonNull;
import android.util.Log;

import com.invetechs.anakeed.Model.HomeAdsResultModel;
import com.invetechs.anakeed.Model.HomeCategoryResultModel;
import com.invetechs.anakeed.Retrofit.Categories.HomeAdsApi;
import com.invetechs.anakeed.Retrofit.Categories.HomeCategoryApi;
import com.invetechs.anakeed.Retrofit.connection.RetrofitClient;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

public class HomeCategoryViewModel extends AndroidViewModel {

    private MutableLiveData<HomeCategoryResultModel> response = new MutableLiveData<>();
    private MutableLiveData<HomeAdsResultModel> response2 = new MutableLiveData<>();
    private MutableLiveData<Integer> checkIfRequestCompleted = new MutableLiveData<>();
    private int counter = 0;
    public HomeCategoryViewModel(@NonNull Application application) {
        super(application);
    }
    public LiveData<HomeCategoryResultModel> fetchHomeCategories() {

        try {
            Retrofit retrofit = RetrofitClient.getInstance();
            HomeCategoryApi homeCategoryApi = retrofit.create(HomeCategoryApi.class);
            CompositeDisposable compositeDisposable = new CompositeDisposable();
            compositeDisposable.add(homeCategoryApi
                    .getHomeCategories()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Consumer<HomeCategoryResultModel>() {
                        @Override
                        public void accept(HomeCategoryResultModel categoryResultModel) throws Exception {
                            response.setValue(categoryResultModel);
                            checkIfRequestCompleted.setValue(counter++);
                        }
                    }));
        }
        catch (Exception e) {
            Log.e("Print", e.toString());
        }
        return response;
    } // function of fetchHomeCategories

    public LiveData<HomeAdsResultModel> fetchAds() {

        try {

            Retrofit retrofit = RetrofitClient.getInstance();
            HomeAdsApi homeAdsApi = retrofit.create(HomeAdsApi.class);

            try {
                CompositeDisposable compositeDisposable = new CompositeDisposable();
                compositeDisposable.add(homeAdsApi.getAds()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Consumer<HomeAdsResultModel>() {
                            @Override
                            public void accept(HomeAdsResultModel homeAdsResultModel) throws Exception {
                                counter = 0;
                                response2.setValue(homeAdsResultModel);
                                checkIfRequestCompleted.setValue(counter++);
                            }
                        }));
            }
            catch (Exception e)
            {
                e.printStackTrace();
                Log.e("Print", e.toString());
            }

        } catch (Exception e) {
            Log.e("Print", e.toString());
        }

        return response2;
    } // function of fetchAds


    public LiveData<Integer> check() {
        return checkIfRequestCompleted;
    } // function of check if request finish

} // class of HomeCategoryViewModel
