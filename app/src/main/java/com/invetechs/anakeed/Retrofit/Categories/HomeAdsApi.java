package com.invetechs.anakeed.Retrofit.Categories;

import com.invetechs.anakeed.Model.HomeAdsResultModel;

import io.reactivex.Observable;
import retrofit2.http.GET;

public interface HomeAdsApi
{
    @GET("Sliders")
    Observable<HomeAdsResultModel> getAds();



}
