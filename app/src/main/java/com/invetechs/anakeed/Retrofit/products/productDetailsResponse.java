package com.invetechs.anakeed.Retrofit.products;

public class productDetailsResponse {


    private String code;
    private ProductBean Product;
    private String Status;
    private String message;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public ProductBean getProduct() {
        return Product;
    }

    public void setProduct(ProductBean Product) {
        this.Product = Product;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static class ProductBean {
        /**
         * id : 2
         * ar_title : فراولة
         * en_title : jioj
         * ar_description : فواكه من خير مزارعنا
         * en_description : oijoij
         * img : jioj
         * color : joij
         * ar_weight1 : 10 كجم
         * ar_weight2 : 10 كجم
         * en_weight1 : 10 كجم
         * en_weight2 : 10 كجم
         * price1 : 20
         * confirm : 1
         * cat_id : 1
         * created_by : 1
         * updated_by : huih
         * created_at : null
         * updated_at : null
         */

        private int id;
        private String ar_title;
        private String en_title;
        private String ar_description;
        private String en_description;
        private String img;
        private String color;
        private String ar_weight1;
        private String ar_weight2;
        private String en_weight1;
        private String en_weight2;
        private int price1;
        private String confirm;
        private String cat_id;
        private String created_by;
        private String updated_by;
        private Object created_at;
        private Object updated_at;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getAr_title() {
            return ar_title;
        }

        public void setAr_title(String ar_title) {
            this.ar_title = ar_title;
        }

        public String getEn_title() {
            return en_title;
        }

        public void setEn_title(String en_title) {
            this.en_title = en_title;
        }

        public String getAr_description() {
            return ar_description;
        }

        public void setAr_description(String ar_description) {
            this.ar_description = ar_description;
        }

        public String getEn_description() {
            return en_description;
        }

        public void setEn_description(String en_description) {
            this.en_description = en_description;
        }

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public String getAr_weight1() {
            return ar_weight1;
        }

        public void setAr_weight1(String ar_weight1) {
            this.ar_weight1 = ar_weight1;
        }

        public String getAr_weight2() {
            return ar_weight2;
        }

        public void setAr_weight2(String ar_weight2) {
            this.ar_weight2 = ar_weight2;
        }

        public String getEn_weight1() {
            return en_weight1;
        }

        public void setEn_weight1(String en_weight1) {
            this.en_weight1 = en_weight1;
        }

        public String getEn_weight2() {
            return en_weight2;
        }

        public void setEn_weight2(String en_weight2) {
            this.en_weight2 = en_weight2;
        }

        public int getPrice1() {
            return price1;
        }

        public void setPrice1(int price1) {
            this.price1 = price1;
        }

        public String getConfirm() {
            return confirm;
        }

        public void setConfirm(String confirm) {
            this.confirm = confirm;
        }

        public String getCat_id() {
            return cat_id;
        }

        public void setCat_id(String cat_id) {
            this.cat_id = cat_id;
        }

        public String getCreated_by() {
            return created_by;
        }

        public void setCreated_by(String created_by) {
            this.created_by = created_by;
        }

        public String getUpdated_by() {
            return updated_by;
        }

        public void setUpdated_by(String updated_by) {
            this.updated_by = updated_by;
        }

        public Object getCreated_at() {
            return created_at;
        }

        public void setCreated_at(Object created_at) {
            this.created_at = created_at;
        }

        public Object getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(Object updated_at) {
            this.updated_at = updated_at;
        }
    }
}
