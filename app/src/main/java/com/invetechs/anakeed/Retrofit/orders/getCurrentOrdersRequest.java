package com.invetechs.anakeed.Retrofit.orders;

import com.invetechs.anakeed.Retrofit.profile.profileResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface getCurrentOrdersRequest {

    @GET("GetCurrentOrders")
    Call<ordersResponse> getCurrentOrders(@Query("id") int id);

} // get current orders
