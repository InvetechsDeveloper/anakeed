package com.invetechs.anakeed.Retrofit.ads;

import retrofit2.Call;
import retrofit2.http.GET;

public interface adsRequest {

    @GET("Sliders")
    Call<adsResponse> getAdvertisments();
}
