package com.invetechs.anakeed.Retrofit.products;

import java.util.List;

public class productResponse {

    private String code;
    private String Status;
    private String message;
    private List<ProductBean> Product;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ProductBean> getProduct() {
        return Product;
    }

    public void setProduct(List<ProductBean> Product) {
        this.Product = Product;
    }

    public static class ProductBean {

        private int id;
        private String ar_title;
        private String en_title;
        private String ar_description;
        private String en_description;
        private String img;
        private String color;
        private String ar_weight1;
        private String ar_weight2;
        private String en_weight1;
        private String en_weight2;
        private int price1;
        private String price2;
        private String confirm;
        private String cat_id;
        private String created_by;
        private String updated_by;
        private String created_at;
        private String updated_at;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getAr_title() {
            return ar_title;
        }

        public void setAr_title(String ar_title) {
            this.ar_title = ar_title;
        }

        public String getEn_title() {
            return en_title;
        }

        public void setEn_title(String en_title) {
            this.en_title = en_title;
        }

        public String getAr_description() {
            return ar_description;
        }

        public void setAr_description(String ar_description) {
            this.ar_description = ar_description;
        }

        public String getEn_description() {
            return en_description;
        }

        public void setEn_description(String en_description) {
            this.en_description = en_description;
        }

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public String getAr_weight1() {
            return ar_weight1;
        }

        public void setAr_weight1(String ar_weight1) {
            this.ar_weight1 = ar_weight1;
        }

        public String getAr_weight2() {
            return ar_weight2;
        }

        public void setAr_weight2(String ar_weight2) {
            this.ar_weight2 = ar_weight2;
        }

        public String getEn_weight1() {
            return en_weight1;
        }

        public void setEn_weight1(String en_weight1) {
            this.en_weight1 = en_weight1;
        }

        public String getEn_weight2() {
            return en_weight2;
        }

        public void setEn_weight2(String en_weight2) {
            this.en_weight2 = en_weight2;
        }

        public int getPrice1() {
            return price1;
        }

        public void setPrice1(int price1) {
            this.price1 = price1;
        }

        public String getPrice2() {
            return price2;
        }

        public void setPrice2(String price2) {
            this.price2 = price2;
        }

        public String getConfirm() {
            return confirm;
        }

        public void setConfirm(String confirm) {
            this.confirm = confirm;
        }

        public String getCat_id() {
            return cat_id;
        }

        public void setCat_id(String cat_id) {
            this.cat_id = cat_id;
        }

        public String getCreated_by() {
            return created_by;
        }

        public void setCreated_by(String created_by) {
            this.created_by = created_by;
        }

        public String getUpdated_by() {
            return updated_by;
        }

        public void setUpdated_by(String updated_by) {
            this.updated_by = updated_by;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }
    }
}
