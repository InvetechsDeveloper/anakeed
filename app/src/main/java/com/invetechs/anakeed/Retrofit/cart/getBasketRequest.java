package com.invetechs.anakeed.Retrofit.cart;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface getBasketRequest {

    @GET("GetBasket")
    Call<getBasketResponse> getBasket(@Query("id") int user_id ,
                                            @Query("lat") double lat ,
                                            @Query("lang") double lang  ,
                                            @Query("address") String address  ,
                                            @Query("name") String name );
}
