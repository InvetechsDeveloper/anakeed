package com.invetechs.anakeed.Retrofit.profile;

import com.invetechs.anakeed.Retrofit.auth.login.loginResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface updateProfileApi {

    @FormUrlEncoded
    @POST("UpdateProfileUser")
    Call<profileResponse> updateProfile(
            @Field("id") int id,
            @Field("phone") String mobilenumber,
            @Field("name") String user_name,
            @Field("password") String password

    ); // name and file ----> without Image
}  // update profile data for user
