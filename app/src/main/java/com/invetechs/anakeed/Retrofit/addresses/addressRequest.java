package com.invetechs.anakeed.Retrofit.addresses;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface addressRequest {

    @GET("get_user_address")
    Call<addressResponse> getUserAddresses(
            @Query("id") int id
    ); // get all user addresses


    @FormUrlEncoded
    @POST("insertAddressesByUser")
    Call<addressResponse> addNewAddress(@Field("user_id") int user_id ,
                                        @Field("address") String address,
                                        @Field("lat") double lat,
                                        @Field("lng") double lng ,
                                        @Field("name") String name
    ); // add new address


    @FormUrlEncoded
    @POST("updateAddressesByUser")
    Call<addressResponse> updateUserAddress(@Field("id") int address_id ,
                                            @Field("user_id") int user_id ,
                                            @Field("address") String address,
                                            @Field("lat") double lat,
                                            @Field("lng") double lng ,
                                            @Field("name") String name
                                        //    @Field("note") String note
    ); // update address

    @FormUrlEncoded
    @POST("deleteAddressesByUser")
    Call<addressResponse> deleteUserAddress(@Field("id") int address_id); // delete UserAddresse
}
