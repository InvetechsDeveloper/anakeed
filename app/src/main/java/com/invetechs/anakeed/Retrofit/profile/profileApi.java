package com.invetechs.anakeed.Retrofit.profile;

import com.invetechs.anakeed.Retrofit.auth.login.loginResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface profileApi {

    @GET("GetUserProfile")
    Call<profileResponse> show_profile(@Query("id") int id);

} // get user profile data
