package com.invetechs.anakeed.Retrofit.notifications;

import java.util.List;

public class NotificationResponse {

    private String code;
    private String Status;
    private String message;
    private List<NotificaionsBean> notificaions;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<NotificaionsBean> getNotificaions() {
        return notificaions;
    }

    public void setNotificaions(List<NotificaionsBean> notificaions) {
        this.notificaions = notificaions;
    }

    public static class NotificaionsBean {

        private int id;
        private String ar_title;
        private String en_title;
        private int order_id;
        private int user_id;
        private int status;
        private String created_at;
        private String updated_at;
        private String ar_time;
        private String en_time;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getAr_title() {
            return ar_title;
        }

        public void setAr_title(String ar_title) {
            this.ar_title = ar_title;
        }

        public String getEn_title() {
            return en_title;
        }

        public void setEn_title(String en_title) {
            this.en_title = en_title;
        }

        public int getOrder_id() {
            return order_id;
        }

        public void setOrder_id(int order_id) {
            this.order_id = order_id;
        }

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public String getAr_time() {
            return ar_time;
        }

        public void setAr_time(String ar_time) {
            this.ar_time = ar_time;
        }

        public String getEn_time() {
            return en_time;
        }

        public void setEn_time(String en_time) {
            this.en_time = en_time;
        }
    }
}
