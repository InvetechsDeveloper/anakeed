package com.invetechs.anakeed.Retrofit.orders;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface getOrderDetails {


    @GET("GetOrderById")
    Call<orderDetailsResponse> getDetailsOrder(@Query("id") String id);

} // get order details
