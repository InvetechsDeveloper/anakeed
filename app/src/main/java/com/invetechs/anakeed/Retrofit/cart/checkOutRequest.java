package com.invetechs.anakeed.Retrofit.cart;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface checkOutRequest {

    @POST("ChechOut")
    Call<getBasketResponse> checkOut(@Query("id") int user_id ,
                                      @Query("lat") double lat ,
                                      @Query("lang") double lang );

}
