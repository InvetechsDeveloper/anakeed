package com.invetechs.anakeed.Retrofit.auth.verification;

import com.invetechs.anakeed.Retrofit.auth.login.loginResponse;

import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface verifyApiRequest {

    @FormUrlEncoded
    @POST("verify_verificationcode")
    retrofit2.Call<loginResponse> verify(@Field("user_id") int user_id,
                                         @Field("token") String token
    );  // user VerifyAPi

}
