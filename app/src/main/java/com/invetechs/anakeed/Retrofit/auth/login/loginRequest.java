package com.invetechs.anakeed.Retrofit.auth.login;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface loginRequest {

    @FormUrlEncoded
    @POST("login")
    Call<loginResponse> login(
            @Field("phone") String mobile_number,
            @Field("password") String password
    );

}
