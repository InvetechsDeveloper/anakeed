package com.invetechs.anakeed.Retrofit.connection;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {

    static Retrofit retrofitInstance;
    static Gson gson;
    static String token = "";
    static OkHttpClient.Builder httpClient;
    static final String MY_PREFS_NAME = "MyPrefsFile";

    public RetrofitClient() {
    } // default constructor

//    public static Retrofit getInstance()
//    {
//        gson = new GsonBuilder()
//                .setLenient()
//                .create();
//
//        if(retrofitInstance == null) {
//            retrofitInstance = new Retrofit.Builder()
//                    .baseUrl("http://192.168.1.4/anakid/public/api/")
//                    .addConverterFactory(GsonConverterFactory.create())
//                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
//                    .build();
//        }
//
//        return retrofitInstance;
//    } // function of getInstance

    public static Retrofit getInstance() {

        if (httpClient == null) {
            httpClient = new OkHttpClient.Builder();
//            httpClient.addInterceptor(new Interceptor() {
//                @Override
//                public Response intercept(Interceptor.Chain chain) throws IOException {
//
//                    Request original = chain.request();
//
//                    Request request = original.newBuilder()
//                            .header("Authorization", "Basic  " + token)
//                            .header("Accept", "application/json")
//                            .header("Content-Type", "application/x-www-form-urlencoded")
//                            .method(original.method(), original.body())
//                            .build();
//                        return chain.proceed(request);
//
//                }
//            });
        }
        //OkHttpClient client = httpClient.build();

        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(100, TimeUnit.SECONDS)
                .readTimeout(100,TimeUnit.SECONDS).build();


        gson = new GsonBuilder()
                .setLenient()
                .create();

        if (retrofitInstance == null) {
            retrofitInstance = new Retrofit.Builder()
                    .baseUrl("http://3nakied.invetechs.com/api/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create(new  Gson()))
                    .build();

        }

        return retrofitInstance;
    } // function of connectWith


} // class of RetrofitClient
