package com.invetechs.anakeed.Retrofit.orders;

import java.util.List;

public class orderDetailsResponse {

    private String code;
    private String Status;
    private OrderBean order;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public OrderBean getOrder() {
        return order;
    }

    public void setOrder(OrderBean order) {
        this.order = order;
    }

    public static class OrderBean {

        private int id;
        private int User_id;
        private double total_price;
        private double total;
        private double delivery;
        private double added_taxes;
        private String type;
        private int address_id;
        private String created_at;
        private String updated_at;
        private String data;
        private String time;
        private String address;
        private List<ProductsBean> products;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getUser_id() {
            return User_id;
        }

        public void setUser_id(int User_id) {
            this.User_id = User_id;
        }

        public double getTotal_price() {
            return total_price;
        }

        public void setTotal_price(int total_price) {
            this.total_price = total_price;
        }

        public double getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public double getDelivery() {
            return delivery;
        }

        public void setDelivery(int delivery) {
            this.delivery = delivery;
        }

        public double getAdded_taxes() {
            return added_taxes;
        }

        public void setAdded_taxes(int added_taxes) {
            this.added_taxes = added_taxes;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public int getAddress_id() {
            return address_id;
        }

        public void setAddress_id(int address_id) {
            this.address_id = address_id;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public String getData() {
            return data;
        }

        public void setData(String data) {
            this.data = data;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public List<ProductsBean> getProducts() {
            return products;
        }

        public void setProducts(List<ProductsBean> products) {
            this.products = products;
        }

        public static class ProductsBean {

            private int id;
            private String ar_title;
            private String en_title;
            private String ar_description;
            private String en_description;
            private String img;
            private String color;
            private String ar_weight1;
            private String ar_weight2;
            private String en_weight1;
            private String en_weight2;
            private String price1;
            private String price2;
            private String confirm;
            private String cat_id;
            private String created_by;
            private String updated_by;
            private Object created_at;
            private Object updated_at;
            private int amount;
            private PivotBean pivot;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getAr_title() {
                return ar_title;
            }

            public void setAr_title(String ar_title) {
                this.ar_title = ar_title;
            }

            public String getEn_title() {
                return en_title;
            }

            public void setEn_title(String en_title) {
                this.en_title = en_title;
            }

            public String getAr_description() {
                return ar_description;
            }

            public void setAr_description(String ar_description) {
                this.ar_description = ar_description;
            }

            public String getEn_description() {
                return en_description;
            }

            public void setEn_description(String en_description) {
                this.en_description = en_description;
            }

            public String getImg() {
                return img;
            }

            public void setImg(String img) {
                this.img = img;
            }

            public String getColor() {
                return color;
            }

            public void setColor(String color) {
                this.color = color;
            }

            public String getAr_weight1() {
                return ar_weight1;
            }

            public void setAr_weight1(String ar_weight1) {
                this.ar_weight1 = ar_weight1;
            }

            public String getAr_weight2() {
                return ar_weight2;
            }

            public void setAr_weight2(String ar_weight2) {
                this.ar_weight2 = ar_weight2;
            }

            public String getEn_weight1() {
                return en_weight1;
            }

            public void setEn_weight1(String en_weight1) {
                this.en_weight1 = en_weight1;
            }

            public String getEn_weight2() {
                return en_weight2;
            }

            public void setEn_weight2(String en_weight2) {
                this.en_weight2 = en_weight2;
            }

            public String getPrice1() {
                return price1;
            }

            public void setPrice1(String price1) {
                this.price1 = price1;
            }

            public String getPrice2() {
                return price2;
            }

            public void setPrice2(String price2) {
                this.price2 = price2;
            }

            public String getConfirm() {
                return confirm;
            }

            public void setConfirm(String confirm) {
                this.confirm = confirm;
            }

            public String getCat_id() {
                return cat_id;
            }

            public void setCat_id(String cat_id) {
                this.cat_id = cat_id;
            }

            public String getCreated_by() {
                return created_by;
            }

            public void setCreated_by(String created_by) {
                this.created_by = created_by;
            }

            public String getUpdated_by() {
                return updated_by;
            }

            public void setUpdated_by(String updated_by) {
                this.updated_by = updated_by;
            }

            public Object getCreated_at() {
                return created_at;
            }

            public void setCreated_at(Object created_at) {
                this.created_at = created_at;
            }

            public Object getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(Object updated_at) {
                this.updated_at = updated_at;
            }

            public int getAmount() {
                return amount;
            }

            public void setAmount(int amount) {
                this.amount = amount;
            }

            public PivotBean getPivot() {
                return pivot;
            }

            public void setPivot(PivotBean pivot) {
                this.pivot = pivot;
            }

            public static class PivotBean {
                /**
                 * order_id : 3
                 * product_id : 2
                 * count : 3
                 * created_at : 2019-03-27 00:00:00
                 * updated_at : 2019-03-28 00:00:00
                 */

                private int order_id;
                private int product_id;
                private int count;
                private String created_at;
                private String updated_at;

                public int getOrder_id() {
                    return order_id;
                }

                public void setOrder_id(int order_id) {
                    this.order_id = order_id;
                }

                public int getProduct_id() {
                    return product_id;
                }

                public void setProduct_id(int product_id) {
                    this.product_id = product_id;
                }

                public int getCount() {
                    return count;
                }

                public void setCount(int count) {
                    this.count = count;
                }

                public String getCreated_at() {
                    return created_at;
                }

                public void setCreated_at(String created_at) {
                    this.created_at = created_at;
                }

                public String getUpdated_at() {
                    return updated_at;
                }

                public void setUpdated_at(String updated_at) {
                    this.updated_at = updated_at;
                }
            }
        }
    }
}
