package com.invetechs.anakeed.Retrofit.auth.register;

import com.invetechs.anakeed.Retrofit.auth.login.loginResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface registerRequest {

    @FormUrlEncoded
    @POST("registration")
    Call<loginResponse> register(@Field("name") String name,
                                 @Field("phone") String mobilenumber,
                                 @Field("password") String password);

}
