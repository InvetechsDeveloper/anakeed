package com.invetechs.anakeed.Retrofit.cart;

import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface storeBasketRequest {

    @POST("Store_Basket")
    Call<storeBasketResponse> storeToBasket(@Query("User_id") int user_id ,
                                            @Query("Product_id") String product_id ,
                                            @Query("count") int count );

}
