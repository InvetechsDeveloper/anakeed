package com.invetechs.anakeed.Retrofit.Categories;

import com.invetechs.anakeed.Model.HomeCategoryResultModel;

import io.reactivex.Observable;
import retrofit2.http.GET;

public interface HomeCategoryApi
{
    @GET("Categories")
    Observable<HomeCategoryResultModel> getHomeCategories();
} // interface of HomeCategoryApi
