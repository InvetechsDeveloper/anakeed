package com.invetechs.anakeed.Retrofit.orders;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface getOldOrdersRequest {

    @GET("GetOldOrders")
    Call<ordersResponse> getOldOrders(@Query("id") int id);

} // get current orders
