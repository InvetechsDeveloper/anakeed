package com.invetechs.anakeed.Retrofit.auth.change_password;
import com.invetechs.anakeed.Retrofit.auth.login.loginResponse;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ChangePassword {

    @FormUrlEncoded
    @POST("change_password")
    retrofit2.Call<loginResponse> change_password(@Field("user_id") int user_id,
                                                  @Field("password") String password);  // user change password

}
