package com.invetechs.anakeed.Retrofit.auth.verification;

import com.invetechs.anakeed.Retrofit.auth.login.loginResponse;

import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface verifyForgetPasswordRequest {

    @FormUrlEncoded
    @POST("verify_verificationcode_forget")
    retrofit2.Call<loginResponse> verifyForgetPass(@Field("phone") String mobilenumber,
                                                   @Field("token") String token
    );

}
