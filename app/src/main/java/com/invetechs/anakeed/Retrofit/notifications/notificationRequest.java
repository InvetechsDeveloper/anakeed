package com.invetechs.anakeed.Retrofit.notifications;

import com.invetechs.anakeed.Retrofit.profile.profileResponse;

import retrofit2.Call;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface notificationRequest {

    @POST("notificaion_by_user")
    Call<NotificationResponse> showNotifications(@Query("id") int id);

}
