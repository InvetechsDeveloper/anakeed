package com.invetechs.anakeed.Retrofit.auth.forget_password;

import com.invetechs.anakeed.Retrofit.auth.login.loginResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface forgetPasswordApi {

    @FormUrlEncoded
    @POST("foroget_password")
    Call<forgetResponse> forgetPass(@Field("phone") String mobilenumber);  // forget password

}
