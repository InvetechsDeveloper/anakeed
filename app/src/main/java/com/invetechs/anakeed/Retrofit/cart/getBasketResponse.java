package com.invetechs.anakeed.Retrofit.cart;


import java.util.List;

public class getBasketResponse {

    private String code;
    private String Status;
    private DetailsBean details;
    private List<DataBean> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public DetailsBean getDetails() {
        return details;
    }

    public void setDetails(DetailsBean details) {
        this.details = details;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DetailsBean {

        private int total_invioce;
        private String user_address;
        private double deleviry;
        private double total;
        private double added_values;

        public int getTotal_invioce() {
            return total_invioce;
        }

        public void setTotal_invioce(int total_invioce) {
            this.total_invioce = total_invioce;
        }

        public String getUser_address() {
            return user_address;
        }

        public void setUser_address(String user_address) {
            this.user_address = user_address;
        }

        public double getDeleviry() {
            return deleviry;
        }

        public void setDeleviry(double deleviry) {
            this.deleviry = deleviry;
        }

        public double getTotal() {
            return total;
        }

        public void setTotal(double total) {
            this.total = total;
        }

        public double getAdded_values() {
            return added_values;
        }

        public void setAdded_values(double added_values) {
            this.added_values = added_values;
        }
    }

    public static class DataBean {

        private int id;
        private int User_id;
        private int Product_id;
        private int count;
        private String created_at;
        private String updated_at;
        private int total_price;
        private String ar_title;
        private String en_title;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getUser_id() {
            return User_id;
        }

        public void setUser_id(int User_id) {
            this.User_id = User_id;
        }

        public int getProduct_id() {
            return Product_id;
        }

        public void setProduct_id(int Product_id) {
            this.Product_id = Product_id;
        }

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public int getTotal_price() {
            return total_price;
        }

        public void setTotal_price(int total_price) {
            this.total_price = total_price;
        }

        public String getAr_title() {
            return ar_title;
        }

        public void setAr_title(String ar_title) {
            this.ar_title = ar_title;
        }

        public String getEn_title() {
            return en_title;
        }

        public void setEn_title(String en_title) {
            this.en_title = en_title;
        }
    }
}
