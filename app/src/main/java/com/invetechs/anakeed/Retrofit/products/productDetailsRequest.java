package com.invetechs.anakeed.Retrofit.products;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface productDetailsRequest {

    @GET("Product_by_id")
    Call<productDetailsResponse> getProductDetails(@Query("id") String id);
}
