package com.invetechs.anakeed.Retrofit.notifications;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface sendNotificationRequest {

    @POST("delete_notificaion")
    Call<NotificationResponse> sendNotification(@Query("id") int id);
}
