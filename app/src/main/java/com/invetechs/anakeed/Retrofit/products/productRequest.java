package com.invetechs.anakeed.Retrofit.products;

import com.invetechs.anakeed.Retrofit.profile.profileResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface productRequest {

    @GET("Product")
    Call<productResponse> getProducts(@Query("id") int id);
}
