package com.invetechs.anakeed.Retrofit.cart;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface deleteBasketRequest {

    @GET("deleteBasket")
    Call<getBasketResponse> deleteBasket(@Query("id") int user_id);

}
