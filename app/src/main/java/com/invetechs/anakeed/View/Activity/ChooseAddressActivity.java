package com.invetechs.anakeed.View.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Button;

import com.invetechs.anakeed.Adapter.ChooseAddressAdapter;
import com.invetechs.anakeed.AppConfig.CustomDialogProgress;
import com.invetechs.anakeed.AppConfig.ShowDialog;
import com.invetechs.anakeed.AppConfig.ToolBar;
import com.invetechs.anakeed.R;
import com.invetechs.anakeed.Retrofit.connection.RetrofitClient;
import com.invetechs.anakeed.Retrofit.addresses.addressRequest;
import com.invetechs.anakeed.Retrofit.addresses.addressResponse;

import java.util.ArrayList;
import java.util.List;

public class ChooseAddressActivity extends AppCompatActivity {

    // bind views
    @BindView(R.id.recyclerView_chooseAddresses)
    RecyclerView recyclerView;

    // vars
    ChooseAddressAdapter adapter;
    List<addressResponse.DataBean> addressListApi = new ArrayList<>();
    List<addressResponse.DataBean> addressList = new ArrayList<>();
    Handler handler;
    CustomDialogProgress progress;
    ShowDialog toastDialog;
    ToolBar toolBarConfig;
    Button okChooseAddressButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_address);
        ButterKnife.bind(this);
        okChooseAddressButton = findViewById(R.id.okChooseAddress);
        initRecycelerView(); // intialize reclerview
        getAddressesFromApi(); // get laa addresses of user
        toastDialog = new ShowDialog();
      //  toolBarConfig = new ToolBar(this);
    }

    private void initRecycelerView() {
        adapter = new ChooseAddressAdapter(this, addressList, okChooseAddressButton);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);
    } // intialize list of addresses

    public static int getScreenWidth(Activity activity) {
        Point size = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(size);
        return size.x;
    } // function getScreenWidth

    public int getPrefUserId() {
        int userId = 0;
        SharedPreferences preferences = getSharedPreferences("MyPrefsFile", MODE_PRIVATE);

        try {
            if (preferences != null)
                userId = preferences.getInt("id", 0);

            Log.i("Print", "User id" + userId);
        } catch (Exception e) {
            Log.i("Print", "Exception" + e.getMessage());
        }

        return userId;
    } // function of getPrefUserId

    @SuppressLint("HandlerLeak")
    private void getAddressesFromApi() {
        progress = new CustomDialogProgress();
        progress.init(this);
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
        new Thread() {
            public void run() {

                Retrofit retrofit = RetrofitClient.getInstance();

                final addressRequest addressApi = retrofit.create(addressRequest.class);

                final Call<addressResponse> getInterestConnection = addressApi.getUserAddresses(getPrefUserId());

                getInterestConnection.enqueue(new Callback<addressResponse>() {
                    @Override
                    public void onResponse(Call<addressResponse> call, Response<addressResponse> response) {
                        try {
                            Log.i("Print", "code" + response.body().getCode());

                            if (response.body() != null) {

                                String code = response.body().getCode();

                                Log.i("Print", "code" + code);

                                if (code.equals("200")) {

                                    addressListApi = response.body().getData();
                                    fillLsitWithAddresses();
                                } //

                            } // if response success
                            else {
                                toastDialog.initDialog(getString(R.string.retry), ChooseAddressActivity.this);
                            } // response faild
                            progress.dismiss();

                        } // try
                        catch (Exception e) {
                            Log.i("Print", "exception : " + e.toString());
                            progress.dismiss();
                        } // catch
                    } // onResponse

                    @Override
                    public void onFailure(Call<addressResponse> call, Throwable t) {

                        toastDialog.initDialog(getString(R.string.retry), ChooseAddressActivity.this);
                        Log.i("Print", "error : " + t.toString());
                        progress.dismiss();
                    } // on Failure
                });

            }

        }.start();
    }//function of getAddressesFromApi

    private void fillLsitWithAddresses() {
        for (int i = 0; i < addressListApi.size(); i++) {
            addressList.add(addressListApi.get(i));
        }
        adapter.notifyDataSetChanged();
    } // function of fillListWithAddresses

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Thread.interrupted();
    }

}
