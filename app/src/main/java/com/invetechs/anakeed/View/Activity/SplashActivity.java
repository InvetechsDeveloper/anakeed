package com.invetechs.anakeed.View.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.invetechs.anakeed.R;
import com.skydoves.medal.MedalAnimation;

import butterknife.BindView;
import butterknife.ButterKnife;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SplashActivity extends AppCompatActivity  {

    @BindView(R.id.img_splash)
    ImageView img_splash;

    SharedPreferences.Editor editorAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        startSplash();
        deleteSavedLocation();

    } // function of onCreate

    private void deleteSavedLocation() {
        SharedPreferences address = getApplicationContext().getSharedPreferences("AddressPref", MODE_PRIVATE);
        if (address != null) {
            address.edit().remove("lat").commit();
            address.edit().remove("lng").commit();
            address.edit().remove("address").commit();

      }
        }

    private void startSplash() {

       final MedalAnimation medalAnimation = new MedalAnimation.Builder()
                .setDirection(MedalAnimation.RESTART)
                .setDegreeX(360)
                .setDegreeZ(360)
                .setSpeed(80)
                .setTurn(1)
                .setLoop(1)
                .build();


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                medalAnimation.startAnimation(img_splash);

                Intent intent = new Intent(SplashActivity.this, MenuActivity.class);
                intent.putExtra("fragmentFlag", "home");
                startActivity(intent);
                finish();
            }
        }, 2000);
    } // function of startSplash

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && hasFocus) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                            View.SYSTEM_UI_FLAG_FULLSCREEN |
                            View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION |
                            View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN |
                            View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    } // prevent touch from resize image


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }// apply fonts


} // class of SplashActivity
