package com.invetechs.anakeed.View.Activity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;

import androidx.fragment.app.DialogFragment;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.AppCompatRadioButton;
import android.widget.DatePicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.invetechs.anakeed.Language.MyContextWrapper;
import com.invetechs.anakeed.R;
import com.invetechs.anakeed.View.Fragment.DatepickerFragment;

import java.text.DateFormat;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class PaymentActivity extends AppCompatActivity
        implements DatePickerDialog.OnDateSetListener {


    // bind views
    @BindView(R.id.radio_group)
    RadioGroup radio_group;

    @BindView(R.id.rb_upon_receipt)
    RadioButton rb_upon_receipt;

    @BindView(R.id.rb_mada)
    RadioButton rb_mada;

    @BindView(R.id.rb_pay_visa)
    RadioButton rb_pay_visa;

    @BindView(R.id.tv_upon_receipt)
    TextView tv_upon_receipt;

    @BindView(R.id.tv_pay_visa)
    TextView tv_pay_visa;

    @BindView(R.id.tv_mada)
    TextView tv_mada;

    @BindView(R.id.tv_expire_date)
    TextView tv_expire_date;

    // vars
    String current_date;

    protected SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_activity);
        ButterKnife.bind(this);
        initRadioGroup();
    }

    @OnClick(R.id.tv_expire_date)
    public void initDatePicker() {
        DialogFragment datePicker = new DatepickerFragment();
        datePicker.show(getSupportFragmentManager(), "date picker");

    } // choose expire date from date picker


    private void initRadioGroup() {
        radio_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rb_upon_receipt) {

                    tv_upon_receipt.setTextColor(getResources().getColor(R.color.colorPrimary));
                    tv_pay_visa.setTextColor(getResources().getColor(R.color.gray));
                    tv_mada.setTextColor(getResources().getColor(R.color.gray));
                }
                if (checkedId == R.id.rb_pay_visa) {
                    tv_upon_receipt.setTextColor(getResources().getColor(R.color.gray));
                    tv_pay_visa.setTextColor(getResources().getColor(R.color.colorPrimary));
                    tv_mada.setTextColor(getResources().getColor(R.color.gray));
                }
                if (checkedId == R.id.rb_mada) {
                    tv_upon_receipt.setTextColor(getResources().getColor(R.color.gray));
                    tv_pay_visa.setTextColor(getResources().getColor(R.color.gray));
                    tv_mada.setTextColor(getResources().getColor(R.color.colorPrimary));
                }
            }
        });

    } // initialize radio group

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

        Calendar calendar = Calendar.getInstance();
        calendar.get(Calendar.YEAR);
        calendar.get(Calendar.MONTH);
        calendar.get(Calendar.DAY_OF_MONTH);
        current_date = DateFormat.getDateInstance(DateFormat.FULL).format(calendar.getTime());
        tv_expire_date.setText(current_date);

    } // override on date set

    @Override
    protected void attachBaseContext(Context newBase) {
        sharedPreferences = newBase.getSharedPreferences("user",MODE_PRIVATE);
        super.attachBaseContext(CalligraphyContextWrapper.wrap(new MyContextWrapper(newBase).wrap(sharedPreferences.getString("language","ar"))));
    }// apply fonts

}
