package com.invetechs.anakeed.View.Activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.ImageView;

import com.invetechs.anakeed.Adapter.NotificationstAdapter;
import com.invetechs.anakeed.AppConfig.CustomDialogProgress;
import com.invetechs.anakeed.AppConfig.ShowDialog;
import com.invetechs.anakeed.AppConfig.ToolBar;
import com.invetechs.anakeed.Language.MyContextWrapper;
import com.invetechs.anakeed.R;
import com.invetechs.anakeed.Retrofit.connection.RetrofitClient;
import com.invetechs.anakeed.Retrofit.notifications.NotificationResponse;
import com.invetechs.anakeed.Retrofit.notifications.notificationRequest;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class NotificationActivity extends AppCompatActivity {

    // Bind views
    @BindView(R.id.notification_recycler_view)
    RecyclerView notification_recycler_view;


    // variables

    protected SharedPreferences sharedPreferences;
    ImageView image_notification;
    List<NotificationResponse.NotificaionsBean> arrayList = new ArrayList<>();
    LinearLayoutManager layoutManager;
    NotificationstAdapter adapter;
    Handler handler;
    CustomDialogProgress progress;
    ShowDialog toastDialog = new ShowDialog();
    SharedPreferences prefs;
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    int user_id = 0;
    ToolBar toolBarConfig;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        ButterKnife.bind(this);
        setToolBarConfig();
        getNotifications();
        getUserId();
        image_notification = findViewById(R.id.img_notifications);
        image_notification.setOnClickListener(null);
    }

    private void setToolBarConfig() {

        toolBarConfig = new ToolBar(this);
        toolBarConfig.setTitle(getString(R.string.notifications));
        toolBarConfig.addNotification();
        Log.i("QW", "notification activity notification");

    } // function of setToolBarConfig

    private void getUserId() {
        prefs = getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE);
        user_id = prefs.getInt("id", 0);
        Log.i("Print", "userId in notification: " + user_id);

    } // getUserId function

    private void initRecyclerView() {

        layoutManager = new LinearLayoutManager(this);
        adapter = new NotificationstAdapter(this, arrayList);
        notification_recycler_view.setHasFixedSize(true);
        notification_recycler_view.setLayoutManager(layoutManager);
        notification_recycler_view.setAdapter(adapter);

    } // initialize recycler view

    @SuppressLint("HandlerLeak")
    private void getNotifications() {
        progress = new CustomDialogProgress();
        progress.init(this);

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
        new Thread() {
            public void run() {

                Retrofit retrofit = RetrofitClient.getInstance();

                final notificationRequest notificationApi = retrofit.create(notificationRequest.class);

                final Call<NotificationResponse> getInterestConnection = notificationApi.showNotifications(user_id);

                getInterestConnection.enqueue(new Callback<NotificationResponse>() {
                    @Override
                    public void onResponse(Call<NotificationResponse> call, Response<NotificationResponse> response) {
                        try {
                            String code = response.body().getCode();

                            Log.i("Print", "code" + code);

                            response.body();

                            Log.i("Print", "response" + response.body().getCode());

                            if (code.equals("200")) {
                                arrayList = response.body().getNotificaions();

                                initRecyclerView();
                            } // login success

                            progress.dismiss();

                        } // try
                        catch (Exception e) {
                            Log.i("Print", "exception : " + e.toString());
                            progress.dismiss();
                        } // catch
                    } // onResponse

                    @Override
                    public void onFailure(Call<NotificationResponse> call, Throwable t) {
                        toastDialog.initDialog(getString(R.string.retry), NotificationActivity.this);
                        Log.i("Print", "error : " + t.toString());
                        progress.dismiss();
                    } // on Failure
                });
            }

        }.start();
    } // show notifications

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Thread.interrupted();
        toolBarConfig.onDestroy();
        progress.dismiss();
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        sharedPreferences = newBase.getSharedPreferences("user", MODE_PRIVATE);
        super.attachBaseContext(CalligraphyContextWrapper.wrap(new MyContextWrapper(newBase).wrap(sharedPreferences.getString("language", "ar"))));
    }// apply fonts

}
