package com.invetechs.anakeed.View.Fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.TextView;

import com.invetechs.anakeed.R;
import com.labo.kaji.fragmentanimations.PushPullAnimation;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class ContactUsFragment extends Fragment {

    // bind views

    @BindView(R.id.tv_title)
    TextView tx_title;

    @BindView(R.id.tv_mobile)
    TextView tv_mobile;

    @BindView(R.id.tv_email)
    TextView tv_email;

    @BindView(R.id.tv_wesite)
    TextView tv_wesite;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.contact_us, container, false);
        ButterKnife.bind(this, view);
        return view;

    } // function of onCreateView


    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        return PushPullAnimation.create(PushPullAnimation.LEFT ,  enter , 3000);
    } // animation

    @OnClick(R.id.img_map)
    public void openMap() {
        String url = "https://www.google.com/maps/place/24%C2%B041'52.4%22N+46%C2%B043'22.9%22E/@24.697895,46.723015,16z/data=!4m5!3m4!1s0x0:0x0!8m2!3d24.6978951!4d46.7230148?hl=en-US";
        Intent website = new Intent(Intent.ACTION_VIEW);
        website.setData(Uri.parse(url));
        startActivity(website);

    } //open map location on click

    @OnClick(R.id.card_website)
    public void openWebsite() {
        String url = "http://www.invetechs.com/";
        Intent openWebSite = new Intent(Intent.ACTION_VIEW);
        openWebSite.setData(Uri.parse(url));
        startActivity(openWebSite);

    } //open website on click

    @OnClick(R.id.card_mobile_number)
    public void makeCall() {
        String number = "(+966) 558359836";
        Intent dialNumber = new Intent(Intent.ACTION_DIAL);
        dialNumber.setData(Uri.parse("tel:" + number));
        startActivity(dialNumber);
    } // make call on click

    @OnClick(R.id.img_instagram)
    public void goToInstagram() {
        String url = "https://www.instagram.com/";
        Intent website = new Intent(Intent.ACTION_VIEW);
        website.setData(Uri.parse(url));
        startActivity(website);
    } // open instagram link on click

    @OnClick(R.id.img_twitter)
    public void goToTwitter() {
        String url = "https://twitter.com/";
        Intent website = new Intent(Intent.ACTION_VIEW);
        website.setData(Uri.parse(url));
        startActivity(website);
    } // open twitter link on click

    @OnClick(R.id.img_facebook)
    public void goToFacebook() {
        String url = "http://www.facebook.com/invetechs";
        Intent website = new Intent(Intent.ACTION_VIEW);
        website.setData(Uri.parse(url));
        startActivity(website);
    } // open facebook link on click

    @OnClick(R.id.img_youtube)
    public void goToYoutube() {
        String url = "https://www.youtube.com/";
        Intent website = new Intent(Intent.ACTION_VIEW);
        website.setData(Uri.parse(url));
        startActivity(website);
    } // open youtube link on click

    @OnClick(R.id.img_linked_in)
    public void goToLinkedIn() {
        String url = "https://www.linkedin.com/invetechs";
        Intent website = new Intent(Intent.ACTION_VIEW);
        website.setData(Uri.parse(url));
        startActivity(website);
    } // open linkedIn link on click

    @Override
    public void onDestroy() {
        super.onDestroy();
        Thread.interrupted();
    } // ondestroy

} // class of ContactUsFragment
