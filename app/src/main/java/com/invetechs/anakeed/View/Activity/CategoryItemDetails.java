package com.invetechs.anakeed.View.Activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;

import com.bumptech.glide.Glide;
import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.google.android.material.button.MaterialButton;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.invetechs.anakeed.AppConfig.CustomDialogProgress;
import com.invetechs.anakeed.AppConfig.ShowDialog;
import com.invetechs.anakeed.AppConfig.ToolBar;
import com.invetechs.anakeed.Language.MyContextWrapper;
import com.invetechs.anakeed.R;
import com.invetechs.anakeed.Retrofit.connection.RetrofitClient;
import com.invetechs.anakeed.Retrofit.cart.storeBasketRequest;
import com.invetechs.anakeed.Retrofit.cart.storeBasketResponse;
import com.invetechs.anakeed.Retrofit.products.productDetailsRequest;
import com.invetechs.anakeed.Retrofit.products.productDetailsResponse;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class CategoryItemDetails extends AppCompatActivity {

    // bind views

    @BindView(R.id.categoryItem_title)
    TextView categoryItem_title;

    @BindView(R.id.image_product)
    ImageView image_product;

    @BindView(R.id.categoryItem_description)
    TextView categoryItem_description;

//    @BindView(R.id.categoryItem_typeOne)
//    TextView categoryItem_typeOne;

//    @BindView(R.id.categoryItem_slash)
//    TextView categoryItem_slash;

//    @BindView(R.id.categoryItem_typeTwo)
//    TextView categoryItem_typeTwo;

    @BindView(R.id.categoryItem_typeOneBackground)
    TextView categoryItem_typeOneBackground;

    @BindView(R.id.tv_price)
    TextView tv_price;

//    @BindView(R.id.categoryItem_typeTwoBackground)
//    TextView categoryItem_typeTwoBackground;

    @BindView(R.id.categoryItem_addToCartButton)
    MaterialButton categoryItem_addToCartButton;

    @BindView(R.id.number_button)
    ElegantNumberButton number_button;

//    @BindView(R.id.categoryItem_decrease1)
//    ImageView categoryItem_decrease1;

//    @BindView(R.id.categoryItem_increase1)
//    ImageView categoryItem_increase1;

//    @BindView(R.id.categoryItem_decrease2)
//    ImageView categoryItem_decrease2;

//    @BindView(R.id.categoryItem_increase2)
//    ImageView categoryItem_increase2;

//    @BindView(R.id.categoryItem_textCounter1)
//    TextView categoryItem_textCounter1;

//    @BindView(R.id.categoryItem_textCounter2)
//    TextView categoryItem_textCounter2;

    // variables

    String color;
    ToolBar toolBar;
    CustomDialogProgress progress;
    Handler handler;
    protected SharedPreferences sharedPreferences;
    String category_id = "";
    ShowDialog showDialog = new ShowDialog();
    private productDetailsResponse.ProductBean product;
    String imageUrl = "http://3nakied.invetechs.com/cpanel/upload/Slider/";
    SharedPreferences.Editor editor;
    ShowDialog toastDialog = new ShowDialog();
    int quantity_value = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_item_details);
        ButterKnife.bind(this);
        setToolbar();
        getIntentData();
        getIdFromCategory();
        getProductDetails();
        getNumberValue();

    } // function of onCreate

    private void getNumberValue() {

        number_button.setOnValueChangeListener(new ElegantNumberButton.OnValueChangeListener() {
            @Override
            public void onValueChange(ElegantNumberButton view, int oldValue, int newValue) {

                quantity_value = Integer.parseInt(number_button.getNumber());

                Log.i("Print", "quantity_value : " + quantity_value);

                int b = product.getPrice1();

                Log.i("Print", "price value : " + b);

                int update_price = quantity_value * b;

                Log.i("Print", "live_changes : " + "" + update_price);

                sharedPreferences = getSharedPreferences("user", MODE_PRIVATE);

                if (sharedPreferences.getString("language", "ar").equals("ar")) {
                      tv_price.setText(convertToArabic(String.valueOf(update_price)) +  " "  + getString(R.string.sr));
                    try {
                     //   Log.i("Print", "live_changes : " + "" + Integer.parseInt(product.getPrice1()) * newValue + " " + getString(R.string.sr));

                    } catch (Exception e) {
                        Log.i("Print", "exccc : " + e.getMessage());

                    }


                    //   tv_price.setText(jjkl);
                } else {
                    Log.i("Print", "font App in order details : en");
                    tv_price.setText(product.getPrice1() + " " + getString(R.string.sr));
                }


                Log.i("Print", "quantity_value in valueChanged: " + quantity_value);
            }
        });

    } // get value of button

    @OnClick(R.id.categoryItem_addToCartButton)
    public void addToCart() {

        sendOrderToApi();

    } // add to cart


    public int getPrefUserId() {
        int userId = 0;
        try {
            SharedPreferences preferences = getSharedPreferences("MyPrefsFile", MODE_PRIVATE);

            if (preferences != null)
                userId = preferences.getInt("id", 0);

            Log.i("Print", "User id" + userId);
        } catch (Exception e) {
            Log.i("Print", "Exception" + e.getMessage());
        }

        return userId;
    } // function of getPrefUserId

    @SuppressLint("HandlerLeak")
    private void sendOrderToApi() {

        progress = new CustomDialogProgress();
        progress.init(this);
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }
        };
        progress.show();
        new Thread() {
            public void run() {

                Retrofit retrofit = RetrofitClient.getInstance();
                storeBasketRequest storeBasketRequest = retrofit.create(storeBasketRequest.class);

                Log.i("Print", "send userrr id : " + getPrefUserId());
                Log.i("Print", "send category id :  : " + category_id);
                Log.i("Print", "send quantity_value : " + quantity_value);


                Call<storeBasketResponse> basketResponseCall = storeBasketRequest.storeToBasket(getPrefUserId(), category_id, quantity_value);
                basketResponseCall.enqueue(new Callback<storeBasketResponse>() {
                    @Override
                    public void onResponse(Call<storeBasketResponse> call, Response<storeBasketResponse> response) {
                        try {

                            Log.i("Print", "code" + response.body().getCode());

                            if (response.body() != null) {

                                String code = response.body().getCode();
                                Log.i("Print", "store basket code : " + code);

                                if (code.equals("200")) {
                                    Log.i("Print", "store basket code success" + code);
                                    toastDialog.initDialog(getString(R.string.addedToCart), CategoryItemDetails.this);

                                } else {
                                    toastDialog.initDialog(getString(R.string.retry), CategoryItemDetails.this);
                                    Log.i("Print", "else ");
                                }
                                progress.dismiss();
                            }

                        } catch (Exception e) {
                            Log.i("Print", "exception : " + e.toString());
                            progress.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(Call<storeBasketResponse> call, Throwable t) {
                        toastDialog.initDialog(getString(R.string.retry), CategoryItemDetails.this);
                        Log.i("Print", "error : " + t.toString());
                        progress.dismiss();
                    }
                });

            }
        }.start();

    } //function of getAddressesFromApi

    private void getIdFromCategory() {
        Intent intent = getIntent();
        if (intent != null) {

            category_id = intent.getStringExtra("category_item_id");
            Log.i("Print", "category_id id in categoryitem details : " + category_id);
        }
    } // get id from category

    @SuppressLint("HandlerLeak")
    private void getProductDetails() {
        progress = new CustomDialogProgress();
        progress.init(this);
        handler = new Handler() {

            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
      /*  new Thread() {
            public void run() {*/
//Retrofit

        Retrofit retrofit = RetrofitClient.getInstance();

        final productDetailsRequest productDetailsRequest = retrofit.create(productDetailsRequest.class);

        Log.i("Print", "category_id before request : " + category_id);


        final Call<productDetailsResponse> getInterestConnection = productDetailsRequest.getProductDetails(category_id);

        getInterestConnection.enqueue(new Callback<productDetailsResponse>() {
            @Override
            public void onResponse(Call<productDetailsResponse> call, Response<productDetailsResponse> response) {
                try {

                    String code = response.body().getCode();
                    Log.i("Print", "code" + code);
                    Log.i("Print", "category_id : " + category_id);

                    if (code.equals("200")) {
                        if (response.body() != null) {
                            product = response.body().getProduct();
                            getDetailsData();
                        } else {
                            showDialog.initDialog(getString(R.string.retry), CategoryItemDetails.this);
                        }

                    } // found order
                    else if (code.equals("1313")) {

                    } // not found

                    progress.dismiss();

                } // try
                catch (Exception e) {
                    Log.i("Print", "exception : " + e.toString());
                    progress.dismiss();
                } // catch
            } // onResponse

            @Override
            public void onFailure(Call<productDetailsResponse> call, Throwable t) {
                showDialog.initDialog(getString(R.string.retry), CategoryItemDetails.this);
                Log.i("Print", "error : " + t.toString());
                progress.dismiss();
            } // on Failure
        });
// Retrofit

           /* }
        }.start();*/
    } // function of getOrderDetails

    public String convertToArabic(String value) {
        String newValue = (((((((((((value + "")
                .replaceAll("1", "١")).replaceAll("2", "٢"))
                .replaceAll("3", "٣")).replaceAll("4", "٤"))
                .replaceAll("5", "٥")).replaceAll("6", "٦"))
                .replaceAll("7", "٧")).replaceAll("8", "٨"))
                .replaceAll("9", "٩")).replaceAll("0", "٠"));

        return newValue;
    } // convert arabic date to english to send to api


    private void getDetailsData() {


        Glide.with(this)
                .load(imageUrl + product.getImg())
                .into(image_product);

        sharedPreferences = getSharedPreferences("user", MODE_PRIVATE);

        if (sharedPreferences.getString("language", "ar").equals("ar")) {

            Log.i("Print", "font App in order details : ar");

            String total = convertToArabic(String.valueOf(product.getPrice1()));

            categoryItem_title.setText(product.getAr_title());
            categoryItem_description.setText(product.getAr_description());
            tv_price.setText(total  + " " + getString(R.string.sr));
        } else {
            Log.i("Print", "font App in order details : en");
            categoryItem_title.setText(product.getEn_title());
            categoryItem_description.setText(product.getEn_description());
            tv_price.setText(product.getPrice1() + " " + getString(R.string.sr));
        }


    } // get data details

    private void setToolbar() {
        toolBar = new ToolBar(this);
        toolBar.setTitle(getString(R.string.product_details));
        toolBar.addNotification();
    } // function of setToolbar

    private void getIntentData() {
        Intent intent = getIntent();
        if (intent != null) {
            color = intent.getStringExtra("color");
            if (color != null)
                colorSetting();
        }
    } // function of getIntentData

    @SuppressLint("ResourceType")
    private void colorSetting() {
        /* color setting */
        categoryItem_title.setTextColor(Color.parseColor(color));
        categoryItem_description.setTextColor(Color.parseColor(color));
        //    categoryItem_typeOne.setTextColor(Color.parseColor(color));
//        categoryItem_typeTwo.setTextColor(Color.parseColor(color));
//        categoryItem_slash.setTextColor(Color.parseColor(color));
        categoryItem_addToCartButton.setBackgroundColor(Color.parseColor(color));


        ColorStateList csl = new ColorStateList(new int[][]{new int[0]}, new int[]{Color.parseColor(color)});
        categoryItem_addToCartButton.setBackgroundTintList(csl);

        //    categoryItem_decrease1.setColorFilter(Color.parseColor(color));
        //   categoryItem_increase1.setColorFilter(Color.parseColor(color));


        //   number_button.setBackgroundResource(R.color.dark_grey);


        // categoryItem_decrease2.setColorFilter(Color.parseColor(color));
        // categoryItem_increase2.setColorFilter(Color.parseColor(color));

        // categoryItem_textCounter1.setTextColor(Color.parseColor(color));
        //   categoryItem_textCounter2.setTextColor(Color.parseColor(color));


        GradientDrawable bgShape = (GradientDrawable) categoryItem_typeOneBackground.getBackground();
        //GradientDrawable bgShape1 = (GradientDrawable)categoryItem_typeTwoBackground.getBackground();


        bgShape.mutate();

        bgShape.setColor(Color.parseColor(color));


//        bgShape1.mutate();
//        bgShape1.setColor(Color.parseColor(color));


        toolBar.setColor(color);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.parseColor(color));
        }
        /* color setting */
    } // function of colorSetting

    @Override
    protected void attachBaseContext(Context newBase) {
        sharedPreferences = newBase.getSharedPreferences("user", MODE_PRIVATE);
        super.attachBaseContext(CalligraphyContextWrapper.wrap(new MyContextWrapper(newBase).wrap(sharedPreferences.getString("language", "ar"))));
    }// apply fonts

} // class of CategoryItemDetails
