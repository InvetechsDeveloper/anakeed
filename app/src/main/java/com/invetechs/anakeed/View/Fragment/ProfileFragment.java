package com.invetechs.anakeed.View.Fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;

import com.google.android.material.button.MaterialButton;
import com.invetechs.anakeed.AppConfig.CustomDialogProgress;
import com.invetechs.anakeed.AppConfig.ShowDialog;
import com.invetechs.anakeed.R;
import com.invetechs.anakeed.Retrofit.connection.RetrofitClient;
import com.invetechs.anakeed.Retrofit.profile.profileApi;
import com.invetechs.anakeed.Retrofit.profile.profileResponse;
import com.invetechs.anakeed.Retrofit.profile.updateProfileApi;
import com.invetechs.anakeed.View.Activity.MenuActivity;
import com.labo.kaji.fragmentanimations.PushPullAnimation;
import com.rengwuxian.materialedittext.MaterialEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static android.content.Context.MODE_PRIVATE;


public class ProfileFragment extends Fragment {
    // bind views
    @BindView(R.id.et_username)
    MaterialEditText et_username;

    @BindView(R.id.et_phone_number)
    MaterialEditText et_phone_number;

    @BindView(R.id.et_password)
    MaterialEditText et_password;

    @BindView(R.id.et_addresses)
    MaterialEditText et_addresses;

    @BindView(R.id.btn_edit)
    MaterialButton btn_edit;

    @BindView(R.id.logo_image)
    ImageView logo_image;

    // vars
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    int id = 0;
    String name = "", mobileNumber = "", password = "", newPassword = "";
    profileResponse.UserBean user;
    Handler handler;
    CustomDialogProgress progress;
    ShowDialog toastDialog = new ShowDialog();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.profile_fragment, container, false);
        ButterKnife.bind(this, view);
        getUserID_Password();
        getDataFromApi();
        fadingImage();
        return view;

    } // function of onCreateView

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        return PushPullAnimation.create(PushPullAnimation.LEFT , enter , 3000);
    } // animation


    private void fadingImage() {
        Animation fadeOut = new AlphaAnimation(1, 0);  // the 1, 0 here notifies that we want the opacity to go from opaque (1) to transparent (0)
        fadeOut.setInterpolator(new AccelerateInterpolator());
        fadeOut.setStartOffset(500); // Start fading out after 500 milli seconds
        fadeOut.setDuration(1000);

        logo_image.setAnimation(fadeOut);


        btn_edit.setHovered(true);

    } // fading image view

    @SuppressLint("HandlerLeak")
    private void getDataFromApi() {

        progress = new CustomDialogProgress();
        progress.init(getContext());

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }
        };
        progress.show();
        new Thread() {
            public void run() {

                Retrofit retrofit = RetrofitClient.getInstance();

                profileApi profileApi = retrofit.create(profileApi.class);

                Log.i("Print", "send id to get data" + id);

                final Call<profileResponse> getInterestConnection = profileApi.show_profile(id);

                getInterestConnection.enqueue(new Callback<profileResponse>() {
                    @Override
                    public void onResponse(Call<profileResponse> call, Response<profileResponse> response) {
                        try {

                            String code = response.body().getCode();
                            response.body();
                            Log.i("Print", "profile code" + code);

                            if (code.equals("200")) {

                                  user = response.body().getUser();


                                Log.i("Print", "get name in profile " + user.getName());
                                Log.i("Print", "get phone in profile " + user.getPhone());

                                et_username.setText(user.getName());
                                et_phone_number.setText(user.getPhone());
                                et_password.setText(password);

                                et_username.setTextColor(getResources().getColor(R.color.colorPrimary));
                                et_phone_number.setTextColor(getResources().getColor(R.color.colorPrimary));
                                et_password.setTextColor(getResources().getColor(R.color.colorPrimary));
                                et_addresses.setTextColor(getResources().getColor(R.color.colorPrimary));

                            } // get user data successfully

                            progress.dismiss();

                        } // try
                        catch (Exception e) {
                            Log.i("Print", "exception : " + e.toString());
                            progress.dismiss();
                        } // catch
                    } // onResponse

                    @Override
                    public void onFailure(Call<profileResponse> call, Throwable t) {

                        toastDialog.initDialog(getString(R.string.retry), getActivity());
                        Log.i("Print", "error : " + t.toString());
                        progress.dismiss();
                    } // on Failure
                });
// Retrofit

            }

        }.start();

    } // show profile data


    @OnClick(R.id.et_addresses)
    public void clickAddresses() {
        Log.i("Print", "addresses clicked");
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new AddressesFragment()).commit();
    } // function of addresses button

    private void getUserID_Password() {

        SharedPreferences prefs = getContext().getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        id = prefs.getInt("id", 0);
        password = prefs.getString("password", "");
        Log.i("Print", "profile id" + id);
        Log.i("Print", "profile password" + password);

    } // function get the id of user to send it to api

    public static int getScreenWidth(Activity activity) {
        Point size = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(size);
        return size.x;
    } // function getScreenWidth

    @OnClick(R.id.btn_edit)
    public void editProfile() {

        et_username.setEnabled(true);
        et_phone_number.setEnabled(true);
        et_password.setEnabled(true);
        et_addresses.setVisibility(View.GONE);

        if (!ValidatePassword() | !ValidateUserName() | !ValidateMobile()) {
            return;
        } else {
            name = et_username.getText().toString();
            mobileNumber = et_phone_number.getText().toString().trim();
            newPassword = et_password.getText().toString().trim();

            if (btn_edit.getText().toString().equals(getString(R.string.save))) {
                updateProfileData();
                et_username.setEnabled(false);
                et_phone_number.setEnabled(false);
                et_password.setEnabled(false);
                et_addresses.setVisibility(View.VISIBLE);
                btn_edit.setText(getString(R.string.edit));


            } else {
                btn_edit.setText(getString(R.string.save));
            }

        }

    } // click on edit profile

    @SuppressLint("HandlerLeak")
    public void updateProfileData() {

        progress = new CustomDialogProgress();
        progress.init(getContext());

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
        new Thread() {
            public void run() {

                Retrofit retrofit = RetrofitClient.getInstance();
                updateProfileApi updateProfile = retrofit.create(updateProfileApi.class);

                Call<profileResponse> getConnection = updateProfile.updateProfile(id, mobileNumber, name, newPassword);

                Log.i("Print", "file : " + "\nid : " + id
                        + "\n mobile : " + mobileNumber
                        + "\n name : " + name
                        + "\n newPassword : " + newPassword);


                getConnection.enqueue(new Callback<profileResponse>() {
                    @Override
                    public void onResponse(Call<profileResponse> call, Response<profileResponse> response) {
                        try {

                            Log.i("Print", "response : " + response.body().getCode());
                            String code = response.body().getCode();

                            Log.i("Print", "code : " + code);
                            Log.i("Print", "response : " + response.body());

                            if (code.equals("200")) {
                                profileResponse.UserBean user = response.body().getUser();
                                SharedPreferences.Editor editor = getActivity().getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                                editor.putString("name", user.getName());
                                editor.putString("password", newPassword);
                                editor.putString("mobilenumber", user.getPhone());
                                editor.apply();
                                toastDialog.initDialog(getString(R.string.profile_updated), getActivity());
                                progress.dismiss();

                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {

                                        Intent intent = new Intent(getActivity(), MenuActivity.class);
                                        intent.putExtra("fragmentFlag", "home");
                                        getActivity().startActivity(intent);

                                    }
                                }, 2000);
                            }

                            else if (code.equals("1233")) {
                                toastDialog.initDialog(getString(R.string.unique), getActivity());
                                progress.dismiss();

                            }
                        } catch (Exception e) {

                            Log.i("Print", "exception : " + e.toString());
                            progress.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(Call<profileResponse> call, Throwable t) {

                        Log.i("Print", "error : " + t.toString());
                        toastDialog.initDialog(getString(R.string.retry), getActivity());
                        progress.dismiss();
                    }
                });
            }

        }.start();
    } // updateProfileData

    private boolean ValidateMobile() {

        String Mobile = et_phone_number.getText().toString().trim();

        if (Mobile.length() < 10 || Mobile.length() > 10) {
            et_phone_number.setError(getString(R.string.error_valid_mobile));
        }
        if (Mobile.length() == 10) {
            et_phone_number.setError(null);
        }

        if (Mobile.length() == 0) {
            et_phone_number.setError(getString(R.string.field_cant_empty));
        }

        return true;
    } // validation on mobile

    public boolean ValidatePassword() {

        password = et_password.getText().toString().trim();

        if (password.isEmpty()) {
            et_password.setError(getString(R.string.field_cant_empty));
            return false;
        }

        if (password.length() < 4) {
            et_password.setError(getString(R.string.valid_password_number));
            return false;

        }

        return true;


    } // validation on password

    private boolean ValidateUserName() {

        name = et_username.getText().toString().trim();

        if (name.isEmpty()) {
            et_username.setError(getString(R.string.field_cant_empty));
            return false;
        } else if (et_username.length() < 5) {
            et_username.setError(getString(R.string.enter_full_name));
            return false;
        } else {

            return true;
        }
    } // validation on username

        @Override
        public void onDestroy() {
            super.onDestroy();
            Thread.interrupted();
        } // on destroy

} // class of OrdersFragment
