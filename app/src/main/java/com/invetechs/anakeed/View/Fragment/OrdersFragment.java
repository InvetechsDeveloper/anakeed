package com.invetechs.anakeed.View.Fragment;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.invetechs.anakeed.Adapter.OrderAdapter;
import com.invetechs.anakeed.AppConfig.CustomDialogProgress;
import com.invetechs.anakeed.AppConfig.ShowDialog;
import com.invetechs.anakeed.R;
import com.invetechs.anakeed.Retrofit.connection.RetrofitClient;
import com.invetechs.anakeed.Retrofit.orders.getCurrentOrdersRequest;
import com.invetechs.anakeed.Retrofit.orders.getOldOrdersRequest;
import com.invetechs.anakeed.Retrofit.orders.ordersResponse;
import com.labo.kaji.fragmentanimations.MoveAnimation;
import com.labo.kaji.fragmentanimations.PushPullAnimation;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static android.content.Context.MODE_PRIVATE;


public class OrdersFragment extends Fragment {

    // bind views

    @BindView(R.id.current_orders)
    RecyclerView current_orders;

    @BindView(R.id.tv_current_orders)
    TextView tv_current_orders;

    @BindView(R.id.tv_previous_orders)
    TextView tv_previous_orders;

    // variables
    SharedPreferences prefs;
    public final String MY_PREFS_NAME = "MyPrefsFile";
    String id = "";
    OrderAdapter orderAdapter;
    List<ordersResponse.OrdersBean> ordersListApi = new ArrayList<>();
    List<ordersResponse.OrdersBean> ordersList = new ArrayList<>();
    ShowDialog toastDialog = new ShowDialog();
    Handler handler;
    CustomDialogProgress progress;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.orders_fragment, container, false);
        ButterKnife.bind(this, view);
        initRecycelerView(); // intialize reclerview
        getCurrentOrdersFromApi(); // get laa addresses of user
        return view;

    } // function of onCreateView


    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        return PushPullAnimation.create(PushPullAnimation.UP , enter , 3000);
    } // animation

    @OnClick(R.id.tv_current_orders)
    public void clickCurrent() {

        tv_current_orders.startAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.rotate));

        tv_current_orders.setTextColor(getResources().getColor(R.color.colorPrimary));
        tv_previous_orders.setTextColor(getResources().getColor(R.color.gray));
        initRecycelerView(); // intialize reclerview
        getCurrentOrdersFromApi(); // get laa addresses of user

    } // click on current orders


    @OnClick(R.id.tv_previous_orders)
    public void clickPrevious() {
        tv_previous_orders.startAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.rotate));
        tv_previous_orders.setTextColor(getResources().getColor(R.color.colorPrimary));
        tv_current_orders.setTextColor(getResources().getColor(R.color.gray));
        initRecycelerView(); // intialize reclerview
        getOldOrdersFromApi();

    } // click on current orders

    private void initRecycelerView() {
        orderAdapter = new OrderAdapter(getActivity(), ordersList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        current_orders.setHasFixedSize(true);
        current_orders.setLayoutManager(linearLayoutManager);
        current_orders.setAdapter(orderAdapter);
    } // intialize list of addresses


    public int getPrefUserId() {
        int userId = 0;
        try {
            SharedPreferences preferences = getContext().getSharedPreferences("MyPrefsFile", MODE_PRIVATE);

            if (preferences != null)
                userId = preferences.getInt("id", 0);

            Log.i("Print", "User id" + userId);
        } catch (Exception e) {
            Log.i("Print", "Exception" + e.getMessage());
        }

        return userId;
    } // function of getPrefUserId

    private void fillLsitWithCurrentOrders() {
        for (int i = 0; i < ordersListApi.size(); i++) {
            ordersList.add(ordersListApi.get(i));
            Log.i("Print", "ordersList" + ordersList.size());
        }
        orderAdapter.notifyDataSetChanged();
        if (ordersListApi.size() <= 0) {
            toastDialog.initDialog(getString(R.string.no_orders), getContext());
        }
    } // function of fillListWithAddresses

    @SuppressLint("HandlerLeak")
    private void getCurrentOrdersFromApi() {

        if (ordersListApi.size() > 0 || ordersList.size() > 0) {
            ordersList.clear();
            ordersListApi.clear();
        }

        progress = new CustomDialogProgress();
        progress.init(getContext());
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }
        };
        progress.show();
        new Thread() {
            public void run() {

                Retrofit retrofit = RetrofitClient.getInstance();
                getCurrentOrdersRequest ordersRequest = retrofit.create(getCurrentOrdersRequest.class);
                Call<ordersResponse> ordersResponseCall = ordersRequest.getCurrentOrders(getPrefUserId());
                ordersResponseCall.enqueue(new Callback<ordersResponse>() {
                    @Override
                    public void onResponse(Call<ordersResponse> call, Response<ordersResponse> response) {
                        try {

                            Log.i("Print", "code" + response.body().getCode());

                            if (response.body() != null) {

                                String code = response.body().getCode();
                                Log.i("Print", "code : " + code);

                                if (code.equals("200")) {
                                    Log.i("Print", "code success" + code);
                                    ordersListApi = response.body().getOrders();
                                    Log.i("Print", "addressListApi " + ordersListApi.size());
                                    fillLsitWithCurrentOrders();
                                } else if (code.equals("1333")) {
                                    toastDialog.initDialog(getString(R.string.no_orders), getActivity());
                                    Log.i("Print", "else ");
                                } else {
                                    toastDialog.initDialog(getString(R.string.retry), getActivity());
                                    Log.i("Print", "else ");
                                }
                                progress.dismiss();
                            }

                        } catch (Exception e) {
                            Log.i("Print", "exception : " + e.toString());
                            progress.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(Call<ordersResponse> call, Throwable t) {
                        toastDialog.initDialog(getString(R.string.retry), getActivity());
                        Log.i("Print", "error : " + t.toString());
                        progress.dismiss();
                    }
                });

            }
        }.start();

    } //function of getAddressesFromApi

    @SuppressLint("HandlerLeak")
    private void getOldOrdersFromApi() {

        if (ordersListApi.size() > 0 || ordersList.size() > 0) {
            ordersList.clear();
            ordersListApi.clear();
        }

        progress = new CustomDialogProgress();
        progress.init(getContext());
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }
        };
        progress.show();
        new Thread() {
            public void run() {

                Retrofit retrofit = RetrofitClient.getInstance();
                getOldOrdersRequest ordersRequest = retrofit.create(getOldOrdersRequest.class);
                Call<ordersResponse> ordersResponseCall = ordersRequest.getOldOrders(getPrefUserId());
                ordersResponseCall.enqueue(new Callback<ordersResponse>() {
                    @Override
                    public void onResponse(Call<ordersResponse> call, Response<ordersResponse> response) {
                        try {

                            Log.i("Print", "code" + response.body().getCode());

                            if (response.body() != null) {

                                String code = response.body().getCode();

                                Log.i("Print", "code : " + code);
                                if (code.equals("200")) {
                                    Log.i("Print", "code success" + code);

                                    ordersListApi = response.body().getOrders();
                                    Log.i("Print", "addressListApi " + ordersListApi.size());
                                    fillLsitWithCurrentOrders();
                                } else if (code.equals("1333")) {
                                    toastDialog.initDialog(getString(R.string.no_orders), getActivity());
                                    Log.i("Print", "else ");
                                } else {
                                    toastDialog.initDialog(getString(R.string.retry), getActivity());
                                    Log.i("Print", "else ");
                                }
                                progress.dismiss();
                            }

                        } catch (Exception e) {
                            Log.i("Print", "exception : " + e.toString());
                            progress.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(Call<ordersResponse> call, Throwable t) {
                        toastDialog.initDialog(getString(R.string.retry), getActivity());
                        Log.i("Print", "error : " + t.toString());
                        progress.dismiss();
                    }
                });

            }
        }.start();

    } //function of getAddressesFromApi

    @Override
    public void onDestroy() {
        super.onDestroy();
        Thread.interrupted();
    } // on destroy

} // class of OrdersFragment
