package com.invetechs.anakeed.View.Activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.google.android.material.button.MaterialButton;
import com.invetechs.anakeed.AppConfig.CustomDialogProgress;
import com.invetechs.anakeed.AppConfig.ShowDialog;
import com.invetechs.anakeed.Language.MyContextWrapper;
import com.invetechs.anakeed.R;
import com.invetechs.anakeed.Retrofit.connection.RetrofitClient;
import com.invetechs.anakeed.Retrofit.auth.login.loginResponse;
import com.invetechs.anakeed.Retrofit.auth.register.registerRequest;
import com.rengwuxian.materialedittext.MaterialEditText;

import androidx.appcompat.widget.AppCompatCheckBox;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class RegisterActivity extends AppCompatActivity {

    // bind views
    @BindView(R.id.et_username)
    MaterialEditText et_username;

    @BindView(R.id.et_phone_number)
    MaterialEditText et_phone_number;

    @BindView(R.id.et_password)
    MaterialEditText et_password;

    @BindView(R.id.et_confirm_password)
    MaterialEditText et_confirm_password;

    @BindView(R.id.cb_terms)
    AppCompatCheckBox cb_terms;

    @BindView(R.id.btn_register)
    MaterialButton btn_register;

    // variables
    protected SharedPreferences sharedPreferences;
    String user_name = "", password = "", confirmPassword = "", mobileNumber = "";
    ShowDialog toastDialog;
    CustomDialogProgress progress;
    Handler handler;
    public static final String MY_PREFS_NAME = "MyPrefsFile";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        progress = new CustomDialogProgress();
        toastDialog = new ShowDialog();

    }

    @OnClick(R.id.btn_register)
    public void registerNewAccount() {

        if ( !ValidatePassword()  | !ValidateMobile()  | !ValidateUserName() | !ValidatecheckBox()) {
            return;
        } //validation on edit text

        else {

            mobileNumber = et_phone_number.getText().toString();
            user_name = et_username.getText().toString();
            password = et_password.getText().toString();
            registerApi();

        }


    } // register method

    @SuppressLint("HandlerLeak")
    private void registerApi() {

        progress.init(this);
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }
        };
        progress.show();
        new Thread() {

            @Override
            public void run() {
                Retrofit retrofit = RetrofitClient.getInstance();
                registerRequest request = retrofit.create(registerRequest.class);
                Call<loginResponse> responseCall = request.register(user_name, mobileNumber, password);
                Log.i("Print", "register username : " + user_name);
                Log.i("Print", "register mobileNumber : " + mobileNumber);
                Log.i("Print", "register password : " + password);

                responseCall.enqueue(new Callback<loginResponse>() {
                    @Override
                    public void onResponse(Call<loginResponse> call, Response<loginResponse> response) {

                        try {

                            String code = response.body().getCode();
                            Log.i("Print", "code : " + code);

                            if (code.equals("200")) {
                                loginResponse.UserdataBean user_data = response.body().getUserdata();

                                int id = user_data.getId();
                                String verification_code = user_data.getVerificationcode();
                                String mobile_number = user_data.getPhone();

                                Log.i("Print", "send id from register : " + id);
                                Log.i("Print", "send mobile from register : " + mobileNumber);
                                Log.i("Print", "verification code : " + verification_code);

                                progress.dismiss();
                                Intent intent = new Intent(RegisterActivity.this, VerificationActivity.class);
                                intent.putExtra("id", id);
                                intent.putExtra("mobilenumber", mobile_number);
                                intent.putExtra("verification_code", verification_code);
                                Log.i("Print", "send id from register : " + id);
                                startActivity(intent);
                                finish();
                            } else if (code.equals("1313")) {
                                progress.dismiss();
                                toastDialog.initDialog(getString(R.string.unique), RegisterActivity.this);

                            }
                            progress.dismiss();

                        }
                        catch (Exception e) {
                            Log.i("Print", "register exception  : " + password);

                        }

                    }

                    @Override
                    public void onFailure(Call<loginResponse> call, Throwable t) {
                        toastDialog.initDialog(getString(R.string.retry), RegisterActivity.this);
                        Log.i("Print", "error : " + t.toString());
                        progress.dismiss();

                    }
                });


            }
        }.start();


    } // register api method


    private boolean ValidateUserName() {

        user_name = et_username.getText().toString().trim();

        if (user_name.isEmpty()) {
            et_username.setError(getString(R.string.field_cant_empty));
            return false;
        } else if (et_username.length() < 5) {
            et_username.setError(getString(R.string.enter_full_name));
            return false;
        } else {

            return true;
        }
    } // validation on username

    public boolean ValidatePassword() {

        password = et_password.getText().toString().trim();
        confirmPassword = et_confirm_password.getText().toString().trim();

        if (password.isEmpty()) {
            et_password.setError(getString(R.string.field_cant_empty));
            return false;
        }

        if (password.length() < 4) {
            et_password.setError(getString(R.string.valid_password_number));
            return false;

        }

        if (!password.equals(confirmPassword)) {
            toastDialog.initDialog(getString(R.string.password_not_match), RegisterActivity.this);

            return false;

        }
        return true;


    } // validation on password

    private boolean ValidateMobile() {

        mobileNumber = et_phone_number.getText().toString().trim();

        if (mobileNumber.isEmpty()) {
            et_phone_number.setError(getString(R.string.field_cant_empty));
            return false;
        }

        if (!(mobileNumber.charAt(0) == '0' && mobileNumber.charAt(1) == '5' && mobileNumber.length() == 10)) {
            et_phone_number.setError(getString(R.string.enter_correct_number));
            return false;
        }

        return true;
    } // validation on mobile number

    private boolean ValidatecheckBox() {

        if (cb_terms.isChecked() == false) {
            Log.i("Print", "not checked");
            cb_terms.setError(getString(R.string.accept_terms));
            cb_terms.requestFocus();
            return false;
        } else {
            cb_terms.setError(null);
            return true;
        }

    } // validation for check box


    @OnClick(R.id.tv_show_terms)
    public void showTerms() {
        Intent intent = new Intent(RegisterActivity.this, TermsAndConditionsActivity.class);
        startActivity(intent);
    } // show Terms & Conditions to the user

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void attachBaseContext(Context newBase) {
        sharedPreferences = newBase.getSharedPreferences("user", MODE_PRIVATE);
        super.attachBaseContext(CalligraphyContextWrapper.wrap(new MyContextWrapper(newBase).wrap(sharedPreferences.getString("language", "ar"))));
    }// apply fonts

}
