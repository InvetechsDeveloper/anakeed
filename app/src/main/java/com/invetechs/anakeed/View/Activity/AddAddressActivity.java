package com.invetechs.anakeed.View.Activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;

import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.invetechs.anakeed.AppConfig.CustomDialogProgress;
import com.invetechs.anakeed.AppConfig.ShowDialog;
import com.invetechs.anakeed.AppConfig.ToolBar;
import com.invetechs.anakeed.Language.MyContextWrapper;
import com.invetechs.anakeed.R;
import com.invetechs.anakeed.Retrofit.connection.RetrofitClient;
import com.invetechs.anakeed.Retrofit.addresses.addressRequest;
import com.invetechs.anakeed.Retrofit.addresses.addressResponse;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class AddAddressActivity extends AppCompatActivity implements OnMapReadyCallback {


    // bind views
    @BindView(R.id.tx_address)
    TextView tx_address;

    @BindView(R.id.ed_name_desc)
    MaterialEditText ed_name_desc;

    // variables

    LocationManager locationManager;
    MarkerOptions markerOptions;
    Location location; // location
    double latitude; // latitude
    double longitude; // longitude
    Context mContext;
    String address = "", intentNote = "", intentFlag = "", intnentAddress = "" , name = "";
    boolean gpsEnabled = false;
    protected SharedPreferences sharedPreferences;
    double intentLat = 0, intentLng = 0;
    int intentId = 0;
    ShowDialog toastDialog;
    Handler handler;
    CustomDialogProgress progress;
    ToolBar toolBarConfig;
    SharedPreferences.Editor editorAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_address);
        ButterKnife.bind(this);
        toastDialog = new ShowDialog();
        mContext = this;
        toolBarConfig = new ToolBar(this);
        toolBarConfig.setTitle(getString(R.string.delivery_address));
        toolBarConfig.addNotification();
        initLocationManager();
        autoComplete();
        getAddressIntent();

    } // function of onCreate

    private void getAddressIntent() {
        Intent intent = getIntent();
        if (intent != null) {
            Log.i("Print", "cart address: " + intentFlag);

            intentFlag = intent.getStringExtra("flag");
            if (intentFlag.equals("add") || intentFlag.equals("update")) {
                intentLat = intent.getDoubleExtra("lat", 0);
                intentLng = intent.getDoubleExtra("lng", 0);
                intnentAddress = intent.getStringExtra("address");
                intentNote = intent.getStringExtra("note");
                intentId = intent.getIntExtra("id", 0);
                name = intent.getStringExtra("name");
            }

            else if (intentFlag.equals("designBanquet") || intentFlag.equals("new")) {
                intentLat = 0;
                intentLng = 0;
                intnentAddress = "";
                intentNote = "";
                intentId = 0;
            }
        }
    } // get addresses saved

    private void initLocationManager() {
        locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                2000,
                10, locationListenerGPS);

        markerOptions = new MarkerOptions();
    } // function of initLocationManager

    LocationListener locationListenerGPS = new LocationListener() {
        @Override
        public void onLocationChanged(android.location.Location location) {

            latitude = location.getLatitude();
            longitude = location.getLongitude();

            Log.i("Print", "listner : ");
            callMap();
            String msg = "New Latitude: " + latitude + "New Longitude: " + longitude;

            //Toast.makeText(mContext, msg, Toast.LENGTH_LONG).show();
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };

    private void callMap() {
        Log.i("Print", "map loaded");
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    } // call map vire

    private void getAddressFromLatandLng(double lat, double lng) {
        Log.i("Print", "Gecode Here ");
        Geocoder geocoder;
        List<Address> addresses = null;
        geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(lat, lng, 1); // Here 1 represent max location result to returned
            address = addresses.get(0).getAddressLine(0);
            tx_address.setText(address);
            Log.i("Print", "address : try " + latitude + " : " + longitude + " : " + address + " : " + addresses);
        } catch (Exception e) {
            Log.i("Print", "address : exception " + latitude + " : " + longitude + " : " + address + " : " + e.toString());
            tx_address.setText(intnentAddress);
        }

    } // function of getAddressFromLatandLng

    @Override
    public void onMapReady(final GoogleMap googleMap) {

        Log.e("Print", "map ready : " + latitude + " : " + longitude);
        // Add a marker in current location
        // and move the map's camera to the same location.

        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

            @Override
            public void onMapClick(LatLng latLng) {

                // Setting the position for the marker
                markerOptions.position(latLng);

                // Setting the title for the marker.
                // This will be displayed on taping the marker
                markerOptions.title(latLng.latitude + " : " + latLng.longitude);

                // Clears the previously touched position
                googleMap.clear();

                // Animating to the touched position
                googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));

                // Placing a marker on the touched position
                googleMap.addMarker(markerOptions);

                longitude = latLng.longitude;
                latitude = latLng.latitude;

                getAddressFromLatandLng(latLng.latitude, latLng.longitude); // get address user_name
            }
        });
        googleMap.clear();
        if (latitude != 0 && longitude != 0) {
            LatLng currentLocation = new LatLng(latitude, longitude);
            googleMap.addMarker(new MarkerOptions().position(currentLocation)
                    .title("Marker in myLocation"));
            googleMap.moveCamera(CameraUpdateFactory.newLatLng(currentLocation));
            zoomInCameraLocation(latitude, longitude, googleMap); // make zoom in location
            getAddressFromLatandLng(latitude, longitude); // get address user_name

        } // if location doesn't null

    } // view map

    private void zoomInCameraLocation(double latitude, double longitude, GoogleMap mMap) {
        CameraUpdate center = CameraUpdateFactory.newLatLng(new LatLng(latitude, longitude));
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(12);
        mMap.moveCamera(center);
        mMap.animateCamera(zoom);
    } // make zoom in location

    private void getLocation() {
        try {

            Log.i("Print", "locationManger : " + locationManager);

            if (ActivityCompat.checkSelfPermission(AddAddressActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(AddAddressActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(AddAddressActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                Log.i("Print", "permission false");
                return;
            } else {
                // Write you code here if permission already given.
                Log.i("Print", "permission true");
            }
            location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (location != null) {
                Log.i("Print", "Location : " + location.getLatitude());
                latitude = location.getLatitude();
                longitude = location.getLongitude();
                callMap();
            } // location not equal null
            else {
                location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                if (location != null) {
                    Log.i("Print", "Location : " + location.getLatitude());
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                    callMap();
                } // location not equal null
                else {
                    Log.i("Print", "Location null on network and gps");
                }
            }

        } catch (SecurityException e) {
            Log.i("Print", "locationManger :exception " + e.toString());
        }
    } // function getlocation

    private void getLocationWithLatAndLng(double lat, double lng) {
        latitude = lat;
        longitude = lng;
        callMap();
    } // function of getLocationWithLatAndLng

    private void autoComplete() {
        PlaceAutocompleteFragment autocompleteFragment = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);

        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // TODO: Get info about the selected place.
                Log.i("Print", "Place: " + place.getLatLng());
                latitude = place.getLatLng().latitude;
                longitude = place.getLatLng().longitude;
                callMap();
            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                Log.i("Print", "An error occurred: " + status);
            }
        });
    } // function of autoComplete

    protected void onResume() {
        super.onResume();
        if (gpsEnabled == false)
            isLocationEnabled("intent");
    }

    private void isLocationEnabled(final String flag) {

        gpsEnabled = true;
        Log.i("Print", "isLocationEnabled");

        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
            alertDialog.setTitle(getString(R.string.enableLocation));
            alertDialog.setMessage(getString(R.string.settingLocationMenu));
            alertDialog.setPositiveButton(getString(R.string.locationSetting), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);

                }
            });
            alertDialog.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });

        final     AlertDialog alert = alertDialog.create();

            alert.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface arg0) {
                    try {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            alert.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorPrimary));
                            alert.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimary));
                        }
                    } catch (Exception e) {
                        e.getMessage();
                        Log.i("Print", "exception" + e.getMessage());
                    }

                }
            });

            alert.show();
        } else {

            if (flag.equals("intent"))
                getLocationWithLatAndLng(intentLat, intentLng);
            else
                getLocation();


        }
    } // function check permission

    public void currentLocationButton(View view) {
        callMap();

        isLocationEnabled("current");
    } // function of current location button

    @Override
    protected void attachBaseContext(Context newBase) {
        sharedPreferences = newBase.getSharedPreferences("user", MODE_PRIVATE);
        super.attachBaseContext(CalligraphyContextWrapper.wrap(new MyContextWrapper(newBase).wrap(sharedPreferences.getString("language", "ar"))));
    }// apply fonts

    public void saveNewAddressButton(View view) {

        name = ed_name_desc.getText().toString();

        if (latitude == 0 || longitude == 0) {
            toastDialog.initDialog(getString(R.string.retry), this);
        } else {
            if (name.equals(""))
                toastDialog.initDialog(getString(R.string.pleaseEnterNameOfNewLocation), this);

            else {
                if (intentFlag.equals("add")  || intentFlag.equals("new") || intentFlag.equals("cart"))
                    addNewAddress();
                else if (intentFlag.equals("update") || intentFlag.equals("cart"))
                    updateAddress();
            }
        }
    } // function of save new address button android


    @SuppressLint("HandlerLeak")
    private void addNewAddress() {
        progress = new CustomDialogProgress();
        progress.init(this);

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
        new Thread() {
            public void run() {

                Retrofit retrofit = RetrofitClient.getInstance();

                final addressRequest addressApi = retrofit.create(addressRequest.class);

                Log.i("Print", "Location : addAddress : " + latitude + " : " + longitude);
                final Call<addressResponse> getInterestConnection = addressApi.addNewAddress(toolBarConfig.getPrefUserId(), address
                        , latitude, longitude , name
                 );

                getInterestConnection.enqueue(new Callback<addressResponse>() {
                    @Override
                    public void onResponse(Call<addressResponse> call, Response<addressResponse> response) {
                        try {
                            String code = response.body().getCode();
                            Log.i("Print", "code" + code);

                            if (code.equals("200")) {
                                toastDialog.initDialog(getString(R.string.addressAdded), AddAddressActivity.this);
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {

                                        Log.i("Print", "intent  flag: " + intentFlag);

                                        editorAddress = getSharedPreferences("AddressPref", MODE_PRIVATE).edit();
                                        editorAddress.putString("address", address);
                                        editorAddress.putFloat("lat", (float) latitude);
                                        editorAddress.putFloat("lng", (float) longitude);
                                 //       editorAddress.putString("flag","flag");
                                        editorAddress.apply();

                                        Log.i("Print", "newAddress : " + latitude + " : " + longitude);
                                        Intent intent = new Intent(AddAddressActivity.this, MenuActivity.class);
                                        intent.putExtra("fragmentFlag", "home");
                                        Log.i("Print", "intent with home flag");
                                        startActivity(intent);
                                        finish();

                                        // Toast.makeText(getContext(), getString(R.string.enterVaildDate), Toast.LENGTH_SHORT).show();
                                    }
                                }, 2000);
                            } else if (code.equals("1313")) {
                                toastDialog.initDialog(getString(R.string.address_exist), AddAddressActivity.this);
                                progress.dismiss();
                                return;
                            }

                            progress.dismiss();

                        } // try
                        catch (Exception e) {
                            Log.i("Print", "exception : " + e.toString());
                            progress.dismiss();
                        } // catch
                    } // onResponse

                    @Override
                    public void onFailure(Call<addressResponse> call, Throwable t) {

                        toastDialog.initDialog(getString(R.string.retry), AddAddressActivity.this);
                        Log.i("Print", "error : " + t.toString());
                        progress.dismiss();
                    } // on Failure
                });

            }

        }.start();
    } // function of addNewAddress

    public int getPrefUserId()
    {
        int userId = 0;
        SharedPreferences preferences = getSharedPreferences("MyPrefsFile",MODE_PRIVATE);

        try {
            if (preferences != null)
                userId = preferences.getInt("id", 0);
        }
        catch (Exception e)
        {
            Log.i("Print" , "Exception" + e.getMessage());
        }

        return  userId;
    } // function of getPrefUserId

    @SuppressLint("HandlerLeak")
    private void updateAddress() {
        progress = new CustomDialogProgress();
        progress.init(this);

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
        new Thread() {
            public void run() {

                Retrofit retrofit = RetrofitClient.getInstance();

                final addressRequest addressApi = retrofit.create(addressRequest.class);

                final Call<addressResponse> getInterestConnection = addressApi.updateUserAddress(intentId, getPrefUserId(), address
                        , latitude, longitude , name
                );

                getInterestConnection.enqueue(new Callback<addressResponse>() {
                    @Override
                    public void onResponse(Call<addressResponse> call, Response<addressResponse> response) {
                        try {
                            String code = response.body().getCode();
                            Log.i("Print", "code" + code);

                            if (code.equals("200")) {
                                toastDialog.initDialog(getString(R.string.addressUpdated), AddAddressActivity.this);
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {

                                        editorAddress = getSharedPreferences("AddressPref", MODE_PRIVATE).edit();
                                        editorAddress.putFloat("lat", (float) latitude);
                                        editorAddress.putFloat("lng", (float) longitude);
                                        editorAddress.apply();

                                        Log.i("Print", "newAddress : " + latitude + " : " + longitude);

                                        Intent intent = new Intent(AddAddressActivity.this, MenuActivity.class);
                                        intent.putExtra("fragmentFlag", "home");
                                        Log.i("Print", "intent with home flag");
                                        startActivity(intent);
                                        finish();

                                    }
                                }, 2000);

                            }

                            progress.dismiss();

                        } // try
                        catch (Exception e) {
                            Log.i("Print", "exception : " + e.toString());
                            progress.dismiss();
                        } // catch
                    } // onResponse

                    @Override
                    public void onFailure(Call<addressResponse> call, Throwable t) {

                        toastDialog.initDialog(getString(R.string.retry), AddAddressActivity.this);
                        Log.i("Print", "error : " + t.toString());
                        progress.dismiss();
                    } // on Failure
                });

            }

        }.start();
    } // function of updateAddress

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (locationManager != null) {
            locationManager.removeUpdates(locationListenerGPS);
        }
        Thread.interrupted();
    } // on destroy


} // class of AddAddressActivity
