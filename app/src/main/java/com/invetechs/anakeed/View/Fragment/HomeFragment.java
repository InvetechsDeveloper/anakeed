package com.invetechs.anakeed.View.Fragment;

import android.Manifest;
import android.animation.ObjectAnimator;
import android.app.Activity;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.google.android.material.button.MaterialButton;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.invetechs.anakeed.Adapter.HomeCategoryAdapter;
import com.invetechs.anakeed.AppConfig.CustomDialogProgress;
import com.invetechs.anakeed.AppConfig.ShowDialog;
import com.invetechs.anakeed.AppConfig.ToolBar;
import com.invetechs.anakeed.Model.HomeAdsResultModel;
import com.invetechs.anakeed.Model.HomeCategory;
import com.invetechs.anakeed.Model.HomeCategoryResultModel;
import com.invetechs.anakeed.R;
import com.invetechs.anakeed.View.Activity.AddAddressActivity;
import com.invetechs.anakeed.View.Activity.ChooseAddressActivity;
import com.invetechs.anakeed.View.Activity.LoginActivity;
import com.invetechs.anakeed.ViewModel.HomeCategoryViewModel;
import com.labo.kaji.fragmentanimations.MoveAnimation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.content.Context.LOCATION_SERVICE;
import static android.content.Context.MODE_PRIVATE;


public class HomeFragment extends Fragment {

    // bind views

    @BindView(R.id.recyclerView_homeCategory)
    RecyclerView recyclerView_homeCategory;

    @BindView(R.id.anakid_image)
    ImageView anakid_image;

    @BindView(R.id.bt_currentLocation)
    MaterialButton bt_currentLocation;

    // variables
    HomeCategoryAdapter homeCategoryAdapter;
    List<HomeCategory> categoryList = new ArrayList<>();
    HomeCategory homeCategory;
    TextSliderView textSliderView;
    SliderLayout sliderShow;
    LocationManager locationManager;
    Location location;
    double latitude = 0;
    double longitude = 0;
    Context mContext;
    boolean enableGps = false;
    String address = "";
    ConnectivityManager connectivityManager;
    String imageUrl = "http://3nakied.invetechs.com/cpanel/upload/Slider/";
    HomeCategoryViewModel viewModel;
    List<HomeCategoryResultModel.CategoriesBean> homeCategoriesList = new ArrayList<>();
    List<HomeAdsResultModel.SlidersBean> imageAdsList = new ArrayList<>();
    CustomDialogProgress progress;
    ToolBar toolBarConfig;
    SharedPreferences.Editor editor;
    ShowDialog toastDialog = new ShowDialog();
    SharedPreferences editorAddress;
    double prefLat, prefLng;
    String flag = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.home_fragment, container, false);
        ButterKnife.bind(this, view);
        toolBarConfig = new ToolBar(getActivity());
        toolBarConfig.addNotification();
        sliderShow = view.findViewById(R.id.slider);
        mContext = getActivity();
        progress = new CustomDialogProgress();
        locationManager = (LocationManager) mContext.getSystemService(LOCATION_SERVICE);
        initHomeCategoryList();

        if (isNetworkAvailable() == true) {
            Log.i("Print", "isNetworkAvailable true");

            initViewModel();

        } else if (isNetworkAvailable() == false){

            Log.i("Print", "isNetworkAvailable false");

            toastDialog.initDialog(getString(R.string.connectionFaild), getActivity());

        }
        getLocationSaved();
        checkPermission();

        try {
            Log.i("Print", "try requestLocationUpdates");

        } catch (Exception e) {
        }
        connectivityManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        try {
            Log.i("Print", "try isLocationEnabled");
            isLocationEnabled();

        } catch (Exception e) {
            toastDialog.initDialog(getString(R.string.connectionFaild), getActivity());
        }

        fadingImage();
        return view;

    } // function of onCreateView

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    public void onResume() {
        super.onResume();

        try {
            getLocationSaved(); // get user location if  choose address  from his addresses
            getLocation();
        } catch (Exception e) {
            Log.i("Print", "Exception onResume" + e.getMessage());

        }
    }


    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        return MoveAnimation.create(MoveAnimation.UP, enter, 3000);
    } // animation

    private void fadingImage() {
        Animation fadeOut = new AlphaAnimation(1, 0);  // the 1, 0 here notifies that we want the opacity to go from opaque (1) to transparent (0)
        fadeOut.setInterpolator(new AccelerateInterpolator());
        fadeOut.setStartOffset(500); // Start fading out after 500 milli seconds
        fadeOut.setDuration(1000);

        anakid_image.setAnimation(fadeOut);
    } // fading image view

    private void getLocationSaved() {

        editorAddress = getContext().getSharedPreferences("AddressPref", MODE_PRIVATE);
        prefLat = editorAddress.getFloat("lat", 0);
        prefLng = editorAddress.getFloat("lng", 0);
        //   address = editorAddress.getString("address", "");

        Log.i("Print", "location prefLat saved from home : " + prefLat);
        Log.i("Print", "location prefLng saved from home : " + prefLng);
        Log.i("Print", "location address saved from home : " + address);

    } // function of getLocationSaved

    @OnClick(R.id.bt_currentLocation)
    public void currentLocationButton() {

        if (toolBarConfig.getPrefUserId() != 0) {
            Intent intent = new Intent(getContext(), ChooseAddressActivity.class);
            intent.putExtra("fragmentFlag", "home");
            startActivity(intent);
        } else {
            Intent intent = new Intent(getContext(), LoginActivity.class);
            getContext().startActivity(intent);
        }
    } // current loaction button , display addresses saved before

    private void initViewModel() {

        viewModel = ViewModelProviders.of(getActivity()).get(HomeCategoryViewModel.class);

        viewModel.fetchHomeCategories().observe(getActivity(), new Observer<HomeCategoryResultModel>() {
            @Override
            public void onChanged(@Nullable HomeCategoryResultModel categoryResultModel) {

                try {
                    homeCategoriesList = categoryResultModel.getCategories();
                    fillHomeCategoryList(categoryResultModel.getCategories().size());

                    Log.e("Print", "homeCategoriesList : " + homeCategoriesList.size());
                } catch (Exception e) {
                    Log.e("Print", "exception : " + e.getMessage());
                }
            }
        });

        viewModel.fetchAds().observe(this, new Observer<HomeAdsResultModel>() {
            @Override
            public void onChanged(@Nullable HomeAdsResultModel homeAdsResultModel) {

                imageAdsList = homeAdsResultModel.getSliders();
                imageSlider();
                Log.i("Print", "ads size in home fragment : " + imageAdsList.size());
            }
        });

    } // function of initViewModel

    @OnClick(R.id.bt_saveLocation)
    public void bt_saveLocation() {
        if (toolBarConfig.getPrefUserId() != 0) {
            Intent intent = new Intent(getContext(), AddAddressActivity.class);
            intent.putExtra("id", 0);
            intent.putExtra("lat", latitude);
            intent.putExtra("lng", longitude);
            intent.putExtra("note", "0");
            intent.putExtra("address", address);
            intent.putExtra("flag", "add");
            startActivity(intent);
        } else {
            Intent intent = new Intent(getContext(), LoginActivity.class);
            getContext().startActivity(intent);
        }
    } // function of bt_saveLocation

    private void imageSlider() {

        for (int i = 0; i < imageAdsList.size(); i++) {
            textSliderView = new TextSliderView(getContext());
            textSliderView
                    .image(imageUrl + imageAdsList.get(i).getImg());
            sliderShow.addSlider(textSliderView);
            sliderShow.setPresetTransformer(SliderLayout.Transformer.DepthPage);
            sliderShow.setCustomIndicator((PagerIndicator) getActivity().findViewById(R.id.custom_indicator));
            sliderShow.setCustomAnimation(new DescriptionAnimation());
            sliderShow.setDuration(3000);
        }

    } // function of imageSlider

    private void initHomeCategoryList() {
        homeCategoryAdapter = new HomeCategoryAdapter(getContext(), categoryList);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView_homeCategory.setLayoutManager(layoutManager);
        recyclerView_homeCategory.setHasFixedSize(true);
        recyclerView_homeCategory.setAdapter(homeCategoryAdapter);

    } // function of initHomeCategoryList

    private void fillHomeCategoryList(int size) {

        if (categoryList.size() < 0) {
            homeCategoriesList.clear();
        }

        for (int i = 0; i < size; i++) {
            homeCategory = new HomeCategory(homeCategoriesList.get(i).getColor(), homeCategoriesList.get(i).getImg()
                    , homeCategoriesList.get(i).getEn_title(),
                    homeCategoriesList.get(i).getAr_title());

            categoryList.add(homeCategory);

            homeCategory.setId(homeCategoriesList.get(i).getId());

            Log.i("Print", "setttttt" + homeCategoriesList.get(i).getId());

        }

        homeCategoryAdapter.notifyDataSetChanged();


    } // function of fillHomeCategoryList

    private void isLocationEnabled() {
        Log.i("Print", "isLocationEnabled");

        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
                || !locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {

            AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
            alertDialog.setTitle(getString(R.string.enableLocation));
            alertDialog.setMessage(getString(R.string.settingLocationMenu));

            alertDialog.setPositiveButton(getString(R.string.locationSetting), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);
                }
            }); // positive button

            alertDialog.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            }); // negative button

            final AlertDialog alert = alertDialog.create();
            alert.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface arg0) {
                    try {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            alert.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorPrimary));
                            alert.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimary));
                        }
                    } catch (Exception e) {
                        e.getMessage();
                        Log.i("Print", "exception" + e.getMessage());
                    }

                }
            });

            alert.show();
            Log.e("Print", "GPS Disabled");
        } else {
            getLocation();
            enableGps = true;
        }

    }// function check permission

    private void checkPermission() {
        locationManager = (LocationManager) mContext.getSystemService(LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.i("Print", "checkPermission");
            return;
        }
    } //checkPermission

    private void getCurrentLocation() {

        locationManager = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);
        List<String> providers = locationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            checkPermission();
            Location l = locationManager.getLastKnownLocation(provider);
            if (l == null) {
                continue;
            }
            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                // Found best last known location: %s", l);
                bestLocation = l;
            }
        }

        if (bestLocation != null) {
            latitude = bestLocation.getLatitude();
            longitude = bestLocation.getLongitude();
            if (prefLat != 0 && prefLng != 0) {
                latitude = prefLat;
                longitude = prefLng;
            }
            getAddressFromLatandLng(latitude, longitude);
            Log.i("Print", "current Location : " + bestLocation);
        } // bestlocation not equal null
        else {
            Log.i("Print", "current Location : Null"); //

        } // location equal null
    } // function of getCurrentLocation

    private void getLocation() {
        try {

            Log.i("Print", "locationManger : " + locationManager);

            if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions((Activity) mContext, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 10);
                Log.i("Print", "permission false");
                return;
            } else {
                // Write you code here if permission already given.
                Log.i("Print", "permission true");
            }
            location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (location != null) {
                //Log.i("Print","Location : "+location.getLatitude()+" : "+location.getLongitude());
                latitude = location.getLatitude();
                longitude = location.getLongitude();
                if (prefLat != 0 && prefLng != 0) {
                    latitude = prefLat;
                    longitude = prefLng;
                }
                getAddressFromLatandLng(latitude, longitude);

            } // location not equal null
            else {
                location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                if (location != null) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                    if (prefLat != 0 && prefLng != 0) {
                        latitude = prefLat;
                        longitude = prefLng;
                    }
                    Log.i("Print", "Location : " + latitude + " : " + longitude);
                    getAddressFromLatandLng(latitude, longitude);

                } // location not equal null
                else {
                    Log.i("Print", "Location null on network and gps");
                    getCurrentLocation();
                }
            }
        } catch (SecurityException e) {
            Log.i("Print", "locationManger :exception " + e.toString());
        }
    } // function get location

    private void getAddressFromLatandLng(double lat, double lng) {
        Geocoder geocoder;
        List<Address> addresses = null;
        geocoder = new Geocoder(mContext, Locale.getDefault());
        try {
            addresses = geocoder.getFromLocation(lat, lng, 1); // Here 1 represent max location result to returned
            address = addresses.get(0).getAddressLine(0);

            Log.i("Print", "address wanted : " + address);

            editor = getContext().getSharedPreferences("AddressPref", MODE_PRIVATE).edit();
            editor.putString("address", address);
            editor.putFloat("lat", (float) lat);
            editor.putFloat("lng", (float) lng);
            editor.apply();
            bt_currentLocation.setText(address);
        } catch (Exception e) {
            toastDialog.initDialog(getString(R.string.connectionFaild), getActivity());
            return;
        }

      /*  String city = addresses.get(0).getLocality();
        String state = addresses.get(0).getAdminArea();
        String country = addresses.get(0).getCountryName();
        String postalCode = addresses.get(0).getPostalCode();
        String knownName = addresses.get(0).getFeatureName();
*/


    } // function of getAddressFromLatandLng

    private boolean checkInternetConnectionAndGPS() {
        connectivityManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {

            if (enableGps == false || latitude == 0 || longitude == 0) {
                isLocationEnabled();
                return false;
            } else {

                return true;
            }

        } // check internet connection
        else {
            return false;
        }
    } // function check internet connection and confirm gps open

    LocationListener locationListenerGPS = new LocationListener() {
        @Override
        public void onLocationChanged(android.location.Location location) {
            //locationManager.removeUpdates(locationListenerGPS);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    }; // locationlistner

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (locationManager != null) {
            locationManager.removeUpdates(locationListenerGPS);
        }
        Thread.interrupted();
        sliderShow.removeAllSliders();
        sliderShow.stopAutoCycle();
        imageAdsList.clear();
        homeCategoriesList.clear();
    } // on destroy

    @Override
    public void onStop() {
        sliderShow.stopAutoCycle();
        super.onStop();
    } // on stop
} // class of HomeFragment
