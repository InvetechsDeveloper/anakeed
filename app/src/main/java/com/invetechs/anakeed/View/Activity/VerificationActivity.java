package com.invetechs.anakeed.View.Activity;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.invetechs.anakeed.AppConfig.CustomDialogProgress;
import com.invetechs.anakeed.AppConfig.ShowDialog;
import com.invetechs.anakeed.Language.MyContextWrapper;
import com.invetechs.anakeed.R;

import com.invetechs.anakeed.Retrofit.connection.RetrofitClient;
import com.invetechs.anakeed.Retrofit.auth.forget_password.forgetPasswordApi;
import com.invetechs.anakeed.Retrofit.auth.forget_password.forgetResponse;
import com.invetechs.anakeed.Retrofit.auth.login.loginResponse;
import com.invetechs.anakeed.Retrofit.auth.verification.verifyApiRequest;
import com.invetechs.anakeed.Retrofit.auth.verification.verifyForgetPasswordRequest;
import com.rengwuxian.materialedittext.MaterialEditText;

public class VerificationActivity extends AppCompatActivity {


    // bind views
    @BindView(R.id.et_verify_code)
    MaterialEditText et_verify_code;

    @BindView(R.id.tv_resend_code)
    TextView tv_resend_code;

    @BindView(R.id.tool_bar)
    Toolbar tool_bar;

    // variables
    ShowDialog toastDialog;
    protected SharedPreferences sharedPreferences;
    String mobile = "", code = "", phone = "", token = "", verifi = "";
    Boolean verification;
    int userId;
    CustomDialogProgress progress;
    Handler handler;
    SharedPreferences.Editor editor;
    public static final String MY_PREFS_NAME = "MyPrefsFile";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification);
        ButterKnife.bind(this);
        toastDialog = new ShowDialog();
        initToolBar();
        getDataFromIntent();
    }

    @OnClick(R.id.tv_resend_code)
    public void resendCode() {

        new CountDownTimer(60000, 1000) {

            public void onTick(long millisUntilFinished) {
                tv_resend_code.setText(getString(R.string.second_remaining) + " " + millisUntilFinished / 1000);
                tv_resend_code.setEnabled(false);
            }

            public void onFinish() {
                resendVerCode();
                tv_resend_code.setText(getString(R.string.resend_code));
                tv_resend_code.setEnabled(true);

            }
        }.start();

    } // click on resend code again

    @SuppressLint("HandlerLeak")
    private void resendVerCode() {

        Log.i("Print", "resendVerCode method: " + verification);

        progress = new CustomDialogProgress();
        progress.init(this);

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }
        };
        progress.show();
        new Thread() {
            public void run() {
//Retrofit
                Retrofit retrofit = RetrofitClient.getInstance();

                final forgetPasswordApi userApi = retrofit.create(forgetPasswordApi.class);

                Log.i("Print", "resend verification mobile : " + phone);

                final Call<forgetResponse> getInterestConnection = userApi.forgetPass(phone);

                getInterestConnection.enqueue(new Callback<forgetResponse>() {
                    @Override
                    public void onResponse(Call<forgetResponse> call, Response<forgetResponse> response) {
                        try {

                            String code = response.body().getCode();
                            Log.i("Print", "resend verification code : " + code);

                            if (code.equals("200")) {
                                forgetResponse.DataBean user_data = response.body().getData();
                                verifi = user_data.getVerificationcode();
                                Log.i("Print", "resend verification : " + verifi);
                              //  toastDialog.initDialog(getString(R.string.code) + "  " + verifi, VerificationActivity.this);
                                progress.dismiss();

                            } else if (code.equals("1313")) {
                                toastDialog.initDialog(getString(R.string.invalid_mobile), VerificationActivity.this);
                                progress.dismiss();
                            }

                            progress.dismiss();

                        } // try
                        catch (Exception e) {
                            Log.i("Print", "exception : " + e.getMessage());
                            progress.dismiss();
                        } // catch
                    } // onResponse

                    @Override
                    public void onFailure(Call<forgetResponse> call, Throwable t) {

                        toastDialog.initDialog(getString(R.string.retry), VerificationActivity.this);
                        Log.i("Print", "error : " + t.toString());
                        progress.dismiss();
                    } // on Failure
                });
// Retrofit

            }

        }.start();
    } // function of forget password

    private void getDataFromIntent() {

        verification = getIntent().getExtras().getBoolean("verify");
        phone = getIntent().getExtras().getString("mobilenumber");
        token = getIntent().getExtras().getString("verification_code");

        Log.i("Print", "user id in verification activity: " + userId);

        Intent intent = getIntent();
        if (intent != null) {
            userId = intent.getIntExtra("id", 0);
            mobile = intent.getStringExtra("mobilenumber");
            Log.i("Print", "user id in verification activity: " + userId);
            Log.i("Print", "mobile number : " + mobile);
            Log.i("Print", "verification_code : " + token);

            if (token != null) {
                toastDialog.initDialog(getString(R.string.code_send) , VerificationActivity.this);
            }
        }

    } // get data from login

    @OnClick(R.id.btn_code_confirm)
    public void confirmClick() {
        code = et_verify_code.getText().toString().trim();
        phone = getIntent().getExtras().getString("mobilenumber");

        Log.i("Print", "allCode : " + code);
        Log.i("Print", "phone : " + phone);
        verify();
    } // confirm code

    @SuppressLint("HandlerLeak")
    private void verify() {

        if (verification == true) {

            Log.i("Print", "verification == true  : ");


            progress = new CustomDialogProgress();
            progress.init(this);
            handler = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    progress.dismiss();
                    super.handleMessage(msg);
                }
            };
            progress.show();
            new Thread() {
                public void run() {

                    Retrofit retrofit = RetrofitClient.getInstance();

                    final verifyForgetPasswordRequest userApi = retrofit.create(verifyForgetPasswordRequest.class);

                    Log.i("Print", "phoneeee  : " + phone);
                    Log.i("Print", "codeeeeee  : " + code);


                    final Call<loginResponse> getInterestConnection = userApi.verifyForgetPass(phone, code);
                    getInterestConnection.enqueue(new Callback<loginResponse>() {
                        @Override
                        public void onResponse(Call<loginResponse> call, Response<loginResponse> response) {
                            try {

                                Log.i("Print", "response  : " + response + "\n" + mobile + " : " + code);

                                String code = response.body().getCode();
                                Log.i("Print", "responseeeeeeeee  : " + code);
                                if (code.equals("200")) {
                                    editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                                    editor.putString("isLogin", "true");
                                    loginResponse.UserdataBean user = response.body().getUserdata();
                                    editor.putInt("id", user.getId());
                                    editor.putString("isLogin", "true");
                                    editor.putString("user_name", user.getName());
                                    editor.putString("email", user.getEmail());
                                    editor.putString("verify", "0");
                                    editor.putString("mobilenumber", user.getPhone());
                                    editor.apply();
                                    editor.apply();
                                    Intent intent = new Intent(VerificationActivity.this, MenuActivity.class);
                                    intent.putExtra("fragmentFlag", "home");
                                    startActivity(intent);
                                    finish();
                                    progress.dismiss();

                                } else if (code.equals("1313")) {
                                    toastDialog.initDialog(getString(R.string.wrong_token), VerificationActivity.this);
                                    progress.dismiss();
                                }

                                progress.dismiss();

                            } // try
                            catch (Exception e) {
                                Log.i("Print", "exception : " + e.toString());
                                progress.dismiss();
                            } // catch
                        } // onResponse

                        @Override
                        public void onFailure(Call<loginResponse> call, Throwable t) {

                            toastDialog.initDialog(getString(R.string.retry), VerificationActivity.this);
                            Log.i("Print", "error : " + t.toString());
                            progress.dismiss();
                        } // on Failure
                    });
// Retrofit

                }

            }.start();
        }

        else {

            Log.i("Print", "verification == false  : ");


            progress = new CustomDialogProgress();
            progress.init(this);

            handler = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    progress.dismiss();
                    super.handleMessage(msg);
                }

            };
            progress.show();
            new Thread() {
                public void run() {
//Retrofit
                    Retrofit retrofit = RetrofitClient.getInstance();

                    final verifyApiRequest userApi = retrofit.create(verifyApiRequest.class);

                    final Call<loginResponse> getInterestConnection = userApi.verify(userId, code);

                    getInterestConnection.enqueue(new Callback<loginResponse>() {
                        @Override
                        public void onResponse(Call<loginResponse> call, Response<loginResponse> response) {
                            try {

                                String code = response.body().getCode();

                                Log.i("Print", "code  : " + code);

                                if (code.equals("200")) {

                                    loginResponse.UserdataBean user = response.body().getUserdata();
                                    progress.dismiss();
                                    SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                                    editor.putInt("id", user.getId());
                                    editor.putString("isLogin", "true");
                                    editor.putString("user_name", user.getName());
                                    editor.putString("email", user.getEmail());
                                    editor.putString("verify", "0");
                                    editor.putString("mobilenumber", user.getPhone());
                                    editor.apply();
                                    Intent intent = new Intent(VerificationActivity.this, MenuActivity.class);
                                    intent.putExtra("fragmentFlag", "home");
                                    startActivity(intent);
                                    finish();
                                } else if (code.equals("1313")) {
                                    toastDialog.initDialog(getString(R.string.wrong_token), VerificationActivity.this);
                                } else if (code.equals("1314")) {
                                    toastDialog.initDialog(getString(R.string.user_verified), VerificationActivity.this);
                                }
                                progress.dismiss();

                            } // try
                            catch (Exception e) {
                                Log.i("Print", "exception : " + e.toString());
                                progress.dismiss();
                            } // catch
                        } // onResponse

                        @Override
                        public void onFailure(Call<loginResponse> call, Throwable t) {

                            toastDialog.initDialog(getString(R.string.retry), VerificationActivity.this);
                            Log.i("Print", "error : " + t.toString());
                            progress.dismiss();
                        } // on Failure
                    });
                }

            }.start();
        } // verify

    } // verify the code

    private void initToolBar() {
        setSupportActionBar(tool_bar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);

        tool_bar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    } // init toolBar

    @Override
    protected void attachBaseContext(Context newBase) {
        sharedPreferences = newBase.getSharedPreferences("user", MODE_PRIVATE);
        super.attachBaseContext(CalligraphyContextWrapper.wrap(new MyContextWrapper(newBase).wrap(sharedPreferences.getString("language", "ar"))));
    }// apply fonts
}
