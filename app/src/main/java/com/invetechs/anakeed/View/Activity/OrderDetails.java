package com.invetechs.anakeed.View.Activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.invetechs.anakeed.AppConfig.CustomDialogProgress;
import com.invetechs.anakeed.AppConfig.ShowDialog;
import com.invetechs.anakeed.Language.MyContextWrapper;
import com.invetechs.anakeed.R;
import com.invetechs.anakeed.Retrofit.connection.RetrofitClient;
import com.invetechs.anakeed.Retrofit.orders.getOrderDetails;
import com.invetechs.anakeed.Retrofit.orders.orderDetailsResponse;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.widget.Toolbar;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class OrderDetails extends AppCompatActivity {

    // bind views
    @BindView(R.id.tv_order_number)
    TextView tv_order_number;

    @BindView(R.id.tv_status)
    TextView tv_status;

    @BindView(R.id.tv_order_date)
    TextView tv_order_date;

    @BindView(R.id.tv_order_time)
    TextView tv_order_time;

    @BindView(R.id.tv_delivery_destintation)
    TextView tv_delivery_destintation;

    @BindView(R.id.tv_total_invoice)
    TextView tv_total_invoice;

    @BindView(R.id.tv_delivery_fees)
    TextView tv_delivery_fees;

    @BindView(R.id.tv_value_added)
    TextView tv_value_added;

    @BindView(R.id.tv_total)
    TextView tv_total;

    @BindView(R.id.tv_order_name)
    TextView tv_order_name;

    @BindView(R.id.tv_order_quantity)
    TextView tv_order_quantity;

    @BindView(R.id.tool_bar)
    Toolbar tool_bar;


    // vars
    protected SharedPreferences sharedPreferences;
    Handler handler;
    CustomDialogProgress progress;
    ShowDialog showDialog = new ShowDialog();
    String orderId = "", order_number = "", status = "", order_date = "", order_time = "", address = "", total_invoice = "", value_added = "", delivery_fees = "", total = "";
    private orderDetailsResponse.OrderBean order;
    private List<orderDetailsResponse.OrderBean.ProductsBean> productsApi = new ArrayList<>();
    String productsArray = "" , productsQuantity = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);
        ButterKnife.bind(this);
        initToolBar();
        getIdFromOrderList(); // get Id From OrderList
        getOrderDetails(); //getOrderDetails with id
    }

    private void initToolBar() {
        setSupportActionBar(tool_bar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);

        }
        tool_bar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    } // initialize tool bar

    private void getIdFromOrderList() {
        Intent intent = getIntent();
        if (intent != null) {

            orderId = intent.getStringExtra("orderId");
            Log.i("Print", "order id in order details : " + orderId);
        }
    }

    public String convertToArabic(String value) {
        String newValue = (((((((((((value + "")
                .replaceAll("1", "١")).replaceAll("2", "٢"))
                .replaceAll("3", "٣")).replaceAll("4", "٤"))
                .replaceAll("5", "٥")).replaceAll("6", "٦"))
                .replaceAll("7", "٧")).replaceAll("8", "٨"))
                .replaceAll("9", "٩")).replaceAll("0", "٠"));

        return newValue;
    } // convert arabic date to english to send to api

    @SuppressLint("HandlerLeak")
    private void getOrderDetails() {
        progress = new CustomDialogProgress();
        progress.init(this);
        handler = new Handler() {

            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
      /*  new Thread() {
            public void run() {*/
//Retrofit

        Retrofit retrofit = RetrofitClient.getInstance();

        final getOrderDetails orderApi = retrofit.create(getOrderDetails.class);

        final Call<orderDetailsResponse> getInterestConnection = orderApi.getDetailsOrder(orderId);

        getInterestConnection.enqueue(new Callback<orderDetailsResponse>() {
            @Override
            public void onResponse(Call<orderDetailsResponse> call, Response<orderDetailsResponse> response) {
                try {

                    String code = response.body().getCode();
                    Log.i("Print", "code" + code);

                    if (code.equals("200")) {
                        if (response.body() != null) {
                            order = response.body().getOrder();
                            getDetailsData();
                        } else {
                            showDialog.initDialog(getString(R.string.retry), OrderDetails.this);
                        }

                    } // found order
                    else if (code.equals("1313")) {

                    } // not found

                    progress.dismiss();

                } // try
                catch (Exception e) {
                    Log.i("Print", "exception : " + e.toString());
                    progress.dismiss();
                } // catch
            } // onResponse

            @Override
            public void onFailure(Call<orderDetailsResponse> call, Throwable t) {
                showDialog.initDialog(getString(R.string.retry), OrderDetails.this);
                Log.i("Print", "error : " + t.toString());
                progress.dismiss();
            } // on Failure
        });
// Retrofit

           /* }
        }.start();*/
    } // function of getOrderDetails

    private void getDetailsData() {


        // set status types

        if (order.getType().equals("current")) {
            status = getString(R.string.new_order);
        } // new order

        else if (order.getType().equals("finished")) {
            status = getString(R.string.finished);

        } // finished order
        else if (order.getType().equals("accepted")) {
            status = getString(R.string.accepted);
        } // accepted order

        else if (order.getType().equals("rejected")) {
            status = getString(R.string.rejected);

        } // rejected order

        // set arabic number when app in arabic language
        sharedPreferences = getSharedPreferences("user", MODE_PRIVATE);

        if (sharedPreferences.getString("language", "ar").equals("ar")) {

            Log.i("Print", "font App in order details : ar");

            order_number = convertToArabic(String.valueOf(order.getId()));
            order_date = convertToArabic(order.getData());
            order_time = convertToArabic(order.getTime());
            total_invoice = convertToArabic(String.valueOf(order.getTotal()));
            value_added = convertToArabic(String.valueOf(order.getAdded_taxes()));
            delivery_fees = convertToArabic(String.valueOf(order.getDelivery()));
            total = convertToArabic(String.valueOf(order.getTotal_price()));

            // set products name and quantity
            if (order.getProducts() != null) {
                productsApi = order.getProducts();

                for (int i = 0; i < productsApi.size(); i++) {
                    productsArray = productsArray + productsApi.get(i).getAr_title() + "\n";
                    productsQuantity = productsQuantity + productsApi.get(i).getAmount() + "\n";
                }
            }

            tv_order_name.setText(productsArray);
            tv_order_quantity.setText(convertToArabic(productsQuantity));

        }  // arabic language

        else {
            Log.i("Print", "font App in order details : en");
            order_number = String.valueOf(order.getId());
            order_date = order.getData();
            order_time = order.getTime();
            total_invoice = String.valueOf(order.getTotal());
            value_added = String.valueOf(order.getAdded_taxes());
            delivery_fees = String.valueOf(order.getDelivery());
            total = String.valueOf(order.getTotal_price());




            // set products name and quantity
            if (order.getProducts() != null) {
                productsApi = order.getProducts();

                for (int i = 0; i < productsApi.size(); i++) {
                    productsArray = productsArray + productsApi.get(i).getEn_title() + "\n";
                    productsQuantity = productsQuantity + productsApi.get(i).getAmount() + "\n";
                }
            }

            Log.i("Print", "quantitiy in details function : " + productsQuantity);


            tv_order_name.setText(productsArray);
            tv_order_quantity.setText(productsQuantity);

        } // english language


        address = order.getAddress();


        Log.i("Print", "id in details function : " + order_number);

        setValues();

    } // get details data

    private void setValues() {
        tv_order_number.setText(order_number);
        tv_status.setText(status);
        tv_order_date.setText(order_date);
        tv_order_time.setText(order_time);
        tv_delivery_destintation.setText(address);
        tv_total_invoice.setText(total_invoice);
        tv_value_added.setText(value_added);
        tv_delivery_fees.setText(delivery_fees);
        tv_total.setText(total);

    } // set texts with data


    @Override
    protected void attachBaseContext(Context newBase) {
        sharedPreferences = newBase.getSharedPreferences("user", MODE_PRIVATE);
        super.attachBaseContext(CalligraphyContextWrapper.wrap(new MyContextWrapper(newBase).wrap(sharedPreferences.getString("language", "ar"))));
    }// apply fonts

}
