package com.invetechs.anakeed.View.Activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.TextView;

import com.invetechs.anakeed.Adapter.CartAdapter;
import com.invetechs.anakeed.AppConfig.CheckUserLogin;
import com.invetechs.anakeed.AppConfig.CustomDialogProgress;
import com.invetechs.anakeed.AppConfig.ShowDialog;
import com.invetechs.anakeed.AppConfig.ToolBar;
import com.invetechs.anakeed.Language.MyContextWrapper;
import com.invetechs.anakeed.R;
import com.invetechs.anakeed.Retrofit.connection.RetrofitClient;
import com.invetechs.anakeed.Retrofit.cart.checkOutRequest;
import com.invetechs.anakeed.Retrofit.cart.getBasketRequest;
import com.invetechs.anakeed.Retrofit.cart.getBasketResponse;

import java.util.ArrayList;
import java.util.List;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class CartActivity extends AppCompatActivity {

    // bind views

    @BindView(R.id.tv_address)
    TextView tv_address;

    @BindView(R.id.tv_total_invoice)
    TextView tv_total_invoice;

    @BindView(R.id.tv_value_added)
    TextView tv_value_added;

    @BindView(R.id.tv_delivery_fees)
    TextView tv_delivery_fees;

    @BindView(R.id.tv_total)
    TextView tv_total;

    @BindView(R.id.cart_recycler_view)
    RecyclerView cart_recycler_view;

    // variables

    protected SharedPreferences sharedPreferences;
    SharedPreferences editorAddress;
    double prefLat, prefLng;
    String address = "";
    LinearLayoutManager layoutManager;
    Handler handler;
    CustomDialogProgress progress = new CustomDialogProgress();
    ShowDialog toastDialog = new ShowDialog();
    ToolBar toolBarConfig;
    CartAdapter cartAdapter = new CartAdapter();
    private List<getBasketResponse.DataBean> cartList = new ArrayList<>();
    private getBasketResponse.DetailsBean details;
    SharedPreferences prefs;
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    int user_id = 0;
    CheckUserLogin checkUserLogin;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        ButterKnife.bind(this);
        checkUserLogin = new CheckUserLogin();
        setToolBarConfig();
        getUserId();
        getLocationSaved();
        getBasketData();
      }


    @OnClick(R.id.btn_complete)
    public void checkOut() {

        // check if user not login

        try {
            if (checkUserLogin.check(this)) {

                Log.i("Print", "user login and will checkOut");
            } else {
                checkUserLogin.openLoginActivity(this);
                Log.i("Print", "user not login and will go to login page");
            }
        } catch (Exception e) {
            Log.i("Print", "exception : " + e.toString());

        }

        checkOutApi();
    } // click check out

    @SuppressLint("HandlerLeak")
    public void checkOutApi() {

        progress = new CustomDialogProgress();
        progress.init(this);

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
        new Thread() {
            public void run() {

                Retrofit retrofit = RetrofitClient.getInstance();

                final checkOutRequest basketRequest = retrofit.create(checkOutRequest.class);

                final Call<getBasketResponse> getInterestConnection = basketRequest.checkOut(user_id, prefLat, prefLng);

                getInterestConnection.enqueue(new Callback<getBasketResponse>() {
                    @Override
                    public void onResponse(Call<getBasketResponse> call, Response<getBasketResponse> response) {
                        try {
                            String code = response.body().getCode();

                            Log.i("Print", "code" + code);

                            response.body();

                            Log.i("Print", "response" + response.body().getCode());

                            if (code.equals("200")) {
                                Log.i("Print", "checkOut success" + response.body().getCode());

                                toastDialog.initDialog(getString(R.string.chek_success), CartActivity.this);
                                Intent home = new Intent(CartActivity.this, MenuActivity.class);
                                home.putExtra("fragmentFlag", "home");
                                startActivity(home);
                                finish();

                            } // login success

                            else if (code.equals("1313")) {
                                toastDialog.initDialog(getString(R.string.cart_empty), CartActivity.this);
                                progress.dismiss();
                            }

                            progress.dismiss();

                        } // try
                        catch (Exception e) {
                            Log.i("Print", "exception : " + e.toString());
                            progress.dismiss();
                        } // catch
                    } // onResponse

                    @Override
                    public void onFailure(Call<getBasketResponse> call, Throwable t) {
                        toastDialog.initDialog(getString(R.string.retry), CartActivity.this);
                        Log.i("Print", "error : " + t.toString());
                        progress.dismiss();
                    } // on Failure
                });
            }

        }.start();
    } // chck out basket

    private void getUserId() {
        prefs = getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE);
        user_id = prefs.getInt("id", 0);
        Log.i("Print", "userId in cart activty: " + user_id);

    } // getUserId function

    private void setToolBarConfig() {
        toolBarConfig = new ToolBar(this);
        toolBarConfig.setTitle(getString(R.string.cart));
        toolBarConfig.addNotification();
        toolBarConfig.badge_cart.setOnClickListener(null);

    } // function of setToolBarConfig

    private void getLocationSaved() {

        try {
            editorAddress = getSharedPreferences("AddressPref", MODE_PRIVATE);
            prefLat = editorAddress.getFloat("lat", 0);
            prefLng = editorAddress.getFloat("lng", 0);
            address = editorAddress.getString("address", "");

            Log.i("Print", "location prefLat in cart : " + prefLat);
            Log.i("Print", "location prefLng in cart : " + prefLng);
            Log.i("Print", "location address in cart : " + address);
        } catch (Exception e) {
            Log.i("Print", "get location Exception cart : " + e.getMessage());
        }

    } // function of getLocationSaved

    private void initRecyclerView() {

        layoutManager = new LinearLayoutManager(this);
        cartAdapter = new CartAdapter(this, cartList);
        cart_recycler_view.setHasFixedSize(true);
        cart_recycler_view.setLayoutManager(layoutManager);
        cart_recycler_view.setAdapter(cartAdapter);
        cartAdapter.notifyDataSetChanged();

    } // initialize recycler view

    public String convertToArabic(String value) {
        String newValue = (((((((((((value + "")
                .replaceAll("1", "١")).replaceAll("2", "٢"))
                .replaceAll("3", "٣")).replaceAll("4", "٤"))
                .replaceAll("5", "٥")).replaceAll("6", "٦"))
                .replaceAll("7", "٧")).replaceAll("8", "٨"))
                .replaceAll("9", "٩")).replaceAll("0", "٠"));

        return newValue;
    } // convert arabic date to english to send to api

    @SuppressLint("HandlerLeak")
    public void getBasketData() {

        cartList.clear();

        progress = new CustomDialogProgress();
        progress.init(this);

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
        new Thread() {
            public void run() {

                Retrofit retrofit = RetrofitClient.getInstance();

                final getBasketRequest basketRequest = retrofit.create(getBasketRequest.class);

                final Call<getBasketResponse> getInterestConnection = basketRequest.getBasket(user_id, prefLat, prefLng , address , "current_location");

                getInterestConnection.enqueue(new Callback<getBasketResponse>() {
                    @Override
                    public void onResponse(Call<getBasketResponse> call, Response<getBasketResponse> response) {
                        try {
                            String code = response.body().getCode();

                            Log.i("Print", "code" + code);

                            response.body();

                            Log.i("Print", "response" + response.body().getCode());

                            if (code.equals("200")) {

                                cartList = response.body().getData();
                                initRecyclerView();
                                details = response.body().getDetails();

                                sharedPreferences = getSharedPreferences("user", MODE_PRIVATE);

                                if (sharedPreferences.getString("language", "ar").equals("ar")) {
                                    tv_total_invoice.setText(convertToArabic("" + details.getTotal_invioce() + "   " + getString(R.string.sr)));
                                    tv_value_added.setText(convertToArabic("" + details.getAdded_values() + "   " + getString(R.string.sr)));
                                    tv_delivery_fees.setText(convertToArabic("" + String.valueOf(details.getDeleviry() + "   " + getString(R.string.sr))));
                                    tv_total.setText(convertToArabic("" + String.valueOf(details.getTotal()) + "   " + getString(R.string.sr)));
                                } else if (sharedPreferences.getString("language", "ar").equals("en")) {
                                    tv_total_invoice.setText("" + details.getTotal_invioce() + "   " + getString(R.string.sr));
                                    tv_value_added.setText("" + details.getAdded_values() + "   " + getString(R.string.sr));
                                    tv_delivery_fees.setText("" + String.valueOf(details.getDeleviry() + "   " + getString(R.string.sr)));
                                    tv_total.setText("" + String.valueOf(details.getTotal()) + "   " + getString(R.string.sr));
                                }

                                tv_address.setText(address);

                            } // login success

                            else if (code.equals("1313")) {
                                tv_total_invoice.setText("");
                                tv_value_added.setText("");
                                tv_delivery_fees.setText("");
                                tv_total.setText("");
                                toastDialog.initDialog(getString(R.string.cart_empty), CartActivity.this);
                                progress.dismiss();
                            }

                            progress.dismiss();

                        } // try
                        catch (Exception e) {
                            Log.i("Print", "exception : " + e.toString());
                            progress.dismiss();
                        } // catch
                    } // onResponse

                    @Override
                    public void onFailure(Call<getBasketResponse> call, Throwable t) {
                        toastDialog.initDialog(getString(R.string.retry), CartActivity.this);
                        Log.i("Print", "error : " + t.toString());
                        progress.dismiss();
                    } // on Failure
                });
            }

        }.start();
    } // get basket

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Thread.interrupted();
        toolBarConfig.onDestroy();

        try {
            progress.dismiss();

        } catch (Exception e) {

        }
    } // on destroy


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void attachBaseContext(Context newBase) {
        sharedPreferences = newBase.getSharedPreferences("user", MODE_PRIVATE);
        super.attachBaseContext(new MyContextWrapper(newBase).wrap(sharedPreferences.getString("language", "ar")));
    }// apply fonts


}
