package com.invetechs.anakeed.View.Fragment;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;

import com.invetechs.anakeed.Adapter.AddressesAdapter;
import com.invetechs.anakeed.AppConfig.CustomDialogProgress;
import com.invetechs.anakeed.AppConfig.ShowDialog;
import com.invetechs.anakeed.AppConfig.ToolBar;
import com.invetechs.anakeed.R;
import com.invetechs.anakeed.Retrofit.connection.RetrofitClient;
import com.invetechs.anakeed.Retrofit.addresses.addressRequest;
import com.invetechs.anakeed.Retrofit.addresses.addressResponse;
import com.labo.kaji.fragmentanimations.CubeAnimation;
import com.labo.kaji.fragmentanimations.FlipAnimation;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static android.content.Context.MODE_PRIVATE;


public class AddressesFragment extends Fragment {
    // bind views
    @BindView(R.id.addresses_recycler_view)
    RecyclerView addresses_recycler_view;

    // variables
    List<addressResponse.DataBean> addressListApi = new ArrayList<>();
    List<addressResponse.DataBean> addressList = new ArrayList<>();
    Handler handler;
    CustomDialogProgress progress;
    ShowDialog toastDialog = new ShowDialog();
    AddressesAdapter adapter;
    ToolBar toolBar;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.addresses_fragment, container, false);
        ButterKnife.bind(this, view);
        toolBar = new ToolBar(getActivity());
        toolBar.setTitle(getString(R.string.addresses));
        initRecycelerView(); // intialize reclerview
        try {
            getAddressesFromApi(); // get laa addresses of user

        }
        catch (Exception e)
        {

        }

        return view;

    } // function of onCreateView

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        return  CubeAnimation.create(CubeAnimation.UP, enter, 3000);
    } // animation

    private void initRecycelerView() {
        adapter = new AddressesAdapter(getActivity(), addressList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        addresses_recycler_view.setHasFixedSize(true);
        addresses_recycler_view.setLayoutManager(linearLayoutManager);
        addresses_recycler_view.setAdapter(adapter);
    } // intialize list of addresses


    public int getPrefUserId() {
        int userId = 0;
        SharedPreferences preferences = getContext().getSharedPreferences("MyPrefsFile", MODE_PRIVATE);

        try {
            if (preferences != null)
                userId = preferences.getInt("id", 0);

            Log.i("Print", "User id" + userId);
        } catch (Exception e) {
            Log.i("Print", "Exception" + e.getMessage());
        }

        return userId;
    } // function of getPrefUserId

    private void fillLsitWithAddresses() {
        for (int i = 0; i < addressListApi.size(); i++) {
            addressList.add(addressListApi.get(i));
            Log.i("Print", "addressList" + addressList.size());
        }
        adapter.notifyDataSetChanged();
        if (addressListApi.size() <= 0) {
            toastDialog.initDialog(getString(R.string.noAddresses), getContext());
        }
    } // function of fillListWithAddresses


    @SuppressLint("HandlerLeak")
    private void getAddressesFromApi() {

        progress = new CustomDialogProgress();
        progress.init(getContext());
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }
        };
        progress.show();
        new Thread() {
            public void run() {

                Retrofit retrofit = RetrofitClient.getInstance();
                addressRequest addressRequest = retrofit.create(addressRequest.class);
                Call<addressResponse> addressResponseCall = addressRequest.getUserAddresses(getPrefUserId());
                addressResponseCall.enqueue(new Callback<addressResponse>() {
                    @Override
                    public void onResponse(Call<addressResponse> call, Response<addressResponse> response) {
                        try {

                            Log.i("Print", "code" + response.body().getCode());


                            if (response.body() != null) {

                                String code = response.body().getCode();
                                Log.i("Print", "code success" + code);

                                if (code.equals("200")) {
                                    addressListApi = response.body().getData();
                                    Log.i("Print", "addressListApi " + addressListApi.size());
                                    fillLsitWithAddresses();
                                } else {
                                    toastDialog.initDialog(getString(R.string.retry), getActivity());
                                    Log.i("Print", "else ");
                                }
                                progress.dismiss();
                            }

                        } catch (Exception e) {
                            Log.i("Print", "exception : " + e.toString());
                            progress.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(Call<addressResponse> call, Throwable t) {
                        toastDialog.initDialog(getString(R.string.retry), getActivity());
                        Log.i("Print", "error : " + t.toString());
                        progress.dismiss();
                    }
                });

            }
        }.start();

    } //function of getAddressesFromApi


    @Override
    public void onDestroy() {
        super.onDestroy();
        Thread.interrupted();
        progress.dismiss();
    } // on destroy

} // class of OrdersFragment
