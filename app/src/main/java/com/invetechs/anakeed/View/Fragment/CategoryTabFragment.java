package com.invetechs.anakeed.View.Fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;

import com.invetechs.anakeed.Adapter.CategoryAdapter;
import com.invetechs.anakeed.AppConfig.CustomDialogProgress;
import com.invetechs.anakeed.AppConfig.ShowDialog;
import com.invetechs.anakeed.Model.HomeCategory;
import com.invetechs.anakeed.Model.HomeCategoryResultModel;
import com.invetechs.anakeed.R;
import com.invetechs.anakeed.Retrofit.connection.RetrofitClient;
import com.invetechs.anakeed.Retrofit.products.productRequest;
import com.invetechs.anakeed.Retrofit.products.productResponse;
import com.invetechs.anakeed.View.Activity.CategoriesActivity;
import com.labo.kaji.fragmentanimations.FlipAnimation;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;


public class CategoryTabFragment extends Fragment {

    int position;
    RecyclerView recyclerView;
    CategoryAdapter categoryAdapter;
    List<String> categoryList = new ArrayList<>();
    HomeCategory homeCategory = HomeCategory.getInstance();
    List<HomeCategory> homeCategoryList = homeCategory.getCategoryList();
    private List<productResponse.ProductBean> productList = new ArrayList<>();
    private List<productResponse.ProductBean> productListApi = new ArrayList<>();
    ShowDialog toastDialog = new ShowDialog();
    Handler handler;
    CustomDialogProgress progress;
    HomeCategoryResultModel.CategoriesBean model;
    CategoriesActivity categoriesActivity;
    int id;



    public static Fragment getInstance(int position) {
        Bundle bundle = new Bundle();
        bundle.putInt("pos", position);
        CategoryTabFragment tabFragment = new CategoryTabFragment();
        tabFragment.setArguments(bundle);
        return tabFragment;
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        return FlipAnimation.create(FlipAnimation.UP  , enter ,  3000);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFragmentData();
    } // function of onCreate

    private void getFragmentData() {
        position = getArguments().getInt("pos");

    } // function of getFragmentData

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_category_tab, container, false);
        ButterKnife.bind(this, view);
        initCategoryListView(view);

        getProductsFromApi();

        return view;
    } // function of onCreateView

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser)
        {
            try {
                getProductsFromApi();

            }
            catch (Exception e)
            {

            }

        }
        else
        {

        }
    }

    private void initCategoryListView(View view) {
        recyclerView = view.findViewById(R.id.recyclerView_categories);

        try {
            categoryAdapter = new CategoryAdapter(getContext() , homeCategoryList.get(position).getBackgroundColor() , productList);
            LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setHasFixedSize(true);
            recyclerView.setAdapter(categoryAdapter);

        }
        catch (Exception e)
        {

        }
       // categoryAdapter = new CategoryAdapter(getContext(), categoryList, homeCategoryList.get(position).getBackgroundColor());

    } // function of initCategoryListView

    private void fillCategoryList() {
        if (categoryList.size() > 0) categoryList.clear();
        categoryList.add("a");
        categoryList.add("a");
        categoryList.add("a");
        categoryAdapter.notifyDataSetChanged();
    } // function of fillCategoryList


    @SuppressLint("HandlerLeak")
    private void getProductsFromApi() {

        if (productListApi.size() > 0 || productList.size() > 0) {
            productList.clear();
            productListApi.clear();
        }

        progress = new CustomDialogProgress();
        progress.init(getContext());
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }
        };

        progress.show();
        new Thread() {
            public void run() {

                Retrofit retrofit = RetrofitClient.getInstance();
                productRequest ordersRequest = retrofit.create(productRequest.class);

               try {

                   id = homeCategoryList.get(homeCategory.getSlectedPosition()).getId();

                 //  id = categoriesActivity.tab_id;
               }
               catch (Exception e)
               {

               }

                Log.i("Print", "iddddd : " + id);

                Call<productResponse> ordersResponseCall = ordersRequest.getProducts(id);

                ordersResponseCall.enqueue(new Callback<productResponse>() {
                    @Override
                    public void onResponse(Call<productResponse> call, Response<productResponse> response) {
                        try {

                            Log.i("Print", "code" + response.body().getCode());

                            if (response.body() != null) {

                                String code = response.body().getCode();
                                Log.i("Print", "code success" + code);

                                if (code.equals("200")) {

                                    productListApi = response.body().getProduct();
                                    Log.i("Print", "addressListApi " + productListApi.size());
                                    fillLsitWithProducts();
                                }

                                else {
                                    toastDialog.initDialog(getString(R.string.retry), getActivity());
                                    Log.i("Print", "else ");
                                }
                                progress.dismiss();
                            }

                        } catch (Exception e) {
                            Log.i("Print", "exception : " + e.toString());
                            progress.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(Call<productResponse> call, Throwable t) {
                        toastDialog.initDialog(getString(R.string.retry), getActivity());
                        Log.i("Print", "error : " + t.toString());
                        progress.dismiss();
                    }
                });

            }
        }.start();

    } //function of getProducts from api

    private void fillLsitWithProducts() {
        for (int i = 0; i < productListApi.size(); i++) {
            productList.add(productListApi.get(i));
            Log.i("Print", "ordersList" + productList.size());
        }
        categoryAdapter.notifyDataSetChanged();
        if (productListApi.size() <= 0) {
            toastDialog.initDialog(getString(R.string.no_products), getContext());
        }
    } // initlalize product list


} // class of CategoryTabFragment
