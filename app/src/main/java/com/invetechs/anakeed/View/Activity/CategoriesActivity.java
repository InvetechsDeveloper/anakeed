package com.invetechs.anakeed.View.Activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.google.android.material.tabs.TabLayout;

import androidx.core.view.ViewCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.invetechs.anakeed.Adapter.CategoriesViewPagerAdapter;
import com.invetechs.anakeed.AppConfig.CustomDialogProgress;
import com.invetechs.anakeed.AppConfig.ToolBar;
import com.invetechs.anakeed.Language.MyContextWrapper;
import com.invetechs.anakeed.Model.HomeCategory;
import com.invetechs.anakeed.R;
import com.invetechs.anakeed.Retrofit.connection.RetrofitClient;
import com.invetechs.anakeed.Retrofit.ads.adsRequest;
import com.invetechs.anakeed.Retrofit.ads.adsResponse;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class CategoriesActivity extends AppCompatActivity {


    private TabLayout tabLayout;
    ToolBar toolBar;
    private ViewPager viewPager;
    private String title = "";
    HomeCategory homeCategory;
    List<HomeCategory> categoryList = new ArrayList<>();
    CategoriesViewPagerAdapter categoriesViewPagerAdapter;
    String imageUrl = "http://3nakied.invetechs.com/cpanel/upload/Slider/";
    List<adsResponse.SlidersBean> imageAdsList = new ArrayList<>();
    Handler handler;
    CustomDialogProgress progress;
    TextSliderView textSliderView;
    SliderLayout sliderShow;
    protected SharedPreferences sharedPreferences;
    public static int tab_id ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);
        getAdverisMent();
        setToolbar();
        getCategories();
        initCategoryTabs();
        getIntentData();

    } // function of onCreate


    @SuppressLint("HandlerLeak")
    private void getAdverisMent() {
        if (imageAdsList.size() > 0) {
            imageAdsList.clear();

        }
        Log.i("Print", "imageAdsList first : " + imageAdsList.size());

        progress = new CustomDialogProgress();
        progress.init(this);

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
        new Thread() {
            public void run() {

                Retrofit retrofit = RetrofitClient.getInstance();

                final adsRequest advertisMentApi = retrofit.create(adsRequest.class);

                Call<adsResponse> getConnection = advertisMentApi.getAdvertisments();

                getConnection.enqueue(new Callback<adsResponse>() {
                    @Override
                    public void onResponse(Call<adsResponse> call, Response<adsResponse> response) {
                        try {

                            String message = response.body().getStatus();

                            Log.i("Print", "message : " + message);

                            if (message.equals("success")) {

                                imageAdsList = response.body().getSliders();
                                imageSlider(imageAdsList);
                                Log.i("Print", "imageAdsList : " + imageAdsList.size());

                            }

                            progress.dismiss();
                        } catch (Exception e) {

                            Log.i("Print", "exception : " + e.toString());
                            progress.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(Call<adsResponse> call, Throwable t) {

                        Log.i("Print", "error : " + t.toString());
                        progress.dismiss();
                    }
                });

            }

        }.start();
    } // function of getAdverisMent

    private void imageSlider(List<adsResponse.SlidersBean> urls) {


        sliderShow = findViewById(R.id.slider);

        Log.i("Print", "urlsList first : " + urls.size());

        for (int i = 0; i < urls.size(); i++) {
            textSliderView = new TextSliderView(this);
            textSliderView
                    .image(imageUrl + urls.get(i).getImg());
            sliderShow.addSlider(textSliderView);
            sliderShow.setPresetTransformer(SliderLayout.Transformer.DepthPage);
            sliderShow.setCustomIndicator((PagerIndicator) findViewById(R.id.custom_indicator));
            sliderShow.setCustomAnimation(new DescriptionAnimation());
            sliderShow.setDuration(3000);

        }

        Log.i("Print", "urls : " + urls.size());

//        if (urls.size() > 0) urls.clear();
//        urls.add("http://www.berkeleywellness.com/sites/default/files/field/image/iStock-589415708_field_img_hero_988_380.jpg");
//        urls.add("https://cdn.aarp.net/content/dam/aarp/health/healthy-living/2017/11/1140-fruits-vegetables-diet.imgcache.revca058f12325dc007a0333fe7396c6665.jpg");
//        urls.add("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvYmt_3bZI9aEaJYFwT_Yos9LLDlqQRZ1sIRdbVuisKQJv5aI1");
//
//        Log.i("Print", " images url : " + urls.size());
//
//        for (int i = 0; i < urls.size(); i++) {
//            textSliderView = new TextSliderView(getContext());
//            textSliderView
//                    .image(urls.get(i));
//            sliderShow.addSlider(textSliderView);
//            Log.e("Print", "Image : " + urls.get(i));
//        }

    } // function of imageSlider

    private void setToolbar() {
        toolBar = new ToolBar(this);
        toolBar.setTitle(getString(R.string.home));
        toolBar.addNotification();
    } // function of setToolbar

    private void getIntentData() {
        Intent intent = getIntent();
        if (intent != null) {
        }
    } // function of getIntentData

    private void getCategories() {
        homeCategory = HomeCategory.getInstance();
        categoryList = homeCategory.getCategoryList();

    } // function of getCategories

    private void initCategoryTabs() {

        try {
            viewPager = findViewById(R.id.categories_viewpager);

            categoriesViewPagerAdapter = new CategoriesViewPagerAdapter(getSupportFragmentManager(), categoryList, this);
            viewPager.setAdapter(categoriesViewPagerAdapter);
            tabLayout = findViewById(R.id.categories_tab);
            tabLayout.setupWithViewPager(viewPager);

            tabLayout.setBackgroundColor(Color.parseColor(categoryList.get(homeCategory.getSlectedPosition()).getBackgroundColor()));
            tabLayout.getTabAt(homeCategory.getSlectedPosition()).select();

            toolBar.setColor(categoryList.get(homeCategory.getSlectedPosition()).getBackgroundColor());

            sharedPreferences = getSharedPreferences("user", MODE_PRIVATE);

            if (sharedPreferences.getString("language", "ar").equals("ar")) {
                ViewCompat.setLayoutDirection(getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
                Typeface font = Typeface.createFromAsset(getAssets(), "fonts/ElMessiri-Regular.ttf");
                for (int i = 0, count = tabLayout.getTabCount(); i < count; i++) {
                    TextView tv = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
                    tv.setTypeface(font);
                    tabLayout.getTabAt(i).setCustomView(tv);
                }
            } else if (sharedPreferences.getString("language", "ar").equals("en")) {
                ViewCompat.setLayoutDirection(getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_LTR);
                Typeface font = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Regular.ttf");
                for (int i = 0, count = tabLayout.getTabCount(); i < count; i++) {
                    TextView tv = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
                    tv.setTypeface(font);
                    tabLayout.getTabAt(i).setCustomView(tv);
                }
            }


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(Color.parseColor(categoryList.get(homeCategory.getSlectedPosition()).getBackgroundColor()));
            }


            tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {

                    homeCategory.setSlectedPosition(tab.getPosition());

                   // tab_id = categoryList.get(tab.getPosition()).getId();

                    Log.i("Print", "click on tab : ");

                    try {

                        tabLayout.setBackgroundColor(Color.parseColor(categoryList.get(tab.getPosition()).getBackgroundColor()));
                        toolBar.setColor(categoryList.get(tab.getPosition()).getBackgroundColor());
                    }

                    catch (Exception e)
                    {

                    }

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        Window window = getWindow();
                        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                        window.setStatusBarColor(Color.parseColor(categoryList.get(tab.getPosition()).getBackgroundColor()));
                    }
                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {

                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {

                }
            });

        } catch (Exception e) {
            Log.e("Print", "Exception : " + e.getMessage());

        }


    } // function of initCategoryTabs

    @Override
    protected void attachBaseContext(Context newBase) {
        sharedPreferences = newBase.getSharedPreferences("user", MODE_PRIVATE);
        super.attachBaseContext(CalligraphyContextWrapper.wrap(new MyContextWrapper(newBase).wrap(sharedPreferences.getString("language", "ar"))));
    }// apply fonts


} // class of CategoriesActivity
