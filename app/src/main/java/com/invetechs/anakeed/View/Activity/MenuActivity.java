package com.invetechs.anakeed.View.Activity;

import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;

import androidx.annotation.NonNull;

import com.google.android.material.navigation.NavigationView;

import androidx.annotation.RequiresApi;
import androidx.core.view.ViewCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.transition.Slide;

import android.os.Build;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;

import com.invetechs.anakeed.AppConfig.CheckUserLogin;
import com.invetechs.anakeed.AppConfig.ToolBar;
import com.invetechs.anakeed.Language.CustomTypefaceSpan;
import com.invetechs.anakeed.Language.MyContextWrapper;
import com.invetechs.anakeed.R;
import com.invetechs.anakeed.View.Fragment.AddressesFragment;
import com.invetechs.anakeed.View.Fragment.ContactUsFragment;
import com.invetechs.anakeed.View.Fragment.HomeFragment;
import com.invetechs.anakeed.View.Fragment.OrdersFragment;
import com.invetechs.anakeed.View.Fragment.ProfileFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MenuActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    // bind views

    @BindView(R.id.drawer_layout)
    DrawerLayout drawer_layout;

    @BindView(R.id.nav_view)
    NavigationView nav_view;

    // variables
    SharedPreferences preferences;
    ToolBar toolBar;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    CheckUserLogin checkUserLogin;
    String flagFragment = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        ButterKnife.bind(this);
        checkUserLogin = new CheckUserLogin();
//        defaultFragment("home");
        setToolbar();
        initNavigationMenu();
        checkIfUserLogin();
    } // function of onCreate


    private void checkIfUserLogin() {
        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        String isLogin = prefs.getString("isLogin", "false");
        Log.i("Print", "profile id" + isLogin);
        if (isLogin.equals("true")) {
            Menu menuNav = nav_view.getMenu();
            MenuItem logoutItem = menuNav.findItem(R.id.nav_login);
            logoutItem.setTitle(getString(R.string.logout));
            applyFontToMenuItem(logoutItem);
        }
    } // check if user login to set logout title

    private void initNavigationMenu() {
        flagFragment = getIntent().getStringExtra("fragmentFlag");

        if (flagFragment != null) {

            if (flagFragment.equals("home")) {
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new HomeFragment()).commit();
                nav_view.setCheckedItem(R.id.home);
            }

        }

        try {
            nav_view.setNavigationItemSelectedListener(this);

            nav_view = findViewById(R.id.nav_view);
            Menu m = nav_view.getMenu();
            for (int i = 0; i < m.size(); i++) {
                MenuItem mi = m.getItem(i);

                //for aapplying a font to subMenu ...
                SubMenu subMenu = mi.getSubMenu();
                if (subMenu != null && subMenu.size() > 0) {
                    for (int j = 0; j < subMenu.size(); j++) {
                        MenuItem subMenuItem = subMenu.getItem(j);

                    }
                }
                //the method we have create in activity
                applyFontToMenuItem(mi);
            }

        } catch (Exception e) {

        }
    } // initialize menu

    private void setToolbar() {
        toolBar = new ToolBar(this);
        toolBar.setTitle(getString(R.string.home));
        toolBar.addNotification();
        toolBar.onDestroy();
    } // function of setToolbar

    private void defaultFragment(String flag) {
        if (flag.equals("home")) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new HomeFragment()).commit();
        }
    } // function defaultFragment

    @Override
    protected void attachBaseContext(Context newBase) {
        sharedPreferences = newBase.getSharedPreferences("user", MODE_PRIVATE);
        super.attachBaseContext(CalligraphyContextWrapper.wrap(new MyContextWrapper(newBase).wrap(sharedPreferences.getString("language", "ar"))));
    }// apply fonts

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

        switch (menuItem.getItemId()) {
            case R.id.nav_home:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new HomeFragment()).commit();
                toolBar.setTitle(getString(R.string.home));
                Log.e("Message", "Home fragment");
                break;

            case R.id.nav_account:
                try {
                    if (checkUserLogin.check(this)) {
                        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new ProfileFragment()).commit();
                        toolBar.setTitle(getString(R.string.account));
                        Log.e("Message", "Account fragment");
                    } else {
                        checkUserLogin.openLoginActivity(this);
                    }
                } catch (Exception e) {
                    Log.i("Print", "error : " + e.toString());

                }
                break;

            case R.id.nav_addresses:
                try {
                    if (checkUserLogin.check(getApplicationContext())) {
                        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new AddressesFragment()).commit();
                        toolBar.setTitle(getString(R.string.addresses));
                        Log.e("Message", "Addresses fragment");
                    } else {
                        checkUserLogin.openLoginActivity(getApplicationContext());
                    }

                } catch (Exception e) {
                    Log.i("Print", "error : " + e.toString());
                }
                break;

            case R.id.nav_orders:

                try {
                    if (checkUserLogin.check(this)) {
                        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new OrdersFragment()).commit();
                        toolBar.setTitle(getString(R.string.orders));
                        Log.e("Message", "Addresses fragment");
                    } else {
                        checkUserLogin.openLoginActivity(this);
                    }

                } catch (Exception e) {
                    Log.i("Print", "error : " + e.toString());
                }
                break;

            case R.id.nav_rate:
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.google.android.youtube"));
                startActivity(intent);
                break;

            case R.id.nav_share:
                Intent share = new Intent(android.content.Intent.ACTION_SEND);
                share.setType("text/plain");
                String shareBodyText = "Your shearing message goes here";
                share.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject/Title");
                share.putExtra(android.content.Intent.EXTRA_TEXT, shareBodyText);
                startActivity(Intent.createChooser(share, "Choose sharing method"));
                break;

            case R.id.nav_contactUs:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new ContactUsFragment()).commit();
                toolBar.setTitle(getString(R.string.contactUs));
                Log.e("Message", "contact Us fragment");
                break;

            case R.id.nav_login:
                SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
                String isLogin = prefs.getString("isLogin", "false");
                Log.i("Print", "profile id" + isLogin);

                if (isLogin.equals("true")) {
                    Intent login = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(login);
                    finish();
                    logOut();
                } else {
                    Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(i);
                    finish();
                    //   toastDialog.initDialog(getString(R.string.logOutSuccessfully),this);
                }
                break;

        }
        drawer_layout.closeDrawers();
        return true;
    } // function of onNavigationItemSelected

    private void logOut() {

        SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
        if (editor != null) {
            editor.remove("id").apply();
            editor.remove("name").apply();
            editor.remove("email").apply();
            editor.remove("mobilenumber").apply();
            editor.remove("password").apply();
            editor.putString("isLogin", "false");
        }

        SharedPreferences sharedPreferences = getSharedPreferences("notification_pusher", MODE_PRIVATE);
        if (sharedPreferences != null) {
            sharedPreferences.edit().remove("notifi_count").apply();
            sharedPreferences.edit().remove("cart_count").apply();

        }

        SharedPreferences address = getSharedPreferences("AddressPref", MODE_PRIVATE);
        if (address != null) {
            address.edit().remove("lat").apply();
            address.edit().remove("lng").apply();
            address.edit().remove("address").apply();
        }

    } // log out method


    private void applyFontToMenuItem(MenuItem mi) {
        Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/ElMessiri-Regular.ttf");
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("", font), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    } // function font

    public void bt_changeLanguage(View view) {

        sharedPreferences = getSharedPreferences("user", MODE_PRIVATE);

        Log.e("Print", "language : " + sharedPreferences.getString("language", ""));
        editor = getSharedPreferences("user", MODE_PRIVATE).edit();

        if (sharedPreferences.getString("language", "ar").equals("ar") || sharedPreferences.getString("language", "ar").equals("")) {
            editor.putString("language", "en");
            editor.apply();
            Log.e("Print", "language1 : " + sharedPreferences.getString("language", ""));
        } else if (sharedPreferences.getString("language", "ar").equals("en")) {
            editor.putString("language", "ar");
            editor.apply();
            Log.e("Print", "language2 : " + sharedPreferences.getString("language", ""));
        }
        Intent intent = new Intent(getApplicationContext(), MenuActivity.class);
        intent.putExtra("fragmentFlag", "home");
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();


    } // function of bt_changeLanguage
} // class of MenuActivity
