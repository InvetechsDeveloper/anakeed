package com.invetechs.anakeed.View.Activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.invetechs.anakeed.AppConfig.CustomDialogProgress;
import com.invetechs.anakeed.AppConfig.ShowDialog;
import com.invetechs.anakeed.Language.MyContextWrapper;
import com.invetechs.anakeed.R;
import com.invetechs.anakeed.Retrofit.connection.RetrofitClient;
import com.invetechs.anakeed.Retrofit.auth.change_password.ChangePassword;
import com.invetechs.anakeed.Retrofit.auth.forget_password.forgetPasswordApi;
import com.invetechs.anakeed.Retrofit.auth.forget_password.forgetResponse;
import com.invetechs.anakeed.Retrofit.auth.login.loginResponse;
import com.invetechs.anakeed.Retrofit.auth.verification.verifyForgetPasswordRequest;
import com.rengwuxian.materialedittext.MaterialEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Forget_password extends AppCompatActivity {

    // bind views

    @BindView(R.id.view_flipper)
    ViewFlipper view_flipper;

    @BindView(R.id.tv_resend_code)
    TextView tv_resend_code;

    @BindView(R.id.et_phone_number)
    MaterialEditText et_phone_number;

    @BindView(R.id.et_new_password)
    MaterialEditText etNewPassword;

    @BindView(R.id.et_confirm_password)
    MaterialEditText etConfirmNewPassword;

    @BindView(R.id.et_verify_code)
    MaterialEditText etCode;

    // vars
    protected SharedPreferences sharedPreferences;
    Handler handler;
    CustomDialogProgress progress;
    SharedPreferences.Editor editor;
    String code, mobile, category = "user";
    int id;
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    ShowDialog toastDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);
        ButterKnife.bind(this);
        toastDialog = new ShowDialog();
        progress = new CustomDialogProgress();
        tv_resend_code.setPaintFlags(tv_resend_code.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

    }


    @OnClick(R.id.tv_resend_code)
    public void resendCode() {
        new CountDownTimer(60000, 1000) {

            public void onTick(long millisUntilFinished) {
                tv_resend_code.setText(getString(R.string.second_remaining) + millisUntilFinished / 1000);
                tv_resend_code.setEnabled(false);

            }

            public void onFinish() {
                tv_resend_code.setText(getString(R.string.resend_code));
                toastDialog.initDialog(getString(R.string.code_send), Forget_password.this);
                resendingCode();
            }

        }.start();
    } // click on resend code

    private boolean ValidateMobile() {

        String Mobile = et_phone_number.getText().toString().trim();

        if (Mobile.length() < 10) {
            et_phone_number.setError(getString(R.string.error_valid_mobile));
            return false;
        }
        if (Mobile.length() > 10) {
            et_phone_number.setError(getString(R.string.enter_valid_number));
            return false;
        }

        if (Mobile.length() == 10) {
            et_phone_number.setError(null);
        }

        return true;
    } // validation for mobile phone

    public boolean ValidatePassword() {

        String newPassword = etNewPassword.getText().toString().trim();
        String confirmPassword = etConfirmNewPassword.getText().toString().trim();

        if (newPassword.isEmpty()) {
            etNewPassword.setError(getString(R.string.field_cant_empty));
            return false;
        }

        if (newPassword.length() < 4) {
            etNewPassword.setError(getString(R.string.valid_password_number));
            return false;

        }

        if (confirmPassword.isEmpty()) {
            etConfirmNewPassword.setError(getString(R.string.field_cant_empty));
            return false;
        }

        if (!newPassword.equals(confirmPassword)) {
            toastDialog.initDialog(getString(R.string.password_not_match), Forget_password.this);
            return false;

        }
        return true;


    }  // validation for password

    public boolean ValidateVerificationCode() {

        code = etCode.getText().toString().trim();

        if (code.isEmpty()) {
            etCode.setError(getString(R.string.field_cant_empty));
            return false;
        }

        if (code.length() != 4) {
            etCode.setError(getString(R.string.invalid_code));
            return false;
        }

        return true;

    } // validate for code


    @OnClick(R.id.btn_phone_confirm)
    public void enterPhoneNumber() {
        if (!ValidateMobile()) {
            return;
        }

        String phone = et_phone_number.getText().toString().trim();
        mobile = phone;
        forgetPassword();
    } // proceed after enter phone number


    @OnClick(R.id.btn_code_confirm)
    public void enterCode() {
        if (!ValidateVerificationCode()) {
            return;
        }
        verify();

    } // entet code number

    @OnClick(R.id.change_password_confirm)
    public void savePassword() {
        if (!ValidatePassword()) {
            return;
        }
        updatePassword();
    } // enter new password and confirm password

    private void resendingCode() {
        progress = new CustomDialogProgress();
        progress.init(this);
        handler = new Handler() {

            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
        new Thread() {
            public void run() {
//Retrofit
                Retrofit retrofit = RetrofitClient.getInstance();
                final forgetPasswordApi userApi = retrofit.create(forgetPasswordApi.class);

                final Call<forgetResponse> getInterestConnection = userApi.forgetPass(mobile);

                getInterestConnection.enqueue(new Callback<forgetResponse>() {
                    @Override
                    public void onResponse(Call<forgetResponse> call, Response<forgetResponse> response) {
                        try {


                            forgetResponse.DataBean user_data = response.body().getData();
                            String code = response.body().getCode();
                            String verification = String.valueOf(user_data.getVerificationcode());

                            if (code.equals("200")) {

                              //  toastDialog.initDialog(verification, Forget_password.this);

                            } // login success

                            else if (code.equals("1313")) {

                                toastDialog.initDialog(getString(R.string.invalid_mobile), Forget_password.this);
                                progress.dismiss();
                            } // user need admin confirmation

                            progress.dismiss();

                        } // try
                        catch (Exception e) {
                            Log.i("Print", "exception : " + e.toString());
                            progress.dismiss();
                        } // catch
                    } // onResponse

                    @Override
                    public void onFailure(Call<forgetResponse> call, Throwable t) {
                        toastDialog.initDialog(getString(R.string.retry), Forget_password.this);
                        Log.i("Print", "error : " + t.toString());
                        progress.dismiss();
                    } // on Failure
                });
// Retrofit

            }

        }.start();
    } // resend code after one muinite

    private void forgetPassword() {
        progress = new CustomDialogProgress();
        progress.init(this);

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
        new Thread() {
            public void run() {

                Retrofit retrofit = RetrofitClient.getInstance();

                final forgetPasswordApi userApi = retrofit.create(forgetPasswordApi.class);

                final Call<forgetResponse> getInterestConnection = userApi.forgetPass(mobile);

                getInterestConnection.enqueue(new Callback<forgetResponse>() {
                    @Override
                    public void onResponse(Call<forgetResponse> call, Response<forgetResponse> response) {
                        try {

                            String message = response.body().getMessage();
                            String code = response.body().getCode();
                            forgetResponse.DataBean user_data = response.body().getData();
                      //      String verification = String.valueOf(user_data.getVerificationcode());

                            if (code.equals("200")) {
                                progress.dismiss();
                                view_flipper.showNext();
                                toastDialog.initDialog(getString(R.string.code_send), Forget_password.this);
                                Log.i("Print", "mobile number : " + mobile);

                            } // login success

                            else if (code.equals("1313")){
                                toastDialog.initDialog(getString(R.string.invalid_mobile), Forget_password.this);
                            }

                            progress.dismiss();

                        } // try
                        catch (Exception e) {
                            Log.i("Print", "exception : " + e.toString());
                            progress.dismiss();
                        } // catch
                    } // onResponse

                    @Override
                    public void onFailure(Call<forgetResponse> call, Throwable t) {

                        toastDialog.initDialog(getString(R.string.retry), Forget_password.this);
                        Log.i("Print", "error : " + t.toString());
                        progress.dismiss();
                    } // on Failure
                });
// Retrofit

            }

        }.start();
    } // function of forget password

    private void verify() {
        progress = new CustomDialogProgress();
        progress.init(this);

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }
        };
        progress.show();
        new Thread() {
            public void run() {
//Retrofit
                Retrofit retrofit = RetrofitClient.getInstance();

                final verifyForgetPasswordRequest userApi = retrofit.create(verifyForgetPasswordRequest.class);

                final Call<loginResponse> getInterestConnection = userApi.verifyForgetPass(mobile, code);

                getInterestConnection.enqueue(new Callback<loginResponse>() {
                    @Override
                    public void onResponse(Call<loginResponse> call, Response<loginResponse> response) {
                        try {
                            Log.i("Print", "mobile  : " + mobile);
                            Log.i("Print", "token  : " + code);

                            String code = response.body().getCode();
                            if (code.equals("200")) {
                                loginResponse.UserdataBean user = response.body().getUserdata();
                                id = user.getId();
                                view_flipper.showNext();
                                progress.dismiss();

                            } else if (code.equals("1313")) {
                                toastDialog.initDialog(getString(R.string.wrong_token), Forget_password.this);
                                progress.dismiss();
                            }

                            progress.dismiss();

                        } // try
                        catch (Exception e) {
                            Log.i("Print", "exception : " + e.toString());
                            progress.dismiss();
                        } // catch
                    } // onResponse

                    @Override
                    public void onFailure(Call<loginResponse> call, Throwable t) {

                        toastDialog.initDialog(getString(R.string.retry), Forget_password.this);
                        Log.i("Print", "error : " + t.toString());
                        progress.dismiss();
                    } // on Failure
                });
// Retrofit

            }

        }.start();
    } // verfy

    @SuppressLint("HandlerLeak")
    private void updatePassword() {

        final String pass = etNewPassword.getText().toString();
        progress = new CustomDialogProgress();
        progress.init(this);

        handler = new Handler() {

            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
        new Thread() {
            public void run() {
//Retrofit
                Retrofit retrofit = RetrofitClient.getInstance();

                final ChangePassword userApi = retrofit.create(ChangePassword.class);

                final Call<loginResponse> getInterestConnection = userApi.change_password(id, pass);

                getInterestConnection.enqueue(new Callback<loginResponse>() {
                    @Override
                    public void onResponse(Call<loginResponse> call, Response<loginResponse> response) {
                        try {
                            String code = response.body().getCode();
                            Log.i("Print", "code : " + code);
                            if (code.equals("200")) {

                                try {
                                    toastDialog.initDialog(getString(R.string.password_saved), Forget_password.this);
                                    progress.dismiss();

                                    Intent intent = new Intent(Forget_password.this, LoginActivity.class);
                                    SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                                    editor.putString("password", pass);
                                    editor.apply();
                                    startActivity(intent);
                                    finish();
                                } catch (Exception e) {

                                }


                            } // update password

                            progress.dismiss();

                        } // try
                        catch (Exception e) {
                            Log.i("Print", "exception : " + e.toString());
                            progress.dismiss();
                        } // catch
                    } // onResponse

                    @Override
                    public void onFailure(Call<loginResponse> call, Throwable t) {

                        toastDialog.initDialog(getString(R.string.retry), Forget_password.this);
                        Log.i("Print", "error : " + t.toString());
                        progress.dismiss();
                    } // on Failure
                });
// Retrofit
            }

        }.start();
    } // function of updatePassword

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Thread.interrupted();
        try {
            toastDialog.dismissDialog();

        } catch (Exception e) {

        }
    } // on destroy


    @Override
    protected void attachBaseContext(Context newBase) {
        sharedPreferences = newBase.getSharedPreferences("user", MODE_PRIVATE);
        super.attachBaseContext(CalligraphyContextWrapper.wrap(new MyContextWrapper(newBase).wrap(sharedPreferences.getString("language", "ar"))));
    }// apply fonts


}
