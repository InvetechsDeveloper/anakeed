package com.invetechs.anakeed.View.Activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.ViewCompat;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.WindowManager;

import com.google.android.material.button.MaterialButton;
import com.invetechs.anakeed.AppConfig.CustomDialogProgress;
import com.invetechs.anakeed.AppConfig.ShowDialog;
import com.invetechs.anakeed.Language.MyContextWrapper;
import com.invetechs.anakeed.R;
import com.invetechs.anakeed.Retrofit.connection.RetrofitClient;
import com.invetechs.anakeed.Retrofit.auth.login.loginRequest;
import com.invetechs.anakeed.Retrofit.auth.login.loginResponse;
import com.rengwuxian.materialedittext.MaterialEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.anwarshahriar.calligrapher.Calligrapher;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class LoginActivity extends AppCompatActivity {

    // bind views
    @BindView(R.id.et_mobile_number)
    MaterialEditText et_mobile_number;

    @BindView(R.id.et_password)
    MaterialEditText et_password;

    @BindView(R.id.btn_login)
    MaterialButton btn_login;

    // variables
    protected SharedPreferences sharedPreferences;
    String moile = "", password = "", code = "";
    CustomDialogProgress progress;
    Handler handler;
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    SharedPreferences.Editor editor;
    ShowDialog toastDialog;
    SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        hideKeyboard();
        progress = new CustomDialogProgress();
        toastDialog = new ShowDialog();
    } // on create method


    @OnClick(R.id.btn_login)
    public void loginClick() {

        if (!validatePassword() | !validateMobile() ) {
            return;
        } else {
            moile = et_mobile_number.getText().toString();
            password = et_password.getText().toString();
            Log.i("Print", "mobile number : " + moile);
            Log.i("Print", "password : " + password);
            loginMethod();
        }

    } // init login click

    @SuppressLint("HandlerLeak")
    private void loginMethod() {

        progress.init(this);
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }
        };
        progress.show();
        new Thread() {
            public void run() {
                Retrofit retrofit = RetrofitClient.getInstance();
                loginRequest login_request = retrofit.create(loginRequest.class);
                Call<loginResponse> getConnection = login_request.login(moile, password);
                Log.i("Print", "send request mobile : " + moile);
                Log.i("Print", "send request password : " + password);

                getConnection.enqueue(new Callback<loginResponse>() {
                    @Override
                    public void onResponse(Call<loginResponse> call, Response<loginResponse> response) {

                        try {
                            code = response.body().getCode();
                            Log.i("Print", "code : " + code);

                            if (code.equals("200")) {
                                loginResponse.UserdataBean user = response.body().getUserdata();
                                editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                                editor.putString("isLogin", "true");
                                editor.putInt("id", user.getId());
                                editor.putString("user_name", user.getName());
                                editor.putString("password", password);
                                editor.putString("token", user.getToken());
                                editor.apply();
                                Log.i("Print", "login id : " + user.getId());
                                Log.i("Print", "user_name : " + user.getName());
                                Log.i("Print", "login password : " + password);
                                Log.i("Print", "login token : " + user.getToken());

                                // start intent
                                Intent menuActivity = new Intent(LoginActivity.this, MenuActivity.class);
                                menuActivity.putExtra("fragmentFlag", "home");
                                startActivity(menuActivity);
                                finish();
                            } else if (code.equals("1314")) {
//                                toastDialog.initDialog(getString(R.string.verify_code), LoginActivity.this);
//                                progress.dismiss();
//                                generateCode();
                                loginResponse.UserdataBean user = response.body().getUserdata();

                                Intent intent = new Intent(LoginActivity.this, VerificationActivity.class);
                                intent.putExtra("verify", true);
                                intent.putExtra("mobilenumber", user.getPhone());
                                intent.putExtra("verification_code", user.getVerificationcode());
                                startActivity(intent);
                                finish();


                            } else if (code.equals("1315")) {
                                toastDialog.initDialog(getString(R.string.password_invalid), LoginActivity.this);
                                progress.dismiss();


                            } // invaild password

                            else if (code.equals("1316")) {
                                toastDialog.initDialog(getString(R.string.invalid_mobile), LoginActivity.this);
                                progress.dismiss();

                            } // invaild mobile
                            progress.dismiss();

                        } catch (Exception e) {
                            Log.i("Print", "exception : " + e.toString());
                            progress.dismiss();
                        } // catch

                    } // on response

                    @Override
                    public void onFailure(Call<loginResponse> call, Throwable t) {
                        toastDialog.initDialog(getString(R.string.retry), LoginActivity.this);
                        Log.i("Print", "error : " + t.toString());
                        progress.dismiss();
                    }
                });


            }
        }.start();

    } // login function

    private boolean validateMobile() {

        moile = et_mobile_number.getText().toString().trim();

        if (moile.isEmpty()) {
            et_mobile_number.setError(getString(R.string.field_cant_empty));
            return false;
        }

        if (moile.length() == 10) {
            if (!(moile.charAt(0) == '0' && moile.charAt(1) == '5')) {
                et_mobile_number.setError(getString(R.string.enter_correct_number));
                return false;
            }
        } else {
            et_mobile_number.setError(getString(R.string.enter_correct_number));
            return false;

        }

        return true;
    } // validation on mobile number

    private boolean validatePassword() {

        password = et_password.getText().toString().trim();
        if (password.isEmpty()) {
            et_password.setError(getString(R.string.field_cant_empty));
            return false;
        } else if (password.length() < 4) {
            et_password.setError(getString(R.string.valid_password_number));
            return false;
        } else {
            et_password.setError(null);
            return true;
        }
    } // validation on password

    private void hideKeyboard() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    } // hide keyboard until user click

    @OnClick(R.id.forget_password)
    public void forgetPassword() {
        startActivity(new Intent(this, Forget_password.class));
        finish();
    } // click on forget password

    @OnClick(R.id.new_account)
    public void registerNewAccount() {
        startActivity(new Intent(this, RegisterActivity.class));
        finish();
    } // go to register activity

    @Override
    protected void attachBaseContext(Context newBase) {
        sharedPreferences = newBase.getSharedPreferences("user", MODE_PRIVATE);
        super.attachBaseContext(CalligraphyContextWrapper.wrap(new MyContextWrapper(newBase).wrap(sharedPreferences.getString("language", "ar"))));
    }// apply fonts

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            toastDialog.dismissDialog();

        } catch (Exception e) {

        }
        Thread.interrupted();
    } // on destroy

    @Override
    protected void onPause() {
        super.onPause();
        try {
            toastDialog.dismissDialog();

        } catch (Exception e) {

        }
        Thread.interrupted();
    } // on pause
}
