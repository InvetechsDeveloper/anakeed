package com.invetechs.anakeed.Language;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.ViewCompat;

import com.invetechs.anakeed.R;

import me.anwarshahriar.calligrapher.Calligrapher;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class Language extends AppCompatActivity {

    //vars
    protected SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        checkLanguage();

    } // function of onCreate

    private void checkLanguage() {

        sharedPreferences = getSharedPreferences("user", MODE_PRIVATE);

        if (sharedPreferences.getString("language","ar").equals("ar"))
        {
            ViewCompat.setLayoutDirection(getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
            Calligrapher calligrapher = new Calligrapher(this);
          //  calligrapher.setFont(this, "fonts/ElMessiri-Regular.ttf", true);

            CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                    .setDefaultFontPath("fonts/ElMessiri-Regular.ttf")
                    .setFontAttrId(R.attr.fontPath)
                    .build()
            );

        }
        else if (sharedPreferences.getString("language","ar").equals("en"))
        {
            ViewCompat.setLayoutDirection(getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_LTR);
            Calligrapher calligrapher = new Calligrapher(this);
            calligrapher.setFont(this, "fonts/OpenSans-Regular.ttf", true);

        }
    } // change direction of layout as per language

    @Override
    protected void attachBaseContext(Context newBase) {
        sharedPreferences = newBase.getSharedPreferences("user",MODE_PRIVATE);
        super.attachBaseContext(new MyContextWrapper(newBase).wrap(sharedPreferences.getString("language","ar")));
    } // attatch base context and set default language to arabic
}
